#include <pjsr/StdIcon.jsh>
#include <pjsr/StdButton.jsh>
#include <pjsr/StdDialogCode.jsh>
#include <pjsr/Sizer.jsh>
#include <pjsr/FrameStyle.jsh>
#include <pjsr/TextAlign.jsh>
#include <pjsr/StdButton.jsh>
#include <pjsr/StdIcon.jsh>
#include <pjsr/StdCursor.jsh>
#include <pjsr/ColorSpace.jsh>
#include <pjsr/Interpolation.jsh>
#include <pjsr/UndoFlag.jsh>
#include <pjsr/NumericControl.jsh>
#include <pjsr/CheckState.jsh>
#include <pjsr/SampleType.jsh>
#include <pjsr/SectionBar.jsh>
#include "PixInsightToolsPreviewControl.jsh"


#feature-id SelectiveColorCorrection :  Toolbox > SelectiveColorCorrection

#feature-info  A script for selective color correction.<br/>\
   <br/>\
   A script for selective color correction. \
   <br/> \
   Copyright &copy; 2024 Juergen Terpe


#define VERSION "1.17a"
#define TITLE  "Selective Color Correction"
#define TEXT   "Changes colors selectively on non-linear images."

#define RED 0
#define GREEN 1
#define BLUE 2

#define LEFTWIDTH 440
#define LABELWIDTH 150

#define FILTER_RED "Red"
#define FILTER_GREEN "Green"
#define FILTER_BLUE "Blue"
#define FILTER_CYAN "Cyan"
#define FILTER_MAGENTA "Magenta"
#define FILTER_YELLOW "Yellow"
#define FILTER_LOWSATURATION "Low Saturation"
#define FILTER_HIGHSATURATION "High Saturation"
#define FILTER_SHADOWS "Shadows"
#define FILTER_MIDTONES "Midtones"
#define FILTER_HIGHLIGHTS "Highlights"
#define FILTER_LUMINANCE "Luminance"
#define FILTER_IMPORTEDMASK "Imported Mask"


var selectiveColorParameters = {
   imageView: undefined,
   mask: undefined,
   previewImage: undefined,
   maskPreview: undefined,
   usedMaskInPreview: undefined,
   starMask: undefined,
   importedMask: undefined,
   maskSmoothness: 10.0,
   maskShadows: 0.0,
   maskBalance: 0.5,
   maskHighlights: 1.0,
   previewMode: 0,
   useStarMask: false,
   showMask: false,
   filterMethod: "Red",
   cyan: 0.0,
   magenta: 0.0,
   yellow: 0.0,
   red: 0.0,
   green: 0.0,
   blue: 0.0,
   luminance: 0.0,
   saturation: 0.0,
   contrast: 0.0,
   undoCount: 0,
   starMaskIntensity: 1.8,
   previewOriginal: undefined,
   showOriginalInPreview: false,
   previewId: 1,

   // stores the current parameters values into the script instance
    save: function() {
        Parameters.set("maskShadows", selectiveColorParameters.maskShadows);
        Parameters.set("maskBalance", selectiveColorParameters.maskBalance);
        Parameters.set("maskHighlights", selectiveColorParameters.maskHighlights);
        Parameters.set("previewMode", selectiveColorParameters.previewMode);
        Parameters.set("useStarMask", selectiveColorParameters.useStarMask);
        Parameters.set("showMask", selectiveColorParameters.showMask);
        Parameters.set("filterMethod", selectiveColorParameters.filterMethod);
        Parameters.set("cyan", selectiveColorParameters.cyan);
        Parameters.set("magenta", selectiveColorParameters.magenta);
        Parameters.set("yellow", selectiveColorParameters.yellow);
        Parameters.set("red", selectiveColorParameters.red);
        Parameters.set("green", selectiveColorParameters.green);
        Parameters.set("blue", selectiveColorParameters.blue);
        Parameters.set("luminance", selectiveColorParameters.luminance);
        Parameters.set("starMaskIntensity", selectiveColorParameters.starMaskIntensity);
        Parameters.set("saturation", selectiveColorParameters.saturation);
        Parameters.set("contrast", selectiveColorParameters.contrast);
        Parameters.set("maskSmoothness", selectiveColorParameters.maskSmoothness);
    },

    // loads the script instance parameters
    load: function() {

         Console.writeln("Loading previous parameters...");
         if (Parameters.has("maskShadows"))
            selectiveColorParameters.maskShadows = Parameters.getReal("maskShadows");
         if (Parameters.has("maskBalance"))
            selectiveColorParameters.maskBalance = Parameters.getReal("maskBalance");
         if (Parameters.has("maskHighlights"))
            selectiveColorParameters.maskHighlights = Parameters.getReal("maskHighlights");
         if (Parameters.has("previewMode"))
            selectiveColorParameters.previewMode = Parameters.getInteger("previewMode");
         if (Parameters.has("useStarMask"))
            selectiveColorParameters.useStarMask = Parameters.getBoolean("useStarMask");
         if (Parameters.has("showMask"))
            selectiveColorParameters.showMask = Parameters.getBoolean("showMask");
         if (Parameters.has("filterMethod"))
            selectiveColorParameters.filterMethod = Parameters.getString("filterMethod");
         if (Parameters.has("cyan"))
            selectiveColorParameters.cyan = Parameters.getReal("cyan");
         if (Parameters.has("magenta"))
            selectiveColorParameters.magenta = Parameters.getReal("magenta");
         if (Parameters.has("yellow"))
            selectiveColorParameters.yellow = Parameters.getReal("yellow");
         if (Parameters.has("red"))
            selectiveColorParameters.red = Parameters.getReal("red");
         if (Parameters.has("green"))
            selectiveColorParameters.green = Parameters.getReal("green");
         if (Parameters.has("blue"))
            selectiveColorParameters.blue = Parameters.getReal("blue");
         if (Parameters.has("luminance"))
            selectiveColorParameters.luminance = Parameters.getReal("luminance");
         if (Parameters.has("saturation"))
            selectiveColorParameters.saturation = Parameters.getReal("saturation");
         if (Parameters.has("contrast"))
            selectiveColorParameters.contrast = Parameters.getReal("contrast");
         if (Parameters.has("starMaskIntensity"))
            selectiveColorParameters.starMaskIntensity = Parameters.getReal("starMaskIntensity");
         if (Parameters.has("maskSmoothness"))
            selectiveColorParameters.maskSmoothness = Parameters.getReal("maskSmoothness");

         selectiveColorParameters.imageView = undefined;
         selectiveColorParameters.mask = undefined;
         selectiveColorParameters.previewImage = undefined;
         selectiveColorParameters.maskPreview = undefined;
         selectiveColorParameters.usedMaskInPreview = undefined;
         selectiveColorParameters.starMask = undefined;
         selectiveColorParameters.importedMask = undefined;
   }

}

function getPreviewKey() {

   try {

      if (selectiveColorParameters.imageView == undefined)
         return ""
      else {
         let key = "Image: "+ selectiveColorParameters.imageView
            + ", Mode: "+ selectiveColorParameters.previewMode
            + ", Method: "+ selectiveColorParameters.filterMethod
            + ", Cyan: " + selectiveColorParameters.cyan
            + ", Magenta: " + selectiveColorParameters.magenta
            + ", Yellow: " + selectiveColorParameters.yellow
            + ", Red: " + selectiveColorParameters.red
            + ", Green: " + selectiveColorParameters.green
            + ", Blue: " + selectiveColorParameters.blue
            + ", Luminance: " + selectiveColorParameters.luminance
            + ", Saturation: " + selectiveColorParameters.saturation
            + ", Contrast: " + selectiveColorParameters.contrast
            + ", UseStarMask: " + selectiveColorParameters.useStarMask
            + ", StarMaskIntensity: " + selectiveColorParameters.starMaskIntensity
            + ", MaskShadows: " + selectiveColorParameters.maskShadows
            + ", MaskHightlights: " + selectiveColorParameters.maskHighlights
            + ", MaskBalance: " + selectiveColorParameters.maskBalance
            + ", MaskSmoothness: " + selectiveColorParameters.maskSmoothness
            + ", previewId: "+ selectiveColorParameters.previewId;

         return key;
      }
   }
   catch(error) {
      Console.writeln(error);
      return "..."
   }
}


function scaleImage(image) {

   var maxSize = Math.max(image.width, image.height);

   if (selectiveColorParameters.previewMode == 0) {
      var scale = Math.min(2400.0/maxSize, 1.0/3.0);
      image.resample(scale);
   }
   else if (selectiveColorParameters.previewMode == 1) {
      var scale = Math.min(4000.0/maxSize, 1.0/2.0);
      image.resample(scale);
   }

   return image;
}

function clone(view, postfix) {
   var P = new PixelMath;
   P.expression = "$T";
   P.useSingleExpression = true;
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = false;
   P.rescaleLower = 0;
   P.rescaleUpper = 1;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = true;
   P.showNewImage = false;
   P.newImageId = view.id+postfix;
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.SameAsTarget;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
   P.executeOn(view);

   return View.viewById(P.newImageId);
}

function subtract(view1, view2, identifier) {
   var P = new PixelMath;
   P.expression = view1.id + " - " + view2.id;
   P.useSingleExpression = true;
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = false;
   P.rescaleLower = 0;
   P.rescaleUpper = 1;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = true;
   P.showNewImage = false;
   P.newImageId = identifier;
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.Gray;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
   P.executeOn(view1);

   return View.viewById(P.newImageId);
}

function extractLuminance(view, id) {
   var P = new PixelMath;
   P.expression = "CIEL($T)";
   P.useSingleExpression = true;
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = false;
   P.rescaleLower = 0;
   P.rescaleUpper = 1;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = true;
   P.showNewImage = false;
   P.newImageId = id;
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.Gray;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
   P.executeOn(view);
   return View.viewById(id);
}


function subtractInPlace(view1, view2) {
   var P = new PixelMath;
   P.expression = "~((~"+ view1.id +")/(~"+ view2.id +"))";
   P.useSingleExpression = true;
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = true;
   P.rescale = false;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = false;
   P.executeOn(view1);
}


function createStarless(view) {

   var clonedView = clone(view, "_starless");

   try {
      var P = new StarXTerminator;
      P.stars = false;
      P.unscreen = false;
      P.overlap = 0.20;

      P.executeOn(clonedView);

      return clonedView;

   } catch(error) {
      return undefined;
   }
   return undefined;
}

function extractStars(view) {
   var starless = createStarless(view);
   if (starless !== undefined) {
      subtractInPlace(view, starless);
      starless.window.forceClose();
   }
   else extractStarsMLT(view);
}


function extractStarsMLT(view) {
   var P = new MultiscaleLinearTransform;
   P.layers = [
      [false, true, 0.000, false, 3.000, 1.00, 1],
      [true, true, 0.000, false, 3.000, 1.00, 1],
      [true, true, 0.000, false, 40.000, 1.00, 1],
      [true, true, 0.500, true, 40.000, 1.00, 1],
      [false, true, 0.000, false, 3.000, 1.00, 1]
   ];
   P.transform = MultiscaleLinearTransform.prototype.StarletTransform;
   P.scaleDelta = 0;
   P.scalingFunctionData = [
      0.25,0.5,0.25,
      0.5,1,0.5,
      0.25,0.5,0.25
   ];
   P.scalingFunctionRowFilter = [
      0.5,
      1,
      0.5
   ];
   P.scalingFunctionColFilter = [
      0.5,
      1,
      0.5
   ];
   P.scalingFunctionNoiseSigma = [
      0.8003,0.2729,0.1198,
      0.0578,0.0287,0.0143,
      0.0072,0.0036,0.0019,
      0.001
   ];
   P.scalingFunctionName = "Linear Interpolation (3)";
   P.linearMask = false;
   P.linearMaskAmpFactor = 100;
   P.linearMaskSmoothness = 1.00;
   P.linearMaskInverted = true;
   P.linearMaskPreview = false;
   P.largeScaleFunction = MultiscaleLinearTransform.prototype.NoFunction;
   P.curveBreakPoint = 0.75;
   P.noiseThresholding = true;
   P.noiseThresholdingAmount = 1.00;
   P.noiseThreshold = 10.00;
   P.softThresholding = true;
   P.useMultiresolutionSupport = false;
   P.deringing = false;
   P.deringingDark = 0.1000;
   P.deringingBright = 0.0000;
   P.outputDeringingMaps = false;
   P.lowRange = 0.0000;
   P.highRange = 0.0000;
   P.previewMode = MultiscaleLinearTransform.prototype.Disabled;
   P.previewLayer = 0;
   P.toLuminance = true;
   P.toChrominance = true;
   P.linear = false;
   P.executeOn(view);

   P = new CurvesTransformation;
   P.K = [ // x, y
      [0.00000, 0.00000],
      [0.32957, 0.25195],
      [0.55079, 0.72987],
      [1.00000, 1.00000]
   ];
   P.Kt = CurvesTransformation.prototype.AkimaSubsplines;
   P.executeOn(view);


   P = new MorphologicalTransformation;
   P.operator = MorphologicalTransformation.prototype.Dilation;
   P.interlacingDistance = 1;
   P.lowThreshold = 0.000000;
   P.highThreshold = 0.000000;
   P.numberOfIterations = 1;
   P.amount = 1.00;
   P.selectionPoint = 0.50;
   P.structureName = "";
   P.structureSize = 7;
   P.structureWayTable = [ // mask
      [[
         0x00,0x00,0x01,0x01,0x01,0x00,0x00,
         0x00,0x01,0x01,0x01,0x01,0x01,0x00,
         0x01,0x01,0x01,0x01,0x01,0x01,0x01,
         0x01,0x01,0x01,0x01,0x01,0x01,0x01,
         0x01,0x01,0x01,0x01,0x01,0x01,0x01,
         0x00,0x01,0x01,0x01,0x01,0x01,0x00,
         0x00,0x00,0x01,0x01,0x01,0x00,0x00
      ]]
   ];
   P.executeOn(view);


}

function intensifyStarMask(view) {
   var P = new PixelMath;
   P.expression = "$T * " + selectiveColorParameters.starMaskIntensity;
   P.useSingleExpression = true;
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = false;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = false;
   P.executeOn(view);
}

function convoluteStarMask(view) {
   var P = new Convolution;
   P.mode = Convolution.prototype.Parametric;
   P.sigma = 2.00;
   P.shape = 2.00;
   P.aspectRatio = 1.00;
   P.rotationAngle = 0.00;
   P.filterSource = "";
   P.rescaleHighPass = false;
   P.viewId = "";
   P.executeOn(view);
}

function createStarMask(view) {
   Console.noteln("Creating star mask...");
   var starMask = extractLuminance(view, view.id+"_lum");
   extractStars(starMask);

   return starMask;
}

function processMagentaCurves(view, magenta, swapfile=true) {

   if (Math.abs(magenta) > 0.0001) {
      let fac = magenta/100.0;

      var P = new CurvesTransformation;
      P.R = [ // x, y
         [0.00000, 0.00000],
         [0.5, 0.5 + 0.4*fac],
         [1.00000, 1.00000]
      ];
      P.Rt = CurvesTransformation.prototype.AkimaSubsplines;
      P.B = [ // x, y
         [0.00000, 0.00000],
         [0.5, 0.5 + 0.4*fac],
         [1.00000, 1.00000]
      ];
      P.Bt = CurvesTransformation.prototype.AkimaSubsplines;

      P.executeOn(view, swapfile);
   }
}


function processRedCurves(view, red, swapfile=true) {

   if (Math.abs(red) > 0.0001) {
      let fac = red/100.0;

      var P = new CurvesTransformation;
      P.R = [ // x, y
         [0.00000, 0.00000],
         [0.5, 0.5 + 0.4*fac],
         [1.00000, 1.00000]
      ];
      P.Rt = CurvesTransformation.prototype.AkimaSubsplines;

      P.executeOn(view, swapfile);
   }
}

function processBlueCurves(view, blue, swapfile=true) {

   if (Math.abs(blue) > 0.0001) {
      let fac = blue/100.0;

      var P = new CurvesTransformation;
      P.B = [ // x, y
         [0.00000, 0.00000],
         [0.5, 0.5 + 0.4*fac],
         [1.00000, 1.00000]
      ];
      P.Bt = CurvesTransformation.prototype.AkimaSubsplines;

      P.executeOn(view, swapfile);
   }
}

function processGreenCurves(view, green, swapfile=true) {

   if (Math.abs(green) > 0.0001) {
      let fac = green/100.0;

      var P = new CurvesTransformation;
      P.G = [ // x, y
         [0.00000, 0.00000],
         [0.5, 0.5 + 0.4*fac],
         [1.00000, 1.00000]
      ];
      P.Gt = CurvesTransformation.prototype.AkimaSubsplines;

      P.executeOn(view, swapfile);
   }
}


function processCyanCurves(view, cyan, swapfile=true) {

   if (Math.abs(cyan) > 0.0001) {
      let fac = cyan/100.0;

      var P = new CurvesTransformation;
      P.G = [ // x, y
         [0.00000, 0.00000],
         [0.5, 0.5 + 0.4*fac],
         [1.00000, 1.00000]
      ];
      P.Gt = CurvesTransformation.prototype.AkimaSubsplines;
      P.B = [ // x, y
         [0.00000, 0.00000],
         [0.5, 0.5 + 0.4*fac],
         [1.00000, 1.00000]
      ];
      P.Bt = CurvesTransformation.prototype.AkimaSubsplines;

      P.executeOn(view, swapfile);
   }
}

function processYellowCurves(view, yellow, swapfile=true) {

   if (Math.abs(yellow) > 0.0001) {
      let fac = yellow/100.0;

      var P = new CurvesTransformation;
      P.R = [ // x, y
         [0.00000, 0.00000],
         [0.5, 0.5 + 0.4*fac],
         [1.00000, 1.00000]
      ];
      P.Rt = CurvesTransformation.prototype.AkimaSubsplines;
      P.G = [ // x, y
         [0.00000, 0.00000],
         [0.5, 0.5 + 0.4*fac],
         [1.00000, 1.00000]
      ];
      P.Gt = CurvesTransformation.prototype.AkimaSubsplines;

      P.executeOn(view, swapfile);
   }
}

function processLuminanceCurves(view, lum, swapfile=true) {

   if (Math.abs(lum) > 0.0001) {
      let fac = lum/100.0;

      var P = new CurvesTransformation;
      P.L = [ // x, y
         [0.00000, 0.00000],
         [0.5, 0.5 + 0.4*fac],
         [1.00000, 1.00000]
      ];
      P.Lt = CurvesTransformation.prototype.AkimaSubsplines;

      P.executeOn(view, swapfile);
   }
}

function processSaturationCurves(view, sat, swapfile=true) {

   if (Math.abs(sat) > 0.0001) {
      let fac = sat/100.0;

      var P = new CurvesTransformation;
      P.S = [ // x, y
         [0.00000, 0.00000],
         [0.25, 0.25 + 0.2*fac],
         [0.5, 0.5 + 0.4*fac],
         [0.75, 0.75 + 0.25*fac],
         [1.00000, 1.00000]
      ];
      P.St = CurvesTransformation.prototype.AkimaSubsplines;

      P.executeOn(view, swapfile);
   }
}

function processContrastCurves(view, contrast, swapfile=true) {

   if (Math.abs(contrast) > 0.0001) {
      let fac = contrast/100.0;

      var P = new CurvesTransformation;
      P.L = [ // x, y
         [0.00000, 0.00000],
         [0.25, 0.25 - 0.2*fac],
         [0.75, 0.75 + 0.2*fac],
         [1.00000, 1.00000]
      ];
      P.Lt = CurvesTransformation.prototype.AkimaSubsplines;

      P.executeOn(view, swapfile);
   }
}

function createRedMask(view) {
   var P = new PixelMath;
   P.expression = "iswitch(H($T)<=0, ~mtf((H($T)+1-(5/6))/(1/6), 0)*CIEc($T),\n" +
   "        H($T)<=(1/6), ~mtf(((1/6)-H($T))/(1/6), 0)* CIEc($T),\n" +
   "        H($T)<(5/6), 0, ~mtf((H($T)-(5/6))/(1/6), 0) * CIEc($T))";
   P.expression1 = "";
   P.expression2 = "";
   P.expression3 = "";
   P.useSingleExpression = true;
   P.symbols = "";
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = true;
   P.rescaleLower = 0;
   P.rescaleUpper = 1;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = true;
   P.showNewImage = false;
   P.newImageId = view.id + "_RedMask_tmp";
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.Gray;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;

   P.executeOn(view, false);

   var maskView = View.viewById(P.newImageId);
   return maskView;
}

function createBlueMask(view) {
   var P = new PixelMath;
   P.expression = "iswitch( H($T)<0.5, 0,\n" +
   "         H($T)<=(2/3), ~mtf((H($T)-0.5)/(1/6),0)*CIEc($T),\n" +
   "         H($T)<=(5/6), ~mtf(((5/6)-H($T))/(1/6), 0)*CIEc($T),\n" +
   "         0)";
   P.expression1 = "";
   P.expression2 = "";
   P.expression3 = "";
   P.useSingleExpression = true;
   P.symbols = "";
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = true;
   P.rescaleLower = 0;
   P.rescaleUpper = 1;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = true;
   P.showNewImage = false;
   P.newImageId = view.id + "_BlueMask_tmp";
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.Gray;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;

   P.executeOn(view, false);

   var maskView = View.viewById(P.newImageId);
   return maskView;
}

function createGreenMask(view) {
   var P = new PixelMath;
   P.expression = "iswitch(H($T)<(1/6), 0,\n" +
   "        H($T)<=(1/3), ~mtf((H($T)-(1/6))/(1/6),0)*CIEc($T),\n" +
   "        H($T)<=0.5, ~mtf((0.5-H($T))/(1/6),0)*CIEc($T),\n" +
   "        0)";
   P.expression1 = "";
   P.expression2 = "";
   P.expression3 = "";
   P.useSingleExpression = true;
   P.symbols = "";
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = true;
   P.rescaleLower = 0;
   P.rescaleUpper = 1;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = true;
   P.showNewImage = false;
   P.newImageId = view.id + "_GreenMask_tmp";
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.Gray;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;

   P.executeOn(view, false);

   var maskView = View.viewById(P.newImageId);
   return maskView;
}

function createYellowMask(view) {
   var P = new PixelMath;
   P.expression = "iswitch(H($T)<0, 0,\n" +
   "        H($T)<=(1/5), ~mtf((H($T)-0)/(1/6),0)*CIEc($T),\n" +
   "        H($T)<=(1/3), ~mtf(((1/3)-H($T))/(1/6),0)*CIEc($T),\n" +
   "        0)";
   P.expression1 = "";
   P.expression2 = "";
   P.expression3 = "";
   P.useSingleExpression = true;
   P.symbols = "";
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = true;
   P.rescaleLower = 0;
   P.rescaleUpper = 1;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = true;
   P.showNewImage = false;
   P.newImageId = view.id + "_YellowMask_tmp";
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.Gray;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;

   P.executeOn(view, false);

   var maskView = View.viewById(P.newImageId);
   return maskView;
}

function createCyanMask(view) {
   var P = new PixelMath;
   P.expression = "iswitch(H($T)<(1/3),0,\n" +
   "        H($T)<=0.5, ~mtf((H($T)-(1/3))/(1/6), 0)* CIEc($T),\n" +
   "        H($T)<=(2/3), ~mtf(((2/3)-H($T))/(1/6), 0)*CIEc($T),\n" +
   "        0)";
   P.expression1 = "";
   P.expression2 = "";
   P.expression3 = "";
   P.useSingleExpression = true;
   P.symbols = "";
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = true;
   P.rescaleLower = 0;
   P.rescaleUpper = 1;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = true;
   P.showNewImage = false;
   P.newImageId = view.id + "_CyanMask_tmp";
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.Gray;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;

   P.executeOn(view, false);

   var maskView = View.viewById(P.newImageId);
   return maskView;
}

function createLowSaturationMask(view) {
   var P = new PixelMath;
   P.expression = "rescale((1-Sv($T))*CIEL($T), 0, 1)";
   P.useSingleExpression = true;
   P.symbols = "";
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = false;
   P.rescaleLower = 0;
   P.rescaleUpper = 1;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = true;
   P.showNewImage = false;
   P.newImageId = view.id + "_LowSat_tmp";
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.Gray;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;

   P.executeOn(view, false);

   var maskView = View.viewById(P.newImageId);
   return maskView;
}

function createHighSaturationMask(view) {
   var P = new PixelMath;
   P.expression = "rescale(Sv($T)*CIEL($T), 0, 1)";
   P.useSingleExpression = true;
   P.symbols = "";
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = false;
   P.rescaleLower = 0;
   P.rescaleUpper = 1;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = true;
   P.showNewImage = false;
   P.newImageId = view.id + "_LowSat_tmp";
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.Gray;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;

   P.executeOn(view, false);

   var maskView = View.viewById(P.newImageId);
   return maskView;
}



function createMagentaMask(view) {
   var P = new PixelMath;
   P.expression = "iswitch(H($T)<=0, ~mtf((0-H($T))/(1/6), 0)*CIEc($T),\n" +
   "        H($T)<(2/3), 0,\n" +
   "        H($T)<=(5/6), ~mtf((H($T)-(2/3))/(1/6), 0)*CIEc($T),\n" +
   "        ~mtf((1+0-H($T))/(1/6), 0)*CIEc($T))";
   P.expression1 = "";
   P.expression2 = "";
   P.expression3 = "";
   P.useSingleExpression = true;
   P.symbols = "";
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = true;
   P.rescaleLower = 0;
   P.rescaleUpper = 1;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = true;
   P.showNewImage = false;
   P.newImageId = view.id + "_MagentaMask_tmp";
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.Gray;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;

   P.executeOn(view, false);

   var maskView = View.viewById(P.newImageId);
   return maskView;
}

function filterShadows(view) {
   var P = new PixelMath;
   P.expression = "iif($T>0.8*med($T), (1.0 - $T)*(1.0 - $T), 0.5*$T)";
   P.useSingleExpression = true;
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = false;
   P.rescaleLower = 0;
   P.rescaleUpper = 1;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = false;
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.Gray;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
   P.executeOn(view, false);
}

function filterMidtones(view) {
   var P = new PixelMath;
   P.expression = "iif($T >= 0.4, 1.42*(1-$T)^2, 1.42*$T^2)";
   P.useSingleExpression = true;
   P.symbols = "";
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = false;
   P.rescaleLower = 0;
   P.rescaleUpper = 1;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = false;
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.Gray;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
   P.executeOn(view, false);
}

function filterHighlights(view) {
   var P = new PixelMath;
   P.expression = "$T*$T";
   P.useSingleExpression = true;
   P.symbols = "";
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = false;
   P.rescaleLower = 0;
   P.rescaleUpper = 1;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = false;
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.Gray;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
   P.executeOn(view, false);
}

function createLuminanceMask(view, mode) {
   var P = new PixelMath;
   P.expression = "CIEL($T)";
   P.expression1 = "";
   P.expression2 = "";
   P.expression3 = "";
   P.useSingleExpression = true;
   P.symbols = "";
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = true;
   P.rescaleLower = 0;
   P.rescaleUpper = 1;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = true;
   P.showNewImage = false;
   P.newImageId = view.id + "_LuminanceMask_tmp";
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.Gray;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;

   P.executeOn(view, false);

   var maskView = View.viewById(P.newImageId);

   if (mode == "Shadows") {
      Console.writeln("filter shadows");
      filterShadows(maskView);
   } else if (mode == "Midtones") {
      Console.writeln("filter midtones");
      filterMidtones(maskView);
   } else if (mode == "Highlights") {
      filterHighlights(maskView);
   }


   return maskView;
}

function blurMask(view) {

   Console.writeln("gconv($T, " + selectiveColorParameters.maskSmoothness + ")");

   var P = new PixelMath;
   P.expression = "gconv($T, " + selectiveColorParameters.maskSmoothness + ")";
   P.useSingleExpression = true;
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = true;
   P.rescale = false;
   P.rescaleLower = 0;
   P.rescaleUpper = 1;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = false;

   P.newImageColorSpace = PixelMath.prototype.Gray;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;

   P.executeOn(view, false);
}

function convertImportedMask(view) {
   if (!selectiveColorParameters.importedMask) {
      importMask();
   }

   if (selectiveColorParameters.importedMask) {
      if (view.image.width != selectiveColorParameters.importedMask.width
         || view.image.height != selectiveColorParameters.importedMask.height) {
         var maskImage = new Image(selectiveColorParameters.importedMask.image);
         maskImage = scaleImage(maskImage);
         var w = maskImage.width;
         var h = maskImage.height;
         var n = maskImage.numberOfChannels;
         var cloneWindow = new ImageWindow(w, h, n, maskImage.bitsPerSample,
                                    maskImage.sampleType == SampleType_Real, maskImage.isColor, view.id + "_selectiveColorCorrection_mask" );
         cloneWindow.hide();

         var clonedView = cloneWindow.mainView;
         clonedView.beginProcess( UndoFlag_NoSwapFile );
         clonedView.image.assign( maskImage );
         clonedView.endProcess();

         return clonedView;
      }
   }
   return undefined;
}


function createMaskInternal(view, filterMethod) {
   if (view !== undefined) {

      Console.noteln("creating mask "+filterMethod);

      switch(filterMethod) {
         case FILTER_RED:
            return createRedMask(view);
         case FILTER_GREEN:
            return createGreenMask(view);
         case FILTER_BLUE:
            return createBlueMask(view);
         case FILTER_CYAN:
            return createCyanMask(view);
         case FILTER_MAGENTA:
            return createMagentaMask(view);
         case FILTER_YELLOW:
            return createYellowMask(view);
         case FILTER_LOWSATURATION:
            return createLowSaturationMask(view);
         case FILTER_HIGHSATURATION:
            return createHighSaturationMask(view);
         case FILTER_SHADOWS:
            return createLuminanceMask(view, filterMethod);
         case FILTER_MIDTONES:
            return createLuminanceMask(view, filterMethod);
         case FILTER_HIGHLIGHTS:
            return createLuminanceMask(view, filterMethod);
         case FILTER_LUMINANCE:
            return createLuminanceMask(view, filterMethod);
         case FILTER_IMPORTEDMASK:
            return convertImportedMask(view);
      }
   }
   return undefined;
}

function modifyMask(view, swapfile=false) {

   if (selectiveColorParameters.filterMethod != FILTER_IMPORTEDMASK) {

      if (Math.abs(selectiveColorParameters.maskShadows) > 0.0 || Math.abs(selectiveColorParameters.maskHighlights) < 1.0) {
         Console.writeln("MaskShadows: " + selectiveColorParameters.maskShadows);
         Console.writeln("MaskHighlights: " + selectiveColorParameters.maskHighlights);

         var P = new CurvesTransformation;
         P.K = [ // x, y
               [0.00000, 0.00000],
               [Math.min(0.999, selectiveColorParameters.maskShadows + 0.00001), 0.00001],
               [1.00000, selectiveColorParameters.maskHighlights]
            ];
         P.Kt = CurvesTransformation.prototype.AkimaSubsplines;
         P.executeOn(view, swapfile);
      }

      if (Math.abs(selectiveColorParameters.maskBalance - 0.5) > 0.001) {
         Console.writeln("MaskBalance: " + selectiveColorParameters.maskBalance);

         var C = new CurvesTransformation;
         C.K = [ // x, y
               [0.00000, 0.00000],
               [selectiveColorParameters.maskBalance, Math.min(0.9, selectiveColorParameters.maskBalance + (1.0 - selectiveColorParameters.maskBalance) * 0.25)],
               [1.00000, 1.00000]
            ];
         C.Kt = CurvesTransformation.prototype.AkimaSubsplines;
         C.executeOn(view, swapfile);

      }

      blurMask(view);
   }
}


function createMask(view, filterMethod) {
   if (view !== undefined) {
      var mask = createMaskInternal(view, filterMethod);

      if (selectiveColorParameters.useStarMask) {
         if (selectiveColorParameters.starMask == undefined) {
            selectiveColorParameters.starMask = createStarMask(view);
         }

         if (selectiveColorParameters.starMask !== undefined) {
            let smClone = clone(selectiveColorParameters.starMask, "_starmask_temp");

            intensifyStarMask(smClone);
            convoluteStarMask(smClone);

            Console.noteln("Subtracting stars from mask...");
            var starlessMask = subtract(mask, smClone, mask.id + "_minusStars");
            mask.window.forceClose();
            smClone.window.forceClose();
            return starlessMask;
         }
      }

      return mask;
   }
   return undefined;
}

function exportCurrentMask() {
   if (selectiveColorParameters.imageView!=undefined) {
      var mask = createMask(selectiveColorParameters.imageView, selectiveColorParameters.filterMethod);
      modifyMask(mask);
      var maskImage = mask.image;
      var cloneWindow = new ImageWindow(maskImage.width, maskImage.height, maskImage.numberOfChannels, maskImage.bitsPerSample,
                                    maskImage.sampleType == SampleType_Real, maskImage.isColor,
                                    selectiveColorParameters.imageView.id + "_" + selectiveColorParameters.filterMethod + "_ExportedMask");

      var clonedView = cloneWindow.mainView;
      clonedView.beginProcess( UndoFlag_NoSwapFile );
      clonedView.image.assign( maskImage );
      clonedView.endProcess();

      mask.window.forceClose();
      cloneWindow.show();
   }
}

function convert(view, filterMethod) {

   var image = view.image;
   var n = image.numberOfChannels;
   var w = image.width;
   var h = image.height;

   if (selectiveColorParameters.mask == undefined) {
      SetRGBWorkingSpace(view);
      if (selectiveColorParameters.filterMethod == FILTER_IMPORTEDMASK)
         selectiveColorParameters.mask = selectiveColorParameters.importedMask;
      else
         selectiveColorParameters.mask = createMask(view, filterMethod);
   }

   if (selectiveColorParameters.mask !== undefined) {

      image.resetRectSelection();

      var cloneWindow = new ImageWindow(w, h, n, image.bitsPerSample,
                                    image.sampleType == SampleType_Real, image.isColor, view.id + "_selectiveColorCorrection_temp" );
      cloneWindow.hide();

      var clonedView = cloneWindow.mainView;
      clonedView.beginProcess( UndoFlag_NoSwapFile );
      clonedView.image.assign( image );
      clonedView.endProcess();

      SetRGBWorkingSpace(clonedView);

      cloneWindow.mask = selectiveColorParameters.mask.window;
      modifyMask(selectiveColorParameters.mask);

      processMagentaCurves(clonedView, selectiveColorParameters.magenta);
      processYellowCurves(clonedView, selectiveColorParameters.yellow);
      processCyanCurves(clonedView, selectiveColorParameters.cyan);
      processRedCurves(clonedView, selectiveColorParameters.red);
      processGreenCurves(clonedView, selectiveColorParameters.green);
      processBlueCurves(clonedView, selectiveColorParameters.blue);

      processContrastCurves(clonedView, selectiveColorParameters.contrast);
      processLuminanceCurves(clonedView, selectiveColorParameters.luminance);
      processSaturationCurves(clonedView, selectiveColorParameters.saturation);

      cloneWindow.removeMask();

      view.beginProcess();
      image.apply( clonedView.image );
      view.endProcess();

      cloneWindow.forceClose();
   } else {
      Console.warningln("No mask created...");
   }
}

function convertPreview(view, filterMethod) {

   var image = view.image;
   var n = image.numberOfChannels;
   var w = image.width;
   var h = image.height;

   Console.noteln("Filter: " + selectiveColorParameters.filterMethod);

   if (selectiveColorParameters.maskPreview == undefined) {
      Console.noteln("creating preview mask...");
      selectiveColorParameters.maskPreview = createMask(view, filterMethod);
      selectiveColorParameters.usedMaskInPreview = clone(selectiveColorParameters.maskPreview, view.id+"_showMask");
   }

   if (selectiveColorParameters.maskPreview !== undefined) {

      var cloneWindow = new ImageWindow(w, h, n, image.bitsPerSample,
                                    image.sampleType == SampleType_Real, image.isColor, view.id + "_selectiveColorCorrection_temp" );
      cloneWindow.hide();

      var clonedView = cloneWindow.mainView;
      clonedView.beginProcess( UndoFlag_NoSwapFile );
      clonedView.image.assign( image );
      clonedView.endProcess();

      SetRGBWorkingSpace(clonedView);

      var tempMask = clone(selectiveColorParameters.maskPreview, view.id+"_temp_mask_"+new Date().getTime());

      modifyMask(tempMask);

      selectiveColorParameters.usedMaskInPreview.beginProcess(UndoFlag_NoSwapFile);
      selectiveColorParameters.usedMaskInPreview.image.assign(tempMask.image);
      selectiveColorParameters.usedMaskInPreview.endProcess();

      cloneWindow.setMask(tempMask.window);
      cloneWindow.maskEnabled = true;
      cloneWindow.maskVisible = true;

      processMagentaCurves(clonedView, selectiveColorParameters.magenta, false);
      processYellowCurves(clonedView, selectiveColorParameters.yellow, false);
      processCyanCurves(clonedView, selectiveColorParameters.cyan, false);
      processRedCurves(clonedView, selectiveColorParameters.red, false);
      processGreenCurves(clonedView, selectiveColorParameters.green, false);

      processBlueCurves(clonedView, selectiveColorParameters.blue, false);

      processContrastCurves(clonedView, selectiveColorParameters.contrast, false);
      processLuminanceCurves(clonedView, selectiveColorParameters.luminance, false);
      processSaturationCurves(clonedView, selectiveColorParameters.saturation, false);

      cloneWindow.removeMask();

      view.beginProcess( UndoFlag_NoSwapFile );
      image.apply( clonedView.image );
      view.endProcess();

      cloneWindow.forceClose();
      tempMask.window.forceClose();
   } else {
      Console.warningln("No mask created...");
   }
}


function closeView(view) {
   if (view !== undefined && view.window != null && !view.window.isNull) {

      if (selectiveColorParameters.importedMask) {
         if (view.id == selectiveColorParameters.importedMask.id)
            return;
      }

      try {
         view.window.forceClose();
      }
      catch(error) {
         Console.writeln("could not close view...");
      }
   }
}

function SetRGBWorkingSpace(view) {

   var P = new RGBWorkingSpace;
   P.channels = [ // Y, x, y
      [1.0, 0.648431, 0.330856],
      [1.0, 0.321152, 0.597871],
      [1.0, 0.155886, 0.066044]
   ];
   P.gamma = 2.20;
   P.sRGBGamma = true;
   P.applyGlobalRGBWS = false;
   P.executeOn(view);
}

function process() {
   if (selectiveColorParameters.imageView!=undefined) {
      Console.noteln("applying color changes...");
      SetRGBWorkingSpace(selectiveColorParameters.imageView);
      convert(selectiveColorParameters.imageView, selectiveColorParameters.filterMethod);
      Console.noteln("applying color changes finished...");
   }
}

function processPreview() {

   if (selectiveColorParameters.imageView!=undefined
      && selectiveColorParameters.imageView != null) {
      Console.noteln("preparing preview...");
      var previewImage = new Image(selectiveColorParameters.imageView.image);
      previewImage = scaleImage(previewImage);

      var prevImage = selectiveColorParameters.previewImage;

      var previewWindow = new ImageWindow(previewImage.width, previewImage.height, previewImage.numberOfChannels, selectiveColorParameters.imageView.image.bitsPerSample,
                                 selectiveColorParameters.imageView.image.sampleType == SampleType_Real,
                                 previewImage.isColor, selectiveColorParameters.imageView.id + "_scc_preview_internal" );

      previewWindow.hide();

      try {

         var view = previewWindow.mainView;
         view.beginProcess( UndoFlag_NoSwapFile );
         view.image.assign( previewImage );
         view.endProcess();

         SetRGBWorkingSpace(view);

         convertPreview(view, selectiveColorParameters.filterMethod);

         previewImage.assign(view.image);
         selectiveColorParameters.previewImage = previewImage;

      } finally {
         prevImage.free();
         previewWindow.forceClose();
         Console.noteln("preparing preview finished...");
      }
   }
}

function undoLastAppliedProcessing() {
   if (selectiveColorParameters.imageView != undefined) {
      selectiveColorParameters.imageView.window.undo();
   }
}

function ImportMaskDialog() {
   this.__base__ = Dialog
   this.__base__();
   this.scaledMinWidth = 300;
   this.scaledMaxWidth = 300;
   this.scaledMinHeight = 120;
   this.scaledMaxHeight = 120;
   this.userResizable = false;

   this.maskSelectorFrame = new Frame(this);
   this.maskSelectorFrame.scaledMinWidth = 300;
   this.maskSelectorFrame.scaledMaxWidth = 300;
   this.maskSelectLabel = new Label(this);
   this.maskSelectLabel.text = "Mask:";
   this.maskSelectLabel.toolTip = "<p>Select the mask.</p>";
   this.maskSelectLabel.textAlignment = TextAlign_Left|TextAlign_VertCenter;
   this.maskSelectLabel.margin = 8;

   this.maskSelector = new ViewList(this);
   this.maskSelector.scaledMaxWidth = 250;
   this.maskSelector.getMainViews();

   with(this.maskSelectorFrame) {
      sizer = new HorizontalSizer();

      with(sizer) {
         margin = 8;
         add(this.maskSelectLabel);
         add(this.maskSelector);
      }
   }

   this.maskSelector.onViewSelected = function(view) {
      if (view) {
         if (view.image.isGrayscale) {
            if (view.image.width != selectiveColorParameters.imageView.image.width
               || view.image.height != selectiveColorParameters.imageView.image.height) {
               Console.noteln("No filter imported: different image dimensions (" + view.image.width +", " + view.image.height+")");
               this.dialog.maskSelector.remove(view);
               return;
            }

            this.dialog.maskSelector.currentView = view;
            selectiveColorParameters.importedMask = view;
         }
         else {
            Console.noteln("No filter imported: Grayscale image required");
            this.dialog.maskSelector.remove(view);
         }
      }
   }

   this.buttonFrame = new Frame(this);
   this.buttonFrame.scaledMinWidth = 300;
   this.buttonFrame.scaledMaxWidth = 300;

   this.okButton = new PushButton(this);
   this.okButton.text = "OK";
   this.cancelButton = new PushButton(this);
   this.cancelButton.text = "Cancel";

   with(this.buttonFrame) {
      sizer = new HorizontalSizer();

      with(sizer) {
         margin = 6;
         addSpacing(8);
         add(this.okButton);
         addStretch();
         add(this.cancelButton);
         addSpacing(8);
      }
   }

   this.cancelButton.onClick = () => {
      this.cancel();
   };

   this.okButton.onClick = () => {
      if (this.dialog.maskSelector.currentView) {
         selectiveColorParameters.importedMask = this.dialog.maskSelector.currentView;
         selectiveColorParameters.filterMethod = FILTER_IMPORTEDMASK;

         if (selectiveColorParameters.mask) {
            closeView(selectiveColorParameters.mask);
            selectiveColorParameters.mask = undefined;
         }
         if (selectiveColorParameters.maskPreview) {
            closeView(selectiveColorParameters.maskPreview);
            selectiveColorParameters.maskPreview = undefined;
         }

         if (selectiveColorParameters.usedMaskInPreview) {
            closeView(selectiveColorParameters.usedMaskInPreview);
            selectiveColorParameters.usedMaskInPreview = undefined;
         }

         Console.noteln(selectiveColorParameters.filterMethod + ": " + selectiveColorParameters.importedMask.id);
      } else {
         Console.noteln("No filter imported");
      }


      this.ok();
   };

   this.sizer = new VerticalSizer();
   this.sizer.addSpacing(16);
   this.sizer.add(this.maskSelectorFrame);
   this.sizer.addSpacing(16);
   this.sizer.add(this.buttonFrame);
   this.sizer.addSpacing(8);
   this.adjustToContents();

}

ImportMaskDialog.prototype = new Dialog;

function importMask() {
   let dialog = new ImportMaskDialog();
   dialog.execute();
}



function SelectiveColorCorrectionDialog() {
   this.__base__ = Dialog
   this.__base__();
   this.scaledMinWidth = 1280;
   this.scaledMinHeight = 800;

   let labelMinWidth = Math.round(this.font.width("Luminance:M"));
   let sliderMinWidth = 270;

   this.helpLabel = new Label( this );
   this.helpLabel.scaledMinWidth = LEFTWIDTH;
   this.helpLabel.scaledMaxWidth = LEFTWIDTH;
   this.helpLabel.frameStyle = FrameStyle_Box;
   this.helpLabel.margin = 4;
   this.helpLabel.wordWrapping = true;
   this.helpLabel.useRichText = true;
   this.helpLabel.text = "<b>" + TITLE + " v"
                         + VERSION;

   // Select the image to be procressed
   this.targetViewSelectorFrame = new Frame(this);
   this.targetViewSelectorFrame.scaledMinWidth = LEFTWIDTH;
   this.targetViewSelectorFrame.scaledMaxWidth = LEFTWIDTH;
   this.targetViewSelectorFrame.backgroundColor = 0xFFD8D7D3;
   this.targetViewSelectLabel = new Label(this);
   this.targetViewSelectLabel.text = "Target View:";
   this.targetViewSelectLabel.toolTip = "<p>Select the non-linear image to be processed.</p>";
   this.targetViewSelectLabel.textAlignment = TextAlign_Left|TextAlign_VertCenter;

   this.targetViewSelector = new ViewList(this);
   this.targetViewSelector.scaledMaxWidth = 350;
   this.targetViewSelector.getMainViews();

   with(this.targetViewSelectorFrame) {
      sizer = new HorizontalSizer();

      with(sizer) {
         margin = 6;
         add(this.targetViewSelectLabel);
         addSpacing(8);
         add(this.targetViewSelector);
         adjustToContents();
      }
   }

   this.targetViewSelector.onViewSelected = function(view) {
      this.dialog.previewTimer.stop();
      selectiveColorParameters.imageView = view;
      if (view.isNull)
         selectiveColorParameters.imageView = undefined;

      this.dialog.showOriginalImageButton.enabled = false;
      this.dialog.importMaskButton.enabled = false;
      this.dialog.exportMaskButton.enabled = false;

      if (selectiveColorParameters.previewOriginal) {
         selectiveColorParameters.previewOriginal.free();
         selectiveColorParameters.previewOriginal = undefined;
      }

      if (selectiveColorParameters.mask) {
         closeView(selectiveColorParameters.mask);
         selectiveColorParameters.mask = undefined;
      }
      if (selectiveColorParameters.maskPreview) {
         closeView(selectiveColorParameters.maskPreview);
         selectiveColorParameters.maskPreview = undefined;
      }

      this.dialog.showOriginalImageButton.icon = this.scaledResource(":/process-interface/real-time-active.png");
      selectiveColorParameters.showOriginalInPreview = false;

      if (selectiveColorParameters.usedMaskInPreview) {
         closeView(selectiveColorParameters.usedMaskInPreview);
         selectiveColorParameters.usedMaskInPreview = undefined;
      }

      if (selectiveColorParameters.imageView && selectiveColorParameters.imageView.id) {
         if (selectiveColorParameters.previewImage) {
            selectiveColorParameters.previewImage.free();
         }

         //SetRGBWorkingSpace(selectiveColorParameters.imageView);

         var previewImage = new Image(selectiveColorParameters.imageView.image);
         previewImage = scaleImage(previewImage);
         var metadata = {
            width: previewImage.width,
            height: previewImage.height
         }

         this.dialog.previewControl.SetPreview(previewImage, selectiveColorParameters.imageView, metadata);
         this.dialog.previewControl.zoomToFit();
         selectiveColorParameters.previewImage = previewImage;

         this.dialog.showOriginalImageButton.enabled = true;
         this.dialog.importMaskButton.enabled = true;
         this.dialog.exportMaskButton.enabled = true;

      } else {
         if (selectiveColorParameters.previewImage) {
            selectiveColorParameters.previewImage.free();
         }

         selectiveColorParameters.previewImage = undefined;
         this.dialog.previewControl.NoImage = true;
         this.dialog.importMaskButton.enabled = false;
         this.dialog.exportMaskButton.enabled = false;

         this.dialog.previewControl.forceRedraw();
      }

      this.dialog.previewTimer.previewKey = "..";
      this.dialog.previewTimer.start();
   }

   this.previewModeFrame = new Frame(this);
   this.previewModeFrame.scaledMinWidth = LEFTWIDTH;
   this.previewModeFrame.scaledMaxWidth = LEFTWIDTH;
   this.previewModeFrame.sizer = new HorizontalSizer;
   this.previewModeFrame.sizer.textAlignment = TextAlign_Left | TextAlign_VertCenter;
   this.previewModeFrame.sizer.margin = 6;
   this.previewModeFrame.backgroundColor = 0xFFD8D7D3;
   this.previewModeFrameLabel = new Label(this);
   this.previewModeFrameLabel.textAlignment = TextAlign_Left | TextAlign_VertCenter;
   this.previewModeFrameLabel.text = "Preview Mode:";
   this.previewModeFrameLabel.toolTip = "<p>Select the mode for the preview. A small preview will be processed fast while a large preview will show more details but takes more time to be processed.</p>";

   this.previewModeSelector = new ComboBox(this);
   this.previewModeSelector.toolTip = "<p>Select the mode for the preview. A small preview will be processed fast while a large preview will show more details but takes more time to be processed.</p>";

   this.previewModeSelector.addItem("Small-sized Preview (fast)");
   this.previewModeSelector.addItem("Medium-sized Preview");
   this.previewModeSelector.addItem("Large-sized Preview (slow)");
   this.previewModeSelector.currentItem = 0;

   this.previewModeSelector.onItemSelected = function(index) {
      this.dialog.previewTimer.stop();

      if (selectiveColorParameters.previewImage) {
         selectiveColorParameters.previewImage.free();
      }

      if (selectiveColorParameters.previewOriginal) {
         selectiveColorParameters.previewOriginal.free();
         selectiveColorParameters.previewOriginal = undefined;
      }

      this.dialog.showOriginalImageButton.icon = this.scaledResource(":/process-interface/real-time-active.png");
      selectiveColorParameters.showOriginalInPreview = false;

      if (selectiveColorParameters.mask) {
         closeView(selectiveColorParameters.mask);
         selectiveColorParameters.mask = undefined;
      }

      if (selectiveColorParameters.maskPreview) {
         closeView(selectiveColorParameters.maskPreview);
         selectiveColorParameters.maskPreview = undefined;
      }

      if (selectiveColorParameters.usedMaskInPreview) {
         closeView(selectiveColorParameters.usedMaskInPreview);
         selectiveColorParameters.usedMaskInPreview = undefined;
      }

      selectiveColorParameters.previewMode = index;
      var previewImage = new Image(selectiveColorParameters.imageView.image);
      previewImage = scaleImage(previewImage);
      var metadata = {
         width: previewImage.width,
         height: previewImage.height
      }

      this.dialog.previewControl.SetPreview(previewImage, selectiveColorParameters.imageView, metadata);
      this.dialog.previewControl.zoomToFit();
      selectiveColorParameters.previewImage = previewImage;
      this.dialog.previewTimer.previewKey = "....";
      this.dialog.previewTimer.start();
   };

   this.previewModeFrame.sizer.add(this.previewModeFrameLabel);
   this.previewModeFrame.sizer.addStretch();
   this.previewModeFrame.sizer.add(this.previewModeSelector);

   this.maskBar = new SectionBar(this, "Mask");
   this.maskBar.scaledMinWidth = LEFTWIDTH;
   this.maskBar.scaledMaxWidth = LEFTWIDTH;
   this.maskControl = new Control(this);
   this.maskControl.scaledMinWidth = LEFTWIDTH;
   this.maskControl.scaledMaxWidth = LEFTWIDTH;

   this.maskBar.onToggleSection =  function( section, toggleBegin ) {
      if ( !toggleBegin ) {
         section.dialog.adjustToContents();
         section.dialog.setVariableSize();
      }
   }

   this.colorSelectLabel = new Label(this);
   this.colorSelectLabel.text = "Mask:";
   this.colorSelectLabel.toolTip = "<p>Select the color or luminance range.</p>";
   this.colorSelectLabel.textAlignment = TextAlign_Left|TextAlign_VertCenter;

   this.colorSelector = new ComboBox(this);
   this.colorSelector.scaledMaxWidth = 350;
   this.colorSelector.toolTip = "<p>Select the color or luminance range.</p>";

   this.colorSelector.addItem(FILTER_RED);
   this.colorSelector.addItem(FILTER_GREEN);
   this.colorSelector.addItem(FILTER_BLUE);
   this.colorSelector.addItem(FILTER_CYAN);
   this.colorSelector.addItem(FILTER_MAGENTA);
   this.colorSelector.addItem(FILTER_YELLOW);
   this.colorSelector.addItem(FILTER_LOWSATURATION);
   this.colorSelector.addItem(FILTER_HIGHSATURATION);
   this.colorSelector.addItem(FILTER_SHADOWS);
   this.colorSelector.addItem(FILTER_MIDTONES);
   this.colorSelector.addItem(FILTER_HIGHLIGHTS);
   this.colorSelector.addItem(FILTER_LUMINANCE);
   this.colorSelector.addItem(FILTER_IMPORTEDMASK);

   this.colorSelector.currentItem = this.colorSelector.findItem(selectiveColorParameters.filterMethod);

   this.colorSelector.onItemSelected = function(index) {
      this.dialog.previewTimer.stop();

      selectiveColorParameters.filterMethod = this.itemText(index);
      if (selectiveColorParameters.filterMethod == FILTER_IMPORTEDMASK) {
         selectiveColorParameters.filterMethod = FILTER_RED;
         this.colorSelector.currentItem = this.colorSelector.findItem(selectiveColorParameters.filterMethod);
      }

      if (selectiveColorParameters.mask) {
         closeView(selectiveColorParameters.mask);
         selectiveColorParameters.mask = undefined;
      }

      if (selectiveColorParameters.maskPreview) {
         closeView(selectiveColorParameters.maskPreview);
         selectiveColorParameters.maskPreview = undefined;
      }

      if (selectiveColorParameters.usedMaskInPreview) {
         closeView(selectiveColorParameters.usedMaskInPreview);
         selectiveColorParameters.usedMaskInPreview = undefined;
      }

      this.dialog.maskShadowsSlider.enabled = true;
      this.dialog.resetShadowsButton.enabled = true;
      this.dialog.maskHighlightsSlider.enabled = true;
      this.dialog.resetHighlightsButton.enabled = true;
      this.dialog.maskBalanceSlider.enabled = true;
      this.dialog.resetBalanceButton.enabled = true;
      this.dialog.maskSmoothnessSlider.enabled = true;
      this.dialog.resetMaskSmoothnessButton.enabled = true;
      this.dialog.starMaskIntensitySlider.enabled = true;
      this.dialog.resetStarMaskIntensityButton.enabled = true;


      this.dialog.showOriginalImageButton.icon = this.scaledResource(":/process-interface/real-time-active.png");
      selectiveColorParameters.showOriginalInPreview = false;

      selectiveColorParameters.cyan = 0.0;
      selectiveColorParameters.magenta = 0.0;
      selectiveColorParameters.yellow = 0.0;
      selectiveColorParameters.red = 0.0;
      selectiveColorParameters.green = 0.0;
      selectiveColorParameters.blue = 0.0;
      selectiveColorParameters.luminance = 0.0;
      selectiveColorParameters.saturation = 0.0;
      selectiveColorParameters.contrast = 0.0;
      selectiveColorParameters.maskShadows = 0.0;
      selectiveColorParameters.maskHighlights = 1.0;
      selectiveColorParameters.maskBalance = 0.5;
      this.dialog.cyanSlider.setValue(selectiveColorParameters.cyan);
      this.dialog.magentaSlider.setValue(selectiveColorParameters.magenta);
      this.dialog.yellowSlider.setValue(selectiveColorParameters.yellow);
      this.dialog.blackSlider.setValue(0.0);
      this.dialog.redSlider.setValue(selectiveColorParameters.red);
      this.dialog.greenSlider.setValue(selectiveColorParameters.green);
      this.dialog.blueSlider.setValue(selectiveColorParameters.blue);
      this.dialog.satSlider.setValue(selectiveColorParameters.saturation);
      this.dialog.contrastSlider.setValue(selectiveColorParameters.contrast);
      this.dialog.maskShadowsSlider.setValue(selectiveColorParameters.maskShadows);
      this.dialog.maskHighlightsSlider.setValue(selectiveColorParameters.maskHighlights);
      this.dialog.maskBalanceSlider.setValue(0.5);

      this.dialog.previewTimer.previewKey = "Mask changed...";
      this.dialog.previewTimer.start();
   }

   this.maskShadowsSlider = new NumericControl(this);
   this.maskShadowsSlider.label.text = "Shadows: ";
   this.maskShadowsSlider.label.minWidth = labelMinWidth;
   this.maskShadowsSlider.label.textAlignment = TextAlign_Right|TextAlign_VertCenter;
   this.maskShadowsSlider.toolTip = "<p>Reduce the shadows in the mask.</p>";
   this.maskShadowsSlider.setRange(0.0, 1.0);
   this.maskShadowsSlider.slider.setRange(0.0, 100000.0);
   this.maskShadowsSlider.slider.scaledMinWidth = sliderMinWidth;
   this.maskShadowsSlider.setPrecision(4);
   this.maskShadowsSlider.setReal(true);
   this.maskShadowsSlider.exponential = true;
   this.maskShadowsSlider.setValue(selectiveColorParameters.maskShadows);

   this.maskShadowsSlider.onValueUpdated = function(t) {
      this.dialog.previewTimer.stop();
      selectiveColorParameters.maskShadows = t;
      this.dialog.previewTimer.start();
   };

   this.resetShadowsButton = new ToolButton( this );
   this.resetShadowsButton.icon = this.scaledResource( ":/icons/clear-inverted.png" );
   this.resetShadowsButton.setScaledFixedSize( 24, 24 );
   this.resetShadowsButton.toolTip = "<p>Reset the masks shadows.</p>";
   this.resetShadowsButton.onClick = () => {
      let timerWasRunning = this.dialog.previewTimer.isRunning;
      this.dialog.previewTimer.stop();
      this.maskShadowsSlider.setValue(0.0);
      selectiveColorParameters.maskShadows = 0.0;
      if (timerWasRunning) this.dialog.previewTimer.start();
   }

   this.maskHighlightsSlider = new NumericControl(this);
   this.maskHighlightsSlider.label.text = "Highlights: ";
   this.maskHighlightsSlider.label.minWidth = labelMinWidth;
   this.maskHighlightsSlider.label.textAlignment = TextAlign_Right|TextAlign_VertCenter;
   this.maskHighlightsSlider.toolTip = "<p>Reduce the highlights in the mask.</p>";
   this.maskHighlightsSlider.setRange(0.0, 1.0);
   this.maskHighlightsSlider.slider.setRange(0.0, 100000.0);
   this.maskHighlightsSlider.slider.scaledMinWidth = sliderMinWidth;
   this.maskHighlightsSlider.setPrecision(4);
   this.maskHighlightsSlider.setReal(true);
   this.maskHighlightsSlider.exponential = true;
   this.maskHighlightsSlider.setValue(selectiveColorParameters.maskHighlights);

   this.maskHighlightsSlider.onValueUpdated = function(t) {
      this.dialog.previewTimer.stop();
      selectiveColorParameters.maskHighlights = t;
      this.dialog.previewTimer.start();
   };

   this.resetHighlightsButton = new ToolButton( this );
   this.resetHighlightsButton.icon = this.scaledResource( ":/icons/clear-inverted.png" );
   this.resetHighlightsButton.setScaledFixedSize( 24, 24 );
   this.resetHighlightsButton.toolTip = "<p>Reset the masks highlights.</p>";
   this.resetHighlightsButton.onClick = () => {
      let timerWasRunning = this.dialog.previewTimer.isRunning;
      this.dialog.previewTimer.stop();
      this.maskHighlightsSlider.setValue(1.0);
      selectiveColorParameters.maskHighlights = 1.0;
      if (timerWasRunning) this.dialog.previewTimer.start();
   }

   this.maskBalanceSlider = new NumericControl(this);
   this.maskBalanceSlider.real = true;
   this.maskBalanceSlider.label.text = "Balance: ";
   this.maskBalanceSlider.label.minWidth = labelMinWidth;
   this.maskBalanceSlider.label.textAlignment = TextAlign_Right|TextAlign_VertCenter;
   this.maskBalanceSlider.toolTip = "<p>Balance the mask intensity.</p>";
   this.maskBalanceSlider.setRange(0.1, 0.9);
   this.maskBalanceSlider.slider.setRange(0.0, 100000.0);
   this.maskBalanceSlider.slider.scaledMinWidth = sliderMinWidth;
   this.maskBalanceSlider.setPrecision(4);
   this.maskBalanceSlider.setReal(true);
   this.maskBalanceSlider.exponential = true;
   this.maskBalanceSlider.setValue(selectiveColorParameters.maskBalance);

   this.maskBalanceSlider.onValueUpdated = function(t) {
      this.dialog.previewTimer.stop();
      selectiveColorParameters.maskBalance = t;
      this.dialog.previewTimer.start();
   };

   this.resetBalanceButton = new ToolButton( this );
   this.resetBalanceButton.icon = this.scaledResource( ":/icons/clear-inverted.png" );
   this.resetBalanceButton.setScaledFixedSize( 24, 24 );
   this.resetBalanceButton.toolTip = "<p>Reset the masks highlights.</p>";
   this.resetBalanceButton.onClick = () => {
      let timerWasRunning = this.dialog.previewTimer.isRunning;
      this.dialog.previewTimer.stop();
      this.maskBalanceSlider.setValue(0.5);
      selectiveColorParameters.maskBalance = 0.5;
      if (timerWasRunning) this.dialog.previewTimer.start();
   }

   this.maskSmoothnessSlider = new NumericControl(this);
   this.maskSmoothnessSlider.real = true;
   this.maskSmoothnessSlider.label.text = "Smoothness: ";
   this.maskSmoothnessSlider.label.minWidth = labelMinWidth;
   this.maskSmoothnessSlider.label.textAlignment = TextAlign_Right|TextAlign_VertCenter;
   this.maskSmoothnessSlider.toolTip = "<p>Change the smoothness of the mask.</p>";
   this.maskSmoothnessSlider.setRange(1.0, 100.0);
   this.maskSmoothnessSlider.slider.setRange(0.0, 100000.0);
   this.maskSmoothnessSlider.slider.scaledMinWidth = sliderMinWidth;
   this.maskSmoothnessSlider.setPrecision(4);
   this.maskSmoothnessSlider.setReal(true);
   this.maskSmoothnessSlider.exponential = true;
   this.maskSmoothnessSlider.setValue(selectiveColorParameters.maskSmoothness);

   this.maskSmoothnessSlider.onValueUpdated = function(t) {
      this.dialog.previewTimer.stop();
      selectiveColorParameters.maskSmoothness = t;
      this.dialog.previewTimer.start();
   };

   this.resetMaskSmoothnessButton = new ToolButton( this );
   this.resetMaskSmoothnessButton.icon = this.scaledResource( ":/icons/clear-inverted.png" );
   this.resetMaskSmoothnessButton.setScaledFixedSize( 24, 24 );
   this.resetMaskSmoothnessButton.toolTip = "<p>Reset the masks highlights.</p>";
   this.resetMaskSmoothnessButton.onClick = () => {
      let timerWasRunning = this.dialog.previewTimer.isRunning;
      this.dialog.previewTimer.stop();
      this.maskSmoothnessSlider.setValue(10);
      selectiveColorParameters.maskSmoothness = 10;
      if (timerWasRunning) this.dialog.previewTimer.start();
   }

   this.createStarMaskCheckbox = new CheckBox(this);
   this.createStarMaskCheckbox.text = "Create and use a star mask";
   this.createStarMaskCheckbox.checked = selectiveColorParameters.useStarMask;
   this.createStarMaskCheckbox.toolTip = "<p>Create a starmask for the image to avoid affecting the stars.</p>"

   this.createStarMaskCheckbox.onCheck = function(checked) {
      let timerWasRunning = this.dialog.previewTimer.isRunning;
      this.dialog.previewTimer.stop();
      selectiveColorParameters.useStarMask = checked;
      if (!checked) {
         if (selectiveColorParameters.starMask) {
            selectiveColorParameters.starMask.window.forceClose();
            selectiveColorParameters.starMask = undefined;
         }
      }

      if (selectiveColorParameters.mask) {
         closeView(selectiveColorParameters.mask);
         selectiveColorParameters.mask = undefined;
      }

      if (selectiveColorParameters.maskPreview) {
         closeView(selectiveColorParameters.maskPreview);
         selectiveColorParameters.maskPreview = undefined;
      }

      if (selectiveColorParameters.usedMaskInPreview) {
         closeView(selectiveColorParameters.usedMaskInPreview);
         selectiveColorParameters.usedMaskInPreview = undefined;
      }

      this.dialog.starMaskIntensitySlider.enabled = checked;
      this.dialog.resetStarMaskIntensityButton.enabled = checked;

      if (timerWasRunning) this.dialog.previewTimer.start();
   }

   this.showMaskCheckbox = new CheckBox(this);
   this.showMaskCheckbox.text = "Show mask";
   this.showMaskCheckbox.checked = selectiveColorParameters.showMask;
   this.showMaskCheckbox.toolTip = "<p>Previews the mask.</p>"
   this.showMaskCheckbox.onCheck = function(checked) {
      selectiveColorParameters.showMask = checked;
      this.dialog.previewControl.forceRedraw();
   }

   this.starMaskIntensitySlider = new NumericControl(this);
   this.starMaskIntensitySlider.real = true;
   this.starMaskIntensitySlider.label.text = "Intensity: ";
   this.starMaskIntensitySlider.label.minWidth = labelMinWidth;
   this.starMaskIntensitySlider.label.textAlignment = TextAlign_Right|TextAlign_VertCenter;
   this.starMaskIntensitySlider.toolTip = "<p>Star Mask intensity.</p>";
   this.starMaskIntensitySlider.setRange(1.0, 3.0);
   this.starMaskIntensitySlider.slider.setRange(1.0, 30000.0);
   this.starMaskIntensitySlider.slider.scaledMinWidth = sliderMinWidth;
   this.starMaskIntensitySlider.setPrecision(4);
   this.starMaskIntensitySlider.setReal(true);
   this.starMaskIntensitySlider.setValue(selectiveColorParameters.starMaskIntensity);
   this.starMaskIntensitySlider.enabled = false;

   this.starMaskIntensitySlider.onValueUpdated = function(t) {
      this.dialog.previewTimer.stop();
      selectiveColorParameters.starMaskIntensity = t;

      if (selectiveColorParameters.mask) {
         closeView(selectiveColorParameters.mask);
         selectiveColorParameters.mask = undefined;
      }
      if (selectiveColorParameters.maskPreview) {
         closeView(selectiveColorParameters.maskPreview);
         selectiveColorParameters.maskPreview = undefined;
      }

      if (selectiveColorParameters.usedMaskInPreview) {
         closeView(selectiveColorParameters.usedMaskInPreview);
         selectiveColorParameters.usedMaskInPreview = undefined;
      }

      this.dialog.previewTimer.start();
   };

   this.resetStarMaskIntensityButton = new ToolButton( this );
   this.resetStarMaskIntensityButton.icon = this.scaledResource( ":/icons/clear-inverted.png" );
   this.resetStarMaskIntensityButton.setScaledFixedSize( 24, 24 );
   this.resetStarMaskIntensityButton.toolTip = "<p>Reset the star mask intensity.</p>";
   this.resetStarMaskIntensityButton.enabled = false;
   this.resetStarMaskIntensityButton.onClick = () => {
      let timerWasRunning = this.dialog.previewTimer.isRunning;
      this.dialog.previewTimer.stop();
      this.starMaskIntensitySlider.setValue(1.8);
      selectiveColorParameters.starMaskIntensity = 1.8;
      if (timerWasRunning) this.dialog.previewTimer.start();
   }

   this.exportMaskButton = new PushButton(this);
   this.exportMaskButton.enabled = false;
   this.exportMaskButton.text = "Export Mask";
   this.exportMaskButton.toolTip = "<p>Export the mask image.</p>";
   this.exportMaskButton.onClick = () => {
      let timerWasRunning = this.dialog.previewTimer.isRunning;
      this.dialog.previewTimer.stop();
      exportCurrentMask();
      if (timerWasRunning) this.dialog.previewTimer.start();
   }

   this.importMaskButton = new PushButton(this);
   this.importMaskButton.enabled = false;
   this.importMaskButton.text = "Import Mask";
   this.importMaskButton.toolTip = "<p>Import the mask image.</p>";
   this.importMaskButton.onClick = () => {
      let timerWasRunning = this.dialog.previewTimer.isRunning;
      this.dialog.previewTimer.stop();
      importMask();
      if (selectiveColorParameters.filterMethod == FILTER_IMPORTEDMASK) {
         this.dialog.colorSelector.currentItem = this.colorSelector.findItem(selectiveColorParameters.filterMethod);

         this.dialog.maskShadowsSlider.enabled = false;
         this.dialog.resetShadowsButton.enabled = false;
         this.dialog.maskHighlightsSlider.enabled = false;
         this.dialog.resetHighlightsButton.enabled = false;
         this.dialog.maskBalanceSlider.enabled = false;
         this.dialog.resetBalanceButton.enabled = false;
         this.dialog.maskSmoothnessSlider.enabled = false;
         this.dialog.resetMaskSmoothnessButton.enabled = false;
         this.dialog.starMaskIntensitySlider.enabled = false;
         this.dialog.resetStarMaskIntensityButton.enabled = false;

      }
      if (timerWasRunning) this.dialog.previewTimer.start();
   }


   with(this.maskControl) {
      sizer = new VerticalSizer();
      sizer.textAlignment = TextAlign_Left|TextAlign_VertCenter;
      backgroundColor = 0xFFD8D7D3;

      with(sizer) {
         margin = 6;

         this.maskhorizontal = new HorizontalSizer();
         this.maskhorizontal.add(this.colorSelectLabel);
         this.maskhorizontal.add(this.colorSelector);
         add(this.maskhorizontal);
         addSpacing(8);

         this.maskhorizontal2 = new HorizontalSizer();
         this.maskhorizontal2.add(this.maskShadowsSlider);
         this.maskhorizontal2.add(this.resetShadowsButton);
         add(this.maskhorizontal2);

         this.maskhorizontal3 = new HorizontalSizer();
         this.maskhorizontal3.add(this.maskHighlightsSlider);
         this.maskhorizontal3.add(this.resetHighlightsButton);
         add(this.maskhorizontal3);

         this.maskhorizontal4 = new HorizontalSizer();
         this.maskhorizontal4.add(this.maskBalanceSlider);
         this.maskhorizontal4.add(this.resetBalanceButton);
         add(this.maskhorizontal4);

         this.maskhorizontal7 = new HorizontalSizer();
         this.maskhorizontal7.add(this.maskSmoothnessSlider);
         this.maskhorizontal7.add(this.resetMaskSmoothnessButton);
         add(this.maskhorizontal7);
         addSpacing(8);

         this.maskhorizontal5 = new HorizontalSizer();
         this.maskhorizontal5.add(this.createStarMaskCheckbox);
         this.maskhorizontal5.addStretch();
         this.maskhorizontal5.add(this.showMaskCheckbox);
         add(this.maskhorizontal5);
         addSpacing(8);

         this.maskhorizontal6 = new HorizontalSizer();
         this.maskhorizontal6.add(this.starMaskIntensitySlider);
         this.maskhorizontal6.addStretch();
         this.maskhorizontal6.add(this.resetStarMaskIntensityButton);
         add(this.maskhorizontal6);
         addSpacing(8);

         this.maskhorizontal8 = new HorizontalSizer();
         this.maskhorizontal8.add(this.importMaskButton);
         this.maskhorizontal8.addStretch();
         this.maskhorizontal8.add(this.exportMaskButton);
         add(this.maskhorizontal8);

         adjustToContents();
      }
   }

   this.maskBar.setSection( this.maskControl );



   this.correctionModeFrame = new Frame(this);
   this.correctionModeFrame.scaledMinWidth = LEFTWIDTH;
   this.correctionModeFrame.scaledMaxWidth = LEFTWIDTH;
   this.correctionModeFrame.backgroundColor = 0xFFD8D7D3;
   this.correctionModeFrame.sizer = new HorizontalSizer;
   this.correctionModeFrame.sizer.textAlignment = TextAlign_Left | TextAlign_VertCenter;
   this.correctionModeFrame.sizer.margin = 6;
   this.correctionModeFrameLabel = new Label(this);
   this.correctionModeFrameLabel.textAlignment = TextAlign_Left | TextAlign_VertCenter;
   this.correctionModeFrameLabel.text = "Change range:";
   this.correctionModeFrameLabel.toolTip = "<p>Select the range of the color-, saturation- and luminance sliders. Use 'Full' for the full value range and 'Small' for very small changes (only 10% of the range).</p>";

   this.correctionModeList = new ComboBox(this);
   this.correctionModeList.scaledMaxWidth = LEFTWIDTH-60;
   this.correctionModeList.toolTip = "<p>Select the range of the color-, saturation- and luminance sliders. Use 'Full' for the full value range and 'Small' for very small changes (only 10% of the range). This will also reset the values!</p>";

   this.correctionModeList.addItem("Full");
   this.correctionModeList.addItem("Small");

   this.correctionModeFrame.sizer.add(this.correctionModeFrameLabel);
   this.correctionModeFrame.sizer.addStretch();
   this.correctionModeFrame.sizer.add(this.correctionModeList);

   this.correctionModeList.onItemSelected = function(index) {
      this.dialog.previewTimer.stop();

      this.dialog.cyanSlider.setValue(0.0);
      selectiveColorParameters.cyan = 0.0;
      this.dialog.magentaSlider.setValue(0.0);
      selectiveColorParameters.magenta = 0.0;
      this.dialog.yellowSlider.setValue(0.0);
      selectiveColorParameters.yellow = 0.0;
      this.dialog.redSlider.setValue(0.0);
      selectiveColorParameters.red = 0.0;
      this.dialog.greenSlider.setValue(0.0);
      selectiveColorParameters.green = 0.0;
      this.dialog.blueSlider.setValue(0.0);
      selectiveColorParameters.blue = 0.0;
      this.dialog.blackSlider.setValue(0.0);
      selectiveColorParameters.luminance = 0.0;
      this.dialog.satSlider.setValue(0.0);
      selectiveColorParameters.saturation = 0.0;
      this.dialog.contrastSlider.setValue(0.0);
      selectiveColorParameters.contrast = 0.0;

      if (this.itemText(index) == "Full") {
         this.dialog.cyanSlider.setRange(-100.0, 100.0);
         this.dialog.magentaSlider.setRange(-100.0, 100.0);
         this.dialog.yellowSlider.setRange(-100.0, 100.0);
         this.dialog.redSlider.setRange(-100.0, 100.0);
         this.dialog.greenSlider.setRange(-100.0, 100.0);
         this.dialog.blueSlider.setRange(-100.0, 100.0);
         this.dialog.blackSlider.setRange(-100.0, 100.0);
         this.dialog.satSlider.setRange(-100.0, 100.0);
         this.dialog.contrastSlider.setRange(-100.0, 100.0);
      } else {
         this.dialog.cyanSlider.setRange(-10.0, 10.0);
         this.dialog.magentaSlider.setRange(-10.0, 10.0);
         this.dialog.yellowSlider.setRange(-10.0, 10.0);
         this.dialog.redSlider.setRange(-10.0, 10.0);
         this.dialog.greenSlider.setRange(-10.0, 10.0);
         this.dialog.blueSlider.setRange(-10.0, 10.0);
         this.dialog.blackSlider.setRange(-25.0, 25.0);
         this.dialog.satSlider.setRange(-25.0, 25.0);
         this.dialog.contrastSlider.setRange(-25.0, 25.0);
      }

      this.dialog.previewTimer.previewKey = "..";
      this.dialog.previewTimer.start();
   }

   this.complementaryBar = new SectionBar(this, "Complementary colors");
   this.complementaryBar.scaledMinWidth = LEFTWIDTH;
   this.complementaryBar.scaledMaxWidth = LEFTWIDTH;
   this.complementaryControl = new Control(this);
   this.complementaryControl.scaledMinWidth = LEFTWIDTH;
   this.complementaryControl.scaledMaxWidth = LEFTWIDTH;

   this.complementaryBar.onToggleSection =  function( section, toggleBegin ) {
      if ( !toggleBegin ) {
         section.dialog.adjustToContents();
         section.dialog.setVariableSize();
      }
   }

   this.complementaryBar.setSection( this.complementaryControl );

   this.rgbBar = new SectionBar(this, "RGB Colors");
   this.rgbBar.scaledMinWidth = LEFTWIDTH;
   this.rgbBar.scaledMaxWidth = LEFTWIDTH;
   this.rgbControl = new Control(this);
   this.rgbControl.scaledMinWidth = LEFTWIDTH;
   this.rgbControl.scaledMaxWidth = LEFTWIDTH;

   this.rgbBar.onToggleSection =  function( section, toggleBegin ) {
      if ( !toggleBegin ) {
         section.dialog.adjustToContents();
         section.dialog.setVariableSize();
      }
   }

   this.rgbBar.setSection( this.rgbControl );

   this.lumSatBar = new SectionBar(this, "Luminance, Saturation and Contrast");
   this.lumSatBar.scaledMinWidth = LEFTWIDTH;
   this.lumSatBar.scaledMaxWidth = LEFTWIDTH;
   this.lumSatControl = new Control(this);
   this.lumSatControl.scaledMinWidth = LEFTWIDTH;
   this.lumSatControl.scaledMaxWidth = LEFTWIDTH;

   this.lumSatBar.onToggleSection =  function( section, toggleBegin ) {
      if ( !toggleBegin ) {
         section.dialog.adjustToContents();
         section.dialog.setVariableSize();
      }
   }

   this.lumSatBar.setSection( this.lumSatControl );

   this.cyanSlider = new NumericControl(this);
   this.cyanSlider.label.text = "Cyan: ";
   this.cyanSlider.label.minWidth = labelMinWidth;
   this.cyanSlider.label.textAlignment = TextAlign_Right|TextAlign_VertCenter;
   this.cyanSlider.toolTip = "<p>Increase/Decrease amount of cyan.</p>";
   this.cyanSlider.setRange(-100.0, 100.0);
   this.cyanSlider.slider.setRange(-100.0, 100.0);
   this.cyanSlider.slider.scaledMinWidth = sliderMinWidth;
   this.cyanSlider.setPrecision(2);
   this.cyanSlider.setReal(true);
   this.cyanSlider.setValue(selectiveColorParameters.cyan);

   this.cyanSlider.onValueUpdated = function(t) {
      this.dialog.previewTimer.stop();
      selectiveColorParameters.cyan = t;
      this.dialog.previewTimer.start();
   };

   this.resetCyanButton = new ToolButton( this );
   this.resetCyanButton.icon = this.scaledResource( ":/icons/clear-inverted.png" );
   this.resetCyanButton.setScaledFixedSize( 24, 24 );
   this.resetCyanButton.toolTip = "<p>Reset the cyan.</p>";
   this.resetCyanButton.onClick = () => {
      let timerWasRunning = this.dialog.previewTimer.isRunning;
      this.dialog.previewTimer.stop();
      this.dialog.cyanSlider.setValue(0.0);
      selectiveColorParameters.cyan = 0.0;
      if (timerWasRunning) this.dialog.previewTimer.start();
   }

   this.magentaSlider = new NumericControl(this);
   this.magentaSlider.label.text = "Magenta: ";
   this.magentaSlider.label.minWidth = labelMinWidth;
   this.magentaSlider.label.textAlignment = TextAlign_Right|TextAlign_VertCenter;
   this.magentaSlider.toolTip = "<p>Increase/Decrease amount of magenta.</p>";
   this.magentaSlider.setRange(-100.0, 100.0);
   this.magentaSlider.slider.setRange(-100.0, 100.0);
   this.magentaSlider.slider.scaledMinWidth = sliderMinWidth;
   this.magentaSlider.setPrecision(2);
   this.magentaSlider.setReal(true);
   this.magentaSlider.setValue(selectiveColorParameters.magenta);

   this.magentaSlider.onValueUpdated = function(t) {
      this.dialog.previewTimer.stop();
      selectiveColorParameters.magenta = t;
      this.dialog.previewTimer.start();
   };

   this.resetMagentaButton = new ToolButton( this );
   this.resetMagentaButton.icon = this.scaledResource( ":/icons/clear-inverted.png" );
   this.resetMagentaButton.setScaledFixedSize( 24, 24 );
   this.resetMagentaButton.toolTip = "<p>Reset the magenta.</p>";
   this.resetMagentaButton.onClick = () => {
      let timerWasRunning = this.dialog.previewTimer.isRunning;
      this.dialog.previewTimer.stop();
      this.dialog.magentaSlider.setValue(0.0);
      selectiveColorParameters.magenta = 0.0;
      if (timerWasRunning) this.dialog.previewTimer.start();
   }

   this.yellowSlider = new NumericControl(this);
   this.yellowSlider.label.text = "Yellow: ";
   this.yellowSlider.label.minWidth = labelMinWidth;
   this.yellowSlider.label.textAlignment = TextAlign_Right|TextAlign_VertCenter;
   this.yellowSlider.toolTip = "<p>Increase/Decrease amount of yellow.</p>";
   this.yellowSlider.setRange(-100.0, 100.0);
   this.yellowSlider.slider.setRange(-100.0, 100.0);
   this.yellowSlider.slider.scaledMinWidth = sliderMinWidth;
   this.yellowSlider.setPrecision(2);
   this.yellowSlider.setReal(true);
   this.yellowSlider.setValue(selectiveColorParameters.yellow);

   this.yellowSlider.onValueUpdated = function(t) {
      this.dialog.previewTimer.stop();
      selectiveColorParameters.yellow = t;
      this.dialog.previewTimer.start();
   };

   this.resetYellowButton = new ToolButton( this );
   this.resetYellowButton.icon = this.scaledResource( ":/icons/clear-inverted.png" );
   this.resetYellowButton.setScaledFixedSize( 24, 24 );
   this.resetYellowButton.toolTip = "<p>Reset the yellow.</p>";
   this.resetYellowButton.onClick = () => {
      let timerWasRunning = this.dialog.previewTimer.isRunning;
      this.dialog.previewTimer.stop();
      this.dialog.yellowSlider.setValue(0.0);
      selectiveColorParameters.yellow = 0.0;
      if (timerWasRunning) this.dialog.previewTimer.start();
   }

   this.redSlider = new NumericControl(this);
   this.redSlider.label.text = "Red: ";
   this.redSlider.label.minWidth = labelMinWidth;
   this.redSlider.label.textAlignment = TextAlign_Right|TextAlign_VertCenter;
   this.redSlider.toolTip = "<p>Increase/Decrease amount of red.</p>";
   this.redSlider.setRange(-100.0, 100.0);
   this.redSlider.slider.setRange(-100.0, 100.0);
   this.redSlider.slider.scaledMinWidth = sliderMinWidth;
   this.redSlider.setPrecision(2);
   this.redSlider.setReal(true);
   this.redSlider.setValue(selectiveColorParameters.red);

   this.redSlider.onValueUpdated = function(t) {
      this.dialog.previewTimer.stop();
      selectiveColorParameters.red = t;
      this.dialog.previewTimer.start();
   };

   this.resetRedButton = new ToolButton( this );
   this.resetRedButton.icon = this.scaledResource( ":/icons/clear-inverted.png" );
   this.resetRedButton.setScaledFixedSize( 24, 24 );
   this.resetRedButton.toolTip = "<p>Reset the red.</p>";
   this.resetRedButton.onClick = () => {
      let timerWasRunning = this.dialog.previewTimer.isRunning;
      this.dialog.previewTimer.stop();
      this.dialog.redSlider.setValue(0.0);
      selectiveColorParameters.red = 0.0;
      if (timerWasRunning) this.dialog.previewTimer.start();
   }

   this.greenSlider = new NumericControl(this);
   this.greenSlider.label.text = "Green: ";
   this.greenSlider.label.minWidth = labelMinWidth;
   this.greenSlider.label.textAlignment = TextAlign_Right|TextAlign_VertCenter;
   this.greenSlider.toolTip = "<p>Increase/Decrease amount of green.</p>";
   this.greenSlider.setRange(-100.0, 100.0);
   this.greenSlider.slider.setRange(-100.0, 100.0);
   this.greenSlider.slider.scaledMinWidth = sliderMinWidth;
   this.greenSlider.setPrecision(2);
   this.greenSlider.setReal(true);
   this.greenSlider.setValue(selectiveColorParameters.green);

   this.greenSlider.onValueUpdated = function(t) {
      this.dialog.previewTimer.stop();
      selectiveColorParameters.green = t;
      this.dialog.previewTimer.start();
   };

   this.resetGreenButton = new ToolButton( this );
   this.resetGreenButton.icon = this.scaledResource( ":/icons/clear-inverted.png" );
   this.resetGreenButton.setScaledFixedSize( 24, 24 );
   this.resetGreenButton.toolTip = "<p>Reset the green.</p>";
   this.resetGreenButton.onClick = () => {
      let timerWasRunning = this.dialog.previewTimer.isRunning;
      this.dialog.previewTimer.stop();
      this.dialog.greenSlider.setValue(0.0);
      selectiveColorParameters.green = 0.0;
      if (timerWasRunning) this.dialog.previewTimer.start();
   }

   this.blueSlider = new NumericControl(this);
   this.blueSlider.label.text = "Blue: ";
   this.blueSlider.label.minWidth = labelMinWidth;
   this.blueSlider.label.textAlignment = TextAlign_Right|TextAlign_VertCenter;
   this.blueSlider.toolTip = "<p>Increase/Decrease amount of blue.</p>";
   this.blueSlider.setRange(-100.0, 100.0);
   this.blueSlider.slider.setRange(-100.0, 100.0);
   this.blueSlider.slider.scaledMinWidth = sliderMinWidth;
   this.blueSlider.setPrecision(2);
   this.blueSlider.setReal(true);
   this.blueSlider.setValue(selectiveColorParameters.blue);

   this.blueSlider.onValueUpdated = function(t) {
      this.dialog.previewTimer.stop();
      selectiveColorParameters.blue = t;
      this.dialog.previewTimer.start();
   };

   this.resetBlueButton = new ToolButton( this );
   this.resetBlueButton.icon = this.scaledResource( ":/icons/clear-inverted.png" );
   this.resetBlueButton.setScaledFixedSize( 24, 24 );
   this.resetBlueButton.toolTip = "<p>Reset the blue.</p>";
   this.resetBlueButton.onClick = () => {
      let timerWasRunning = this.dialog.previewTimer.isRunning;
      this.dialog.previewTimer.stop();
      this.dialog.blueSlider.setValue(0.0);
      selectiveColorParameters.blue = 0.0;
      if (timerWasRunning) this.dialog.previewTimer.start();
   }

   this.blackSlider = new NumericControl(this);
   this.blackSlider.label.text = "Luminance: ";
   this.blackSlider.label.minWidth = labelMinWidth;
   this.blackSlider.label.textAlignment = TextAlign_Right|TextAlign_VertCenter;
   this.blackSlider.toolTip = "<p>Increase/Decrease the luminance.</p>";
   this.blackSlider.setRange(-100.0, 100.0);
   this.blackSlider.slider.setRange(-100.0, 100.0);
   this.blackSlider.slider.scaledMinWidth = sliderMinWidth;
   this.blackSlider.setPrecision(2);
   this.blackSlider.setReal(true);
   this.blackSlider.setValue(selectiveColorParameters.luminance);

   this.blackSlider.onValueUpdated = function(t) {
      this.dialog.previewTimer.stop();
      selectiveColorParameters.luminance = t;
      this.dialog.previewTimer.start();
   };

   this.resetBlackButton = new ToolButton( this );
   this.resetBlackButton.icon = this.scaledResource( ":/icons/clear-inverted.png" );
   this.resetBlackButton.setScaledFixedSize( 24, 24 );
   this.resetBlackButton.toolTip = "<p>Reset the luminance.</p>";
   this.resetBlackButton.onClick = () => {
      let timerWasRunning = this.dialog.previewTimer.isRunning;
      this.dialog.previewTimer.stop();
      this.dialog.blackSlider.setValue(0.0);
      selectiveColorParameters.luminance = 0.0;
      if (timerWasRunning) this.dialog.previewTimer.start();
   }

   this.satSlider = new NumericControl(this);
   this.satSlider.label.text = "Saturation: ";
   this.satSlider.label.minWidth = labelMinWidth;
   this.satSlider.label.textAlignment = TextAlign_Right|TextAlign_VertCenter;
   this.satSlider.toolTip = "<p>Increase/Decrease the color saturation.</p>";
   this.satSlider.setRange(-100.0, 100.0);
   this.satSlider.slider.setRange(-100.0, 100.0);
   this.satSlider.slider.scaledMinWidth = sliderMinWidth;
   this.satSlider.setPrecision(2);
   this.satSlider.setReal(true);
   this.satSlider.setValue(selectiveColorParameters.saturation);

   this.satSlider.onValueUpdated = function(t) {
      this.dialog.previewTimer.stop();
      selectiveColorParameters.saturation = t;
      this.dialog.previewTimer.start();
   };

   this.resetSaturationButton = new ToolButton( this );
   this.resetSaturationButton.icon = this.scaledResource( ":/icons/clear-inverted.png" );
   this.resetSaturationButton.setScaledFixedSize( 24, 24 );
   this.resetSaturationButton.toolTip = "<p>Reset the saturation.</p>";
   this.resetSaturationButton.onClick = () => {
      let timerWasRunning = this.dialog.previewTimer.isRunning;
      this.dialog.previewTimer.stop();
      this.dialog.satSlider.setValue(0.0);
      selectiveColorParameters.saturation = 0.0;
      if (timerWasRunning) this.dialog.previewTimer.start();
   }

   this.contrastSlider = new NumericControl(this);
   this.contrastSlider.label.text = "Contrast: ";
   this.contrastSlider.label.minWidth = labelMinWidth;
   this.contrastSlider.label.textAlignment = TextAlign_Right|TextAlign_VertCenter;
   this.contrastSlider.toolTip = "<p>Increase/Decrease the contrast.</p>";
   this.contrastSlider.setRange(-100.0, 100.0);
   this.contrastSlider.slider.setRange(-100.0, 100.0);
   this.contrastSlider.slider.scaledMinWidth = sliderMinWidth;
   this.contrastSlider.setPrecision(2);
   this.contrastSlider.setReal(true);
   this.contrastSlider.setValue(selectiveColorParameters.contrast);

   this.contrastSlider.onValueUpdated = function(t) {
      this.dialog.previewTimer.stop();
      selectiveColorParameters.contrast = t;
      this.dialog.previewTimer.start();
   };

   this.resetContrastButton = new ToolButton( this );
   this.resetContrastButton.icon = this.scaledResource( ":/icons/clear-inverted.png" );
   this.resetContrastButton.setScaledFixedSize( 24, 24 );
   this.resetContrastButton.toolTip = "<p>Reset the contrast.</p>";
   this.resetContrastButton.onClick = () => {
      let timerWasRunning = this.dialog.previewTimer.isRunning;
      this.dialog.previewTimer.stop();
      this.dialog.contrastSlider.setValue(0.0);
      selectiveColorParameters.contrast = 0.0;
      if (timerWasRunning) this.dialog.previewTimer.start();
   }



   with(this.complementaryControl) {
      sizer = new VerticalSizer();
      backgroundColor = 0xFFD8D7D3;

      with(sizer) {
         margin = 6;

         this.cyanSizer = new HorizontalSizer();
         this.cyanSizer.add(this.cyanSlider);
         this.cyanSizer.add(this.resetCyanButton);
         add(this.cyanSizer);
         addSpacing(4);

         this.magentaSizer = new HorizontalSizer();
         this.magentaSizer.add(this.magentaSlider);
         this.magentaSizer.add(this.resetMagentaButton);
         add(this.magentaSizer);
         addSpacing(4);

         this.yellowSizer = new HorizontalSizer();
         this.yellowSizer.add(this.yellowSlider);
         this.yellowSizer.add(this.resetYellowButton);
         add(this.yellowSizer);
         addSpacing(16);
         adjustToContents();
      }
   }

   with(this.rgbControl) {
      sizer = new VerticalSizer();
      backgroundColor = 0xFFD8D7D3;

      with(sizer) {
         margin = 6;
         this.redSizer = new HorizontalSizer();
         this.redSizer.add(this.redSlider);
         this.redSizer.add(this.resetRedButton);
         add(this.redSizer);
         addSpacing(4);

         this.greenSizer = new HorizontalSizer();
         this.greenSizer.add(this.greenSlider);
         this.greenSizer.add(this.resetGreenButton);
         add(this.greenSizer);
         addSpacing(4);

         this.blueSizer = new HorizontalSizer();
         this.blueSizer.add(this.blueSlider);
         this.blueSizer.add(this.resetBlueButton);
         add(this.blueSizer);
         addSpacing(16);
      }
   }

   with(this.lumSatControl) {
      sizer = new VerticalSizer();
      backgroundColor = 0xFFD8D7D3;

      with(sizer) {
         margin = 6;
         this.blackSizer = new HorizontalSizer();
         this.blackSizer.add(this.blackSlider);
         this.blackSizer.add(this.resetBlackButton);
         add(this.blackSizer);
         addSpacing(4);

         this.satSizer = new HorizontalSizer();
         this.satSizer.add(this.satSlider);
         this.satSizer.add(this.resetSaturationButton);
         add(this.satSizer);

         this.contrastSizer = new HorizontalSizer();
         this.contrastSizer.add(this.contrastSlider);
         this.contrastSizer.add(this.resetContrastButton);
         add(this.contrastSizer);

         adjustToContents();
      }
   }

   this.previewSettingsFrame = new Frame();
   this.previewSettingsFrame.sizer = new HorizontalSizer();
   this.previewSettingsFrame.scaledMinWidth = LEFTWIDTH;
   this.previewSettingsFrame.scaledMaxWidth = LEFTWIDTH;
   this.previewSettingsFrame.sizer.addStretch();

   let previewButtonMinWidth = Math.round(this.font.width(" Preview changed image MMM")) + 40;

   this.showOriginalImageButton = new PushButton( this );
   this.showOriginalImageButton.defaultButton = false;
   this.showOriginalImageButton.text = " Preview changed image ";
   this.showOriginalImageButton.icon = this.scaledResource( ":/process-interface/real-time-active.png" );
   this.showOriginalImageButton.minHeight = 32;
   this.showOriginalImageButton.minWidth = previewButtonMinWidth;
   this.showOriginalImageButton.maxWidth = previewButtonMinWidth;

   this.showOriginalImageButton.toolTip = "<p>Enable to show the unchanged image in the preview for comparing your changes (CTRL+Spacebar).</p>";
   this.showOriginalImageButton.enabled = false;

   this.showOriginalImageButton.onClick = () => {
      if (!selectiveColorParameters.showOriginalInPreview) {
         let previewOriginalImage = new Image(selectiveColorParameters.imageView.image);
         previewOriginalImage = scaleImage(previewOriginalImage);
         this.dialog.showOriginalImageButton.text = " Preview original image";
         this.dialog.showOriginalImageButton.icon = this.scaledResource( ":/process-interface/real-time.png" );


         this.dialog.showOriginalImageButton.minHeight = 32;
         this.dialog.showOriginalImageButton.minWidth = previewButtonMinWidth;
         this.dialog.showOriginalImageButton.maxWidth = previewButtonMinWidth;

         selectiveColorParameters.previewOriginal = previewOriginalImage
         selectiveColorParameters.showOriginalInPreview = true;
      }
      else {
         selectiveColorParameters.showOriginalInPreview = false;
         this.dialog.showOriginalImageButton.text = " Preview changed image ";
         this.dialog.showOriginalImageButton.icon = this.scaledResource( ":/process-interface/real-time-active.png" );

         this.dialog.showOriginalImageButton.minHeight = 32;
         this.dialog.showOriginalImageButton.minWidth = previewButtonMinWidth;
         this.dialog.showOriginalImageButton.maxWidth = previewButtonMinWidth;

         if (selectiveColorParameters.previewOriginal !== undefined) {
            selectiveColorParameters.previewOriginal.free();
            selectiveColorParameters.previewOriginal = undefined;
         }
      }
      this.dialog.previewControl.repaint();
   }

   this.onKeyPress = function(keycode, modifiers) {
      if (keycode == 32 && (modifiers == 24 || modifiers == 16)) { // ctrl+space
         if (!selectiveColorParameters.showOriginalInPreview) {
            let previewOriginalImage = new Image(selectiveColorParameters.imageView.image);
            previewOriginalImage = scaleImage(previewOriginalImage);
            this.dialog.showOriginalImageButton.text = " Preview original image";
            this.dialog.showOriginalImageButton.icon = this.scaledResource( ":/process-interface/real-time.png" );

            this.dialog.showOriginalImageButton.minHeight = 32;
            this.dialog.showOriginalImageButton.minWidth = previewButtonMinWidth;
            this.dialog.showOriginalImageButton.maxWidth = previewButtonMinWidth;

            selectiveColorParameters.previewOriginal = previewOriginalImage
            selectiveColorParameters.showOriginalInPreview = true;
         }
         else {
            selectiveColorParameters.showOriginalInPreview = false;
            this.dialog.showOriginalImageButton.text = " Preview changed image ";
            this.dialog.showOriginalImageButton.icon = this.scaledResource( ":/process-interface/real-time-active.png" );
            this.dialog.showOriginalImageButton.minHeight = 32;
            this.dialog.showOriginalImageButton.minWidth = previewButtonMinWidth;
            this.dialog.showOriginalImageButton.maxWidth = previewButtonMinWidth;

            if (selectiveColorParameters.previewOriginal !== undefined) {
               selectiveColorParameters.previewOriginal.free();
               selectiveColorParameters.previewOriginal = undefined;
            }
         }
         this.previewControl.repaint();
      }

      if (keycode == 13 && modifiers == 4) {
         this.dialog.previewTimer.stop();
         if (this.dialog.previewTimer.busy) return;
         selectiveColorParameters.save();

         process();
         selectiveColorParameters.undoCount += 1;
         this.dialog.undo_Button.enabled = true;
         this.dialog.previewKey = "...";
         this.dialog.previewTimer.start();
      }

      if (keycode == 85 && modifiers == 16) {
         if (selectiveColorParameters.undoCount>0) {
            this.dialog.previewTimer.stop();
            selectiveColorParameters.undoCount -= 1;
            this.dialog.undo_Button.enabled = selectiveColorParameters.undoCount > 0;
            undoLastAppliedProcessing();
            this.dialog.previewKey = "...";
            this.dialog.previewTimer.start();
         }
      }
   }

   this.previewSettingsFrame.sizer.add(this.showOriginalImageButton);
   this.previewSettingsFrame.sizer.addSpacing(8);

   this.buttonFrame = new Frame;
   this.buttonFrame.scaledMinWidth = LEFTWIDTH;
   this.buttonFrame.scaledMaxWidth = LEFTWIDTH;
   this.buttonFrame.sizer = new HorizontalSizer;
   this.buttonFrame.sizer.margin = 6;
   this.buttonFrame.sizer.spacing = 6;

   this.newInstanceButton = new ToolButton( this );
   this.newInstanceButton.icon = this.scaledResource( ":/process-interface/new-instance.png" );
   this.newInstanceButton.setScaledFixedSize( 24, 24 );
   this.newInstanceButton.onMousePress = () => {
      selectiveColorParameters.save();
      this.newInstance();
   }

   this.ok_Button = new ToolButton( this );
   this.ok_Button.text = "Apply";
   this.ok_Button.icon = this.scaledResource( ":/icons/process.png" );
   this.ok_Button.toolTip = "<p>Apply the changes to the target view. The dialog will not be closed! (ALT+Enter)</p>";
   this.ok_Button.onClick = () => {
      this.dialog.previewTimer.stop();
      if (this.dialog.previewTimer.busy) return;
      selectiveColorParameters.save();

      process();
      selectiveColorParameters.undoCount += 1;
      this.dialog.undo_Button.enabled = true;

      this.dialog.previewKey = "...done:.";
      this.dialog.previewTimer.start();
   };

   this.undo_Button = new ToolButton(this);
   this.undo_Button.icon = this.scaledResource( ":/icons/undo.png" );
   this.undo_Button.toolTip = "<p>Undo the last applied change! (CTRL+U)</p>";
   this.undo_Button.onClick = () => {
      if (selectiveColorParameters.undoCount>0) {
         this.dialog.previewTimer.stop();
         selectiveColorParameters.undoCount -= 1;
         this.dialog.undo_Button.enabled = selectiveColorParameters.undoCount > 0;
         undoLastAppliedProcessing();
         this.dialog.resetAll();
         Console.noteln("Undo...");
         this.dialog.previewKey = "..undo: " + selectiveColorParameters.undoCount+1;
         selectiveColorParameters.previewId += 1;
         this.dialog.previewTimer.start();
      }
   };
   this.undo_Button.enabled = false;

   this.reset_Button = new ToolButton( this );
   this.reset_Button.icon = this.scaledResource( ":/images/icons/reset.png" );
   this.reset_Button.setScaledFixedSize( 24, 24 );
   this.reset_Button.toolTip = "<p>Resets all color values, the luminance and saturation. The mask parameters are not changed!</p>";
   this.reset_Button.onClick = () => {
      this.dialog.previewTimer.stop();
      this.dialog.resetAll();

      this.dialog.previewKey = "..reset..";
      this.dialog.previewTimer.start();
   };

   this.cancel_Button = new ToolButton( this );
   this.cancel_Button.icon = this.scaledResource( ":/process-interface/cancel.png" );
   this.cancel_Button.setScaledFixedSize( 24, 24 );
   this.cancel_Button.toolTip = "<p>Close this dialog with no changes (Esc).</p>";
   this.cancel_Button.onClick = () => {
      this.dialog.previewTimer.stop();
      if (this.dialog.previewTimer.busy) return;
      this.cancel();
   };

   this.help_Button = new ToolButton( this );
   this.help_Button.icon = this.scaledResource(":/process-interface/browse-documentation.png");
   this.help_Button.setScaledFixedSize( 24, 24 );
   this.help_Button.toolTip = "<p>Shows the script documentation.</p>";
   this.help_Button.onClick = () => {
      let timerWasRunning = this.dialog.previewTimer.isRunning;
      this.dialog.previewTimer.stop();
      Dialog.browseScriptDocumentation("SelectiveColorCorrection");
      if (timerWasRunning) this.dialog.previewTimer.start();
   };

   this.buttonFrame.sizer.add( this.newInstanceButton );
   this.buttonFrame.sizer.addStretch();
   this.buttonFrame.sizer.add( this.ok_Button );
   this.buttonFrame.sizer.addStretch();
   this.buttonFrame.sizer.add( this.undo_Button );
   this.buttonFrame.sizer.addStretch();
   this.buttonFrame.sizer.add( this.reset_Button );
   this.buttonFrame.sizer.addStretch();
   this.buttonFrame.sizer.add( this.help_Button );

   this.buttonFrame.sizer.addStretch();
   this.buttonFrame.sizer.add( this.cancel_Button );


   this.previewControl = new PreviewControl(this);
   this.previewControl.scaledMinWidth = 800;
   this.previewControl.scaledMinHeight = 600;

   this.previewControl.onCustomScope = this;
   this.previewControl.onCustomPaint = function (graphics, x0, y0, x1, y1) {
      if (selectiveColorParameters.showMask && selectiveColorParameters.usedMaskInPreview !== undefined) {
         graphics.drawBitmap(0, 0, selectiveColorParameters.usedMaskInPreview.image.render());
      } else if (selectiveColorParameters.showOriginalInPreview && selectiveColorParameters.previewOriginal !== undefined) {
         var img = selectiveColorParameters.previewOriginal;
         let bmp = img.render();
         selectiveColorParameters.imageView.window.applyColorTransformation(bmp);
         graphics.drawBitmap(0, 0, bmp);
      }
   }

   this.sizer = new HorizontalSizer;
   this.sizer.margin = 6;
   this.sizer.spacing = 6;

   this.leftSizer = new VerticalSizer;
   this.leftSizer.add(this.helpLabel);
   this.leftSizer.addSpacing(16);
   this.leftSizer.add(this.targetViewSelectorFrame);
   this.leftSizer.add(this.previewModeFrame);
   this.leftSizer.addSpacing(16);
   this.leftSizer.add(this.maskBar);
   this.leftSizer.add(this.maskControl);
   this.leftSizer.addSpacing(16);
   this.leftSizer.add(this.correctionModeFrame);
   this.leftSizer.addSpacing(8);
   this.leftSizer.add(this.complementaryBar);
   this.leftSizer.add(this.complementaryControl);
   this.leftSizer.addSpacing(8);
   this.leftSizer.add(this.rgbBar);
   this.leftSizer.add(this.rgbControl);
   this.leftSizer.addSpacing(8);
   this.leftSizer.add(this.lumSatBar);
   this.leftSizer.add(this.lumSatControl);
   this.leftSizer.addSpacing(16);
   this.leftSizer.add(this.previewSettingsFrame);

   this.leftSizer.addSpacing(32);
   this.leftSizer.addStretch();
   this.leftSizer.add(this.buttonFrame);

   this.sizer.add(this.leftSizer);
   this.sizer.add(this.previewControl);

   this.previewTimer = new Timer();
   this.previewTimer.interval = 1.00;
   this.previewTimer.periodic = true;
   this.previewTimer.dialog = this;
   this.previewTimer.busy = false;
   this.previewTimer.previewKey = "";
   this.previewTimer.updateCount = 0;

   this.resetAll = function() {

      this.cyanSlider.setValue(0.0);
      this.magentaSlider.setValue(0.0);
      this.yellowSlider.setValue(0.0);
      this.blackSlider.setValue(0.0);
      this.redSlider.setValue(0.0);
      this.greenSlider.setValue(0.0);
      this.blueSlider.setValue(0.0);
      this.satSlider.setValue(0.0);
      this.contrastSlider.setValue(0.0);
      selectiveColorParameters.cyan = 0.0;
      selectiveColorParameters.magenta = 0.0;
      selectiveColorParameters.yellow = 0.0;
      selectiveColorParameters.red = 0.0;
      selectiveColorParameters.green = 0.0;
      selectiveColorParameters.blue = 0.0;
      selectiveColorParameters.luminance = 0.0;
      selectiveColorParameters.saturation = 0.0;
      selectiveColorParameters.contrast = 0.0;
   }

   this.previewTimer.onTimeout = function()  {
      let currentPreviewKey = getPreviewKey();
      let needsUpdate = (this.dialog.previewTimer.previewKey != currentPreviewKey);

      if (needsUpdate) {
         if (this.dialog.previewTimer.busy) return;

         this.dialog.targetViewSelector.enabled = false;
         this.dialog.previewModeSelector.enabled = false;
         this.dialog.ok_Button.enabled = false;
         this.dialog.cancel_Button.enabled = false;
         this.dialog.previewTimer.stop();
         this.dialog.previewTimer.busy = true;

         this.dialog.cursor = new Cursor( StdCursor_Hourglass );
         this.dialog.previewControl.cursor = new Cursor( StdCursor_Hourglass );
         try {

            processPreview();

            if (selectiveColorParameters.previewImage) {
               let previewImage = selectiveColorParameters.previewImage;
               var metadata = {
                  width: previewImage.width,
                  height: previewImage.height
               }

               this.dialog.previewControl.SetPreview(previewImage, selectiveColorParameters.imageView, metadata);
               this.dialog.previewControl.forceRedraw();
            }
         }
         catch(error) {
            Console.critical(error);
         }
         finally {
            this.dialog.previewTimer.previewKey = currentPreviewKey;

            this.dialog.previewTimer.busy = false;
            this.dialog.targetViewSelector.enabled = true;
            this.dialog.previewModeSelector.enabled = true;
            this.dialog.ok_Button.enabled = true;
            this.dialog.cancel_Button.enabled = true;

            this.dialog.cursor = new Cursor( StdCursor_Arrow);
            this.dialog.previewControl.cursor = new Cursor( StdCursor_Arrow );
            this.dialog.previewTimer.start();

         }
      }

   }

   if (selectiveColorParameters.imageView) {
      this.targetViewSelector.currentView = selectiveColorParameters.imageView;
      if (selectiveColorParameters.previewImage) {
            selectiveColorParameters.previewImage.free();
      }

      //SetRGBWorkingSpace(selectiveColorParameters.imageView);

      var previewImage = new Image(selectiveColorParameters.imageView.image);
      previewImage = scaleImage(previewImage);
      var metadata = {
         width: previewImage.width,
         height: previewImage.height
      }

      this.dialog.previewControl.SetPreview(previewImage, selectiveColorParameters.imageView, metadata);
      this.dialog.previewControl.zoomToFit();
      selectiveColorParameters.previewImage = previewImage;
      this.dialog.showOriginalImageButton.enabled = true;
      this.dialog.importMaskButton.enabled = true;
      this.dialog.exportMaskButton.enabled = true;
      this.previewTimer.previewKey = "..";
      this.previewTimer.start();
   }

   this.onHide = function() {
      this.previewTimer.stop();
      if (selectiveColorParameters.mask) {
         closeView(selectiveColorParameters.mask);
         selectiveColorParameters.mask = undefined;
      }

      if (selectiveColorParameters.maskPreview) {
         closeView(selectiveColorParameters.maskPreview);
         selectiveColorParameters.maskPreview = undefined;
      }

      if (selectiveColorParameters.usedMaskInPreview) {
         closeView(selectiveColorParameters.usedMaskInPreview);
         selectiveColorParameters.usedMaskInPreview = undefined;
      }

      if (selectiveColorParameters.previewImage) {
         var freeImage = selectiveColorParameters.previewImage;
         selectiveColorParameters.previewImage = undefined;
         freeImage.free();
      }

      if (selectiveColorParameters.starMask) {
         selectiveColorParameters.starMask.window.forceClose();
         selectiveColorParameters.starMask = undefined;
      }
   }

}

SelectiveColorCorrectionDialog.prototype = new Dialog;


function main() {

   // is script started from an instance in global or view target context?
   if (Parameters.isViewTarget) {
      selectiveColorParameters.load();
      selectiveColorParameters.imageView = Parameters.targetView;
      process();
      return;
   } else if (Parameters.isGlobalTarget) {
      let msgBox = new MessageBox("Execution of SelectiveColorCorrection is not supported in a global context!", "Global Execution not supported");
      msgBox.execute();

      return;
   }

   var window = ImageWindow.activeWindow;
   if (!window.isNull) {
      selectiveColorParameters.imageView = window.mainView;
   }

   let dialog = new SelectiveColorCorrectionDialog();
   dialog.execute();

}

main();
