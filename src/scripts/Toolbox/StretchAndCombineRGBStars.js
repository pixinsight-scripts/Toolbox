#include <pjsr/StdIcon.jsh>
#include <pjsr/StdButton.jsh>
#include <pjsr/StdDialogCode.jsh>
#include <pjsr/Sizer.jsh>
#include <pjsr/FrameStyle.jsh>
#include <pjsr/TextAlign.jsh>
#include <pjsr/StdButton.jsh>
#include <pjsr/StdIcon.jsh>
#include <pjsr/ColorSpace.jsh>
#include <pjsr/Interpolation.jsh>
#include <pjsr/UndoFlag.jsh>
#include <pjsr/NumericControl.jsh>
#include <pjsr/SectionBar.jsh>
#include <pjsr/CheckState.jsh>
#include <pjsr/SampleType.jsh>
#include "PixInsightToolsPreviewControl.jsh"

#feature-id StretchAndCombineRGBStars :  Toolbox > StretchAndCombineRGBStars

#feature-info  A script to stretch and combine RGB stars.<br/>\
   <br/>\
   A script to stretch and combine RGB stars. \
   <br/> \
   Copyright &copy; 2024 Juergen Terpe


#define VERSION "1.2"
#define TITLE  "Stretch and combine RGB Stars"
#define TEXT   "Stretch and combine RGB Stars."

#define LEFTWIDTH 400

#define Stretch_DEFAULT 2.0
#define LocalStretch_DEFAULT 2.0
#define ContrastStretch_DEFAULT 0.0
#define Contrast_DEFAULT 0.0
#define Saturation_DEFAULT 1.0
#define SCNR_DEFAULT 0.0
#define BRIGHTNESS_DEFAULT 1.0

var stretchAndCombineRGBStars = {
   imageStars: undefined,
   imageStarless: undefined,
   previewImage: undefined,
   correctStarCores: true,
   createNewImage: true,
   combine: true,
   usePreviews: true,
   preview: undefined,
   stretchFactor: Stretch_DEFAULT,
   localStretchFactor: LocalStretch_DEFAULT,
   contrastStretch: Contrast_DEFAULT,
   contrast: Contrast_DEFAULT,
   saturation: Saturation_DEFAULT,
   scnrGreenValue: SCNR_DEFAULT,
   starBrightness: BRIGHTNESS_DEFAULT,

   save: function() {
      Parameters.set("CorrectStarCores", stretchAndCombineRGBStars.correctStarCores);
      Parameters.set("CreateNewImage", stretchAndCombineRGBStars.createNewImage);
      Parameters.set("Combine", stretchAndCombineRGBStars.combine);
      Parameters.set("Stretch", stretchAndCombineRGBStars.stretchFactor);
      Parameters.set("LocalStretch", stretchAndCombineRGBStars.localStretchFactor);
      Parameters.set("ContrastStretch", stretchAndCombineRGBStars.contrastStretch);
      Parameters.set("Contrast", stretchAndCombineRGBStars.contrast);
      Parameters.set("Saturation", stretchAndCombineRGBStars.saturation);
      Parameters.set("SCNRG", stretchAndCombineRGBStars.scnrGreenValue);
      Parameters.set("Brightness", stretchAndCombineRGBStars.starBrightness);
   },

   load: function() {
      if (Parameters.has("CorrectStarCores"))
         stretchAndCombineRGBStars.correctStarCores = Parameters.getBoolean("CorrectStarCores");
      if (Parameters.has("CreateNewImage"))
         stretchAndCombineRGBStars.createNewImage = Parameters.getBoolean("CreateNewImage");
      if (Parameters.has("Combine"))
         stretchAndCombineRGBStars.combine = Parameters.getBoolean("Combine");

      if (Parameters.has("Stretch"))
         stretchAndCombineRGBStars.stretchFactor = Parameters.getReal("Stretch");
      if (Parameters.has("LocalStretch"))
         stretchAndCombineRGBStars.localStretchFactor = Parameters.getReal("LocalStretch");
      if (Parameters.has("ContrastStretch"))
         stretchAndCombineRGBStars.contrastStretch = Parameters.getReal("ContrastStretch");
      if (Parameters.has("Contrast"))
         stretchAndCombineRGBStars.contrast = Parameters.getReal("Contrast");
      if (Parameters.has("Saturation"))
         stretchAndCombineRGBStars.saturation = Parameters.getReal("Saturation");
      if (Parameters.has("SCNRG"))
         stretchAndCombineRGBStars.scnrGreenValue = Parameters.getReal("SCNRG");
      if (Parameters.has("Brightness"))
         stretchAndCombineRGBStars.starBrightness = Parameters.getReal("Brightness");

   }
}

function closeView(view) {
   if (view!=null && view!=undefined && view.window!=null && view.window!=undefined) {
      try {
         //Console.writeln("closing view: "+view.id);
         view.window.forceClose();
      }
      catch(error) {
         Console.writeln("could not close view..." + error);
      }
   }
}

function closePreview() {
   if (stretchAndCombineRGBStars.previewImage != undefined && stretchAndCombineRGBStars.previewImage != null && stretchAndCombineRGBStars.previewImage.id.length > 0) {
      closeView(stretchAndCombineRGBStars.previewImage);
      stretchAndCombineRGBStars.previewImage = undefined;
   }
}


function cloneHidden(view, postfix, swapfile=true) {
   var P = new PixelMath;
   P.expression = "$T";
   P.useSingleExpression = true;
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = false;
   P.truncate = true;
   P.createNewImage = true;
   P.showNewImage = false;
   P.newImageId = view.id + postfix;
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.RGB;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
   P.executeOn(view, swapfile);

   return View.viewById(P.newImageId);
}



function starSaturation(view, amount) {
   var P = new ColorSaturation;
   P.HS = [ // x, y
      [0.00000, 0.2*amount],
      [1.00000, 0.2*amount]
   ];
   P.HSt = ColorSaturation.prototype.AkimaSubsplines;
   P.hueShift = 0.000;

   P.executeOn(view);
}

function arcsinStretch(view, amount) {

   if (amount > 0.0) {
      var P = new ArcsinhStretch;
      P.stretch = 2.50 * amount;
      P.blackPoint = 0.000000;
      P.protectHighlights = true;
      P.useRGBWS = true;

      P.executeOn(view);
   }
}


function starStretch(view, amount, b, c) {
   if (amount<0.0001) {
      amount = 0.0001;
   }

   if (b<0.0001) {
      b = 0.0001;
   }

   var P = new PixelMath;
   P.expression = "D = "+(amount * 250.0)+"; b = "+ b + "; HP= 0.95;\n\n"
      + "SP= med($T)*"+ c + ";\n"
      + "q0=  (1+(D*b)*SP)^(-1/b);\n"
      + "qWP= 2-(1+(D*b)*(HP-SP))^(-1/b);\n"
      + "q1=  2-2*(1+(D*b)*(HP-SP))^(-1/b)+(1+(D*b)*(2*HP-SP-1))^(-1/b);\n"
      + "iif($T<SP, (1+(D*b)*(SP-$T))^(-1/b)-q0,\n"
      + "    2-(1+(D*b)*($T-SP))^(-1/b)-q0 )\n"
      + "    /(q1-q0);\n";
   P.useSingleExpression = true;
   P.symbols = "D,b,SP,HP,q0,qWP,q1";
   P.optimization = true;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = false;

   P.executeOn(view);
}

function repairStarCores(view, threshold = 0.25) {

   var image = view.image;
   var n = image.numberOfChannels;
   var w = image.width;
   var h = image.height;
   var r, g, b;
   var rn, gn, bn;

   // Create a working image copy
   var tmp = new Image( w, h, image.numberOfChannels, image.colorSpace);
   tmp.apply(image);

   // Initialize the status monitoring system for this image.
   // The status monitor will provide progress information on the console.
   image.statusEnabled = true;
   image.initializeStatus( "repairStarCores", w*h );

   // Don't allow other routines to re-initialize the status monitor
   image.statusInitializationEnabled = false;

   // Reset the rectangular selection to the whole image boundary
   image.resetRectSelection();

   // Don't scan pixels on the very periphery of the image, so we can
   // read nearest neighbors without exceeding image bounds.

   // For each row (except first & last)
   for ( var y = 1; y < h-1; ++y )
   {
      // For each column (except first & last)
      for ( var x = 1; x < w-1; ++x )
      {
         r = tmp.sample( x, y, 0 );
         g = tmp.sample( x, y, 1 );
         b = tmp.sample( x, y, 2 );

         if ((r > threshold) ||
             (g > threshold) ||
             (b > threshold))
         {
            // Pixel above threshold: average the pixel!
            r = 0;
            g = 0;
            b = 0;
            var good = 0;

            // Get neighbour pixels in a square around
            for (var iy = -1; iy <= +1; iy++)
            {
               for (var ix = -1; ix <= +1; ix++)
               {
                  rn = tmp.sample( x+ix, y+iy, 0 );
                  gn = tmp.sample( x+ix, y+iy, 1 );
                  bn = tmp.sample( x+ix, y+iy, 2 );
                  if ((rn <= threshold) &&
                      (gn <= threshold) &&
                      (bn <= threshold))
                  {
                     // include in mean
                     r += rn;
                     g += gn;
                     b += bn;
                     good++;
                  }
               }
            }
            if (good > 0)
            {
               r /= good;
               g /= good;
               b /= good;
            }
            else
            {
               // Pixeel at image borders
               r = threshold;
               g = threshold;
               b = threshold;
            }

            // Write changed pixel back
            tmp.setSample(r, x, y, 0);
            tmp.setSample(g, x, y, 1);
            tmp.setSample(b, x, y, 2);
         }
      }

      image.advanceStatus( w );
   }

   view.beginProcess(UndoFlag_NoSwapFile);
   view.image.assign( tmp );
   view.endProcess();

   tmp.free();

}

function starContrast(view, contrast) {

   if (contrast > 0.0) {

      var P = new CurvesTransformation;

      P.K = [ // x, y
         [0.00000, 0.00000],
         [0.25, 0.25 - 0.2 * contrast],
         [1.00000, 1.00000]
      ];
      P.Kt = CurvesTransformation.prototype.AkimaSubsplines;

      P.executeOn(view);
   }
}

function starBrightness(view, amount) {

   var P = new CurvesTransformation;

   P.L = [ // x, y
      [0.00000, 0.00000],
      [0.5, 0.5 + 0.25*(amount - 1.0)],
      [1.00000, 1.00000]
   ];
   P.Lt = CurvesTransformation.prototype.AkimaSubsplines;

   P.executeOn(view);

}


function starErosion(view, contrast) {

   if (contrast > 0.0) {

      var P = new MorphologicalTransformation;
      P.operator = MorphologicalTransformation.prototype.Erosion;
      P.interlacingDistance = 1;
      P.lowThreshold = 0.000000;
      P.highThreshold = 1.000000;
      P.numberOfIterations = 1;
      P.amount = 0.30 * contrast;
      P.selectionPoint = 0.50;
      P.structureName = "";
      P.structureSize = 25;
      P.structureWayTable = [ // mask
         [[
            0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
            0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
            0x00,0x00,0x00,0x00,0x00,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x00,0x00,0x00,0x00,0x00,
            0x00,0x00,0x00,0x00,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x00,0x00,0x00,0x00,
            0x00,0x00,0x00,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x00,0x00,0x00,
            0x00,0x00,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x00,0x00,
            0x00,0x00,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x00,0x00,
            0x00,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x00,
            0x00,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x00,
            0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,
            0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,
            0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,
            0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,
            0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,
            0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,
            0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,
            0x00,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x00,
            0x00,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x00,
            0x00,0x00,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x00,0x00,
            0x00,0x00,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x00,0x00,
            0x00,0x00,0x00,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x00,0x00,0x00,
            0x00,0x00,0x00,0x00,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x00,0x00,0x00,0x00,
            0x00,0x00,0x00,0x00,0x00,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x00,0x00,0x00,0x00,0x00,
            0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
            0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00
         ]]
      ];

      P.executeOn(view);
   }
}


function scnrGreen(view, amount) {
   var P = new SCNR;
   P.amount = amount;
   P.protectionMethod = SCNR.prototype.AverageNeutral;
   P.colorToRemove = SCNR.prototype.Green;
   P.preserveLightness = true;

   P.executeOn(view);
}


function combine(starless, stars, swapfile) {
   var P = new PixelMath;
   P.expression = "~(~"+ starless.id + " * ~" + stars.id+")";
   P.useSingleExpression = true;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = false;

   P.newImageColorSpace = PixelMath.prototype.SameAsTarget;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
   P.executeOn(starless, swapfile);
}

function preparePreview() {
   if (stretchAndCombineRGBStars.usePreviews && stretchAndCombineRGBStars.preview != undefined) {
      let width = stretchAndCombineRGBStars.preview.image.width;
      let height = stretchAndCombineRGBStars.preview.image.height;
      let channels = stretchAndCombineRGBStars.preview.image.numberOfChannels;
      let bitsPerSample = stretchAndCombineRGBStars.preview.image.bitsPerSample;

      Console.writeln("copy preview..."+width+"x"+height);

      var window = new ImageWindow (
         width,
         height,
         channels,
         bitsPerSample,
         stretchAndCombineRGBStars.preview.image.sampleType != SampleType_Integer,
         stretchAndCombineRGBStars.preview.image != ColorSpace_Gray,
         stretchAndCombineRGBStars.imageStarless.id + "_preview"
      );
      //window.hide();

      var view = window.mainView;
      view.stf = stretchAndCombineRGBStars.imageStarless.stf;
      view.beginProcess (UndoFlag_NoSwapFile);
      view.image.fill(0);
      view.image.apply(stretchAndCombineRGBStars.preview.image);
      view.endProcess();

      Console.writeln("preview " + view.id);

      return view;
   }

   Console.writeln("prepare cloned preview...");
   return cloneHidden(stretchAndCombineRGBStars.imageStarless, "_preview", false);
}

function getPreview(view, postfix) {
   if (stretchAndCombineRGBStars.imageStarless == undefined || stretchAndCombineRGBStars.imageStarless == null) {
      return cloneHidden(view, postfix, false);
   }

   if (stretchAndCombineRGBStars.usePreviews && stretchAndCombineRGBStars.preview != undefined) {
      let rect = stretchAndCombineRGBStars.imageStarless.window.previewRect( stretchAndCombineRGBStars.preview );
      let id = view.id + postfix + "_subview";
      Console.writeln(id);
      var preview = view.window.createPreview( rect, id);

      let width = stretchAndCombineRGBStars.preview.image.width;
      let height = stretchAndCombineRGBStars.preview.image.height;
      let channels = stretchAndCombineRGBStars.preview.image.numberOfChannels;
      let bitsPerSample = stretchAndCombineRGBStars.preview.image.bitsPerSample;

      var window = new ImageWindow (
         width,
         height,
         channels,
         bitsPerSample,
         stretchAndCombineRGBStars.preview.image.sampleType != SampleType_Integer,
         stretchAndCombineRGBStars.preview.image != ColorSpace_Gray,
         stretchAndCombineRGBStars.imageStarless.id + "_preview"
      );
      window.hide();

      var subview = window.mainView;
      subview.beginProcess (UndoFlag_NoSwapFile);
      subview.image.fill(0);
      subview.image.apply(view.image, 2, new Point(0,0), -1, rect);
      subview.endProcess();

      view.window.deletePreview(preview);

      return subview;
   }

   return cloneHidden(view, postfix, false);
}

function processPreview() {

   if (!stretchAndCombineRGBStars.imageStars || stretchAndCombineRGBStars.imageStars.window == null) {

      if (stretchAndCombineRGBStars.imageStarless) {

         if (!stretchAndCombineRGBStars.previewImage) {
            stretchAndCombineRGBStars.previewImage = preparePreview();
         }
      }
      return;
   }

   var clonedStars = getPreview(stretchAndCombineRGBStars.imageStars, "_preview_temp");

   // correct
   repairStarCores(clonedStars);

   // stretch and saturate
   starSaturation(clonedStars, stretchAndCombineRGBStars.saturation);
   arcsinStretch(clonedStars, stretchAndCombineRGBStars.saturation);

   starStretch(clonedStars, stretchAndCombineRGBStars.stretchFactor,
               stretchAndCombineRGBStars.localStretchFactor,
               stretchAndCombineRGBStars.contrastStretch);

   starContrast(clonedStars, stretchAndCombineRGBStars.contrast);
   starErosion(clonedStars, stretchAndCombineRGBStars.contrast);

   if (stretchAndCombineRGBStars.scnrGreenValue > 0.0) {
      scnrGreen(clonedStars, stretchAndCombineRGBStars.scnrGreenValue);
   }

   starBrightness(clonedStars, stretchAndCombineRGBStars.starBrightness);

   if (stretchAndCombineRGBStars.imageStarless) {
      var clonedStarless = getPreview(stretchAndCombineRGBStars.imageStarless, "_preview_starless_temp");
      combine(clonedStarless, clonedStars, false);

      if (!stretchAndCombineRGBStars.previewImage) {
          stretchAndCombineRGBStars.previewImage = preparePreview();
      }

      stretchAndCombineRGBStars.previewImage.beginProcess( UndoFlag_NoSwapFile );
      stretchAndCombineRGBStars.previewImage.image.assign( clonedStarless.image );
      stretchAndCombineRGBStars.previewImage.endProcess();
      closeView(clonedStars);
      closeView(clonedStarless);

   } else {
      stretchAndCombineRGBStars.previewImage.beginProcess( UndoFlag_NoSwapFile );
      stretchAndCombineRGBStars.previewImage.image.assign( clonedStars.image );
      stretchAndCombineRGBStars.previewImage.endProcess();
      closeView(clonedStars);
   }
}


function process() {

   if (!stretchAndCombineRGBStars.imageStars) {
      return;
   }

   var clonedStars = cloneHidden(stretchAndCombineRGBStars.imageStars, "_stars_final", false);

   // correct
   repairStarCores(clonedStars);

   // stretch and saturate
   starSaturation(clonedStars, stretchAndCombineRGBStars.saturation);
   arcsinStretch(clonedStars, stretchAndCombineRGBStars.saturation);
   starStretch(clonedStars, stretchAndCombineRGBStars.stretchFactor,
               stretchAndCombineRGBStars.localStretchFactor,
               stretchAndCombineRGBStars.contrastStretch);

   starContrast(clonedStars, stretchAndCombineRGBStars.contrast);
   starErosion(clonedStars, stretchAndCombineRGBStars.contrast);

   if (stretchAndCombineRGBStars.scnrGreenValue > 0.0) {
      scnrGreen(clonedStars, stretchAndCombineRGBStars.scnrGreenValue);
   }

   starBrightness(clonedStars, stretchAndCombineRGBStars.starBrightness);

   if (stretchAndCombineRGBStars.imageStarless && stretchAndCombineRGBStars.combine) {
      var clonedStarless = cloneHidden(stretchAndCombineRGBStars.imageStarless, "_stars_final", false);
      combine(clonedStarless, clonedStars, false);

      if (stretchAndCombineRGBStars.previewImage) {
         closeView(stretchAndCombineRGBStars.previewImage);
      }

      if (stretchAndCombineRGBStars.createNewImage) {
         if (stretchAndCombineRGBStars.imageStarless.hasAstrometricSolution) {
            clonedStarless.window.copyAstrometricSolution(stretchAndCombineRGBStars.imageStarless.window);
         }

         closeView(clonedStars);
         clonedStarless.window.show();
      } else {
         stretchAndCombineRGBStars.imageStarless.beginProcess();
         stretchAndCombineRGBStars.imageStarless.image.assign( clonedStarless.image );
         stretchAndCombineRGBStars.imageStarless.endProcess();
         closeView(clonedStars);
         closeView(clonedStarless);
      }
   } else {

      if (stretchAndCombineRGBStars.createNewImage) {
         if (stretchAndCombineRGBStars.imageStars.hasAstrometricSolution) {
            clonedStars.window.copyAstrometricSolution(stretchAndCombineRGBStars.imageStars.window);
         }

         clonedStars.window.show();
      } else {
         stretchAndCombineRGBStars.imageStars.beginProcess();
         stretchAndCombineRGBStars.imageStars.image.assign( clonedStars.image );
         stretchAndCombineRGBStars.imageStars.endProcess();
         closeView(clonedStars);
         closeView(clonedStarless);
      }
   }
}


function StretchAndCombineStarsDialog() {
   this.__base__ = Dialog
   this.__base__();
   this.scaledMinWidth = 1280;
   this.scaledMinHeight = 800;

   let labelMinWidth = Math.round(this.font.width("Stretch:M"));
   let sliderMinWidth = 250;

   this.helpLabel = new Label( this );
   this.helpLabel.scaledMinWidth = LEFTWIDTH;
   this.helpLabel.scaledMaxWidth = LEFTWIDTH;
   this.helpLabel.frameStyle = FrameStyle_Box;
   this.helpLabel.margin = 4;
   this.helpLabel.wordWrapping = true;
   this.helpLabel.useRichText = true;
   this.helpLabel.text = "<b>" + TITLE + " v"
                        + VERSION + "</b> &mdash; "
                        + TEXT;


   this.sizerLeft = new VerticalSizer;
   this.sizerLeft.add(this.helpLabel);
   this.sizerLeft.addSpacing(16);

   this.imageStarlessViewSelectorFrame = new Frame(this);
   this.imageStarlessViewSelectorFrame.scaledMinWidth = LEFTWIDTH;
   this.imageStarlessViewSelectorFrame.scaledMaxWidth = LEFTWIDTH;
   this.imageStarlessViewSelectLabel = new Label(this);
   this.imageStarlessViewSelectLabel.text = "Starless Image:";
   this.imageStarlessViewSelectLabel.toolTip = "<p>Select the Starless image. This image must already be stretched and processed.</p>";
   this.imageStarlessViewSelectLabel.textAlignment = TextAlign_Left|TextAlign_VertCenter;

   this.imageStarlessViewSelector = new ViewList(this);
   this.imageStarlessViewSelector.scaledMinWidth = LEFTWIDTH-100;
   this.imageStarlessViewSelector.getMainViews();

   with(this.imageStarlessViewSelectorFrame) {
      sizer = new HorizontalSizer();

      with(sizer) {
         margin = 6;
         add(this.imageStarlessViewSelectLabel);
         addSpacing(8);
         add(this.imageStarlessViewSelector);
         adjustToContents();
      }
   }


   this.imageStarlessViewSelector.onViewSelected = function(view) {
      if (view == undefined) {
         stretchAndCombineRGBStars.imageStarless = undefined;
         this.dialog.updatePreview();
      }
      else if (view.image) {
         stretchAndCombineRGBStars.imageStarless = view;
         stretchAndCombineRGBStars.preview = undefined;
         closePreview();

         this.dialog.previewSelector.enabled = stretchAndCombineRGBStars.usePreviews;

         this.dialog.updatePreview();
         this.dialog.previewControl.zoomToFit();
      }

   }


   this.sizerLeft.add(this.imageStarlessViewSelectorFrame);
   this.sizerLeft.addSpacing(16);

   this.imageStarViewSelectorFrame = new Frame(this);
   this.imageStarViewSelectorFrame.scaledMinWidth = LEFTWIDTH;
   this.imageStarViewSelectorFrame.scaledMaxWidth = LEFTWIDTH;
   this.imageStarViewSelectLabel = new Label(this);
   this.imageStarViewSelectLabel.text = "Star Image:";
   this.imageStarViewSelectLabel.toolTip = "<p>Select the image containing the extracted stars.</p>";
   this.imageStarViewSelectLabel.textAlignment = TextAlign_Left|TextAlign_VertCenter;

   this.imageStarViewSelector = new ViewList(this);
   this.imageStarViewSelector.scaledMinWidth = LEFTWIDTH-100;
   this.imageStarViewSelector.getMainViews();

   with(this.imageStarViewSelectorFrame) {
      sizer = new HorizontalSizer();

      with(sizer) {
         margin = 6;
         add(this.imageStarViewSelectLabel);
         addSpacing(8);
         add(this.imageStarViewSelector);
         adjustToContents();
      }
   }

   this.imageStarViewSelector.onViewSelected = function(view) {

      if (view == undefined || view == null || view.window == null) {
         stretchAndCombineRGBStars.imageStars = undefined;
         this.dialog.updatePreview();
      }
      else if (view.image) {
         stretchAndCombineRGBStars.imageStars = view;

         this.dialog.updatePreview();
      }
      else {
         stretchAndCombineRGBStars.imageStars = undefined;
         this.dialog.updatePreview();
      }

   }

   this.sizerLeft.add(this.imageStarViewSelectorFrame);
   this.sizerLeft.addSpacing(16);

   this.correctSaturatedStarsCheckbox = new CheckBox(this);
   this.correctSaturatedStarsCheckbox.text = "Correct saturated stars";
   this.correctSaturatedStarsCheckbox.checked = stretchAndCombineRGBStars.correctStarCores;
   this.correctSaturatedStarsCheckbox.toolTip = "<p>Correct pixels of saturated star cores.</p>";

   this.correctSaturatedStarsCheckbox.onCheck = function(checked) {
      stretchAndCombineRGBStars.correctStarCores = checked;
   }

   this.newImageCheckbox = new CheckBox(this);
   this.newImageCheckbox.text = "Create new Image";
   this.newImageCheckbox.checked = stretchAndCombineRGBStars.createNewImage;
   this.newImageCheckbox.toolTip = "<p>Create a new image.</p>";

   this.newImageCheckbox.onCheck = function(checked) {
      stretchAndCombineRGBStars.createNewImage = checked;
   }

   this.combineCheckbox = new CheckBox(this);
   this.combineCheckbox.text = "Combine";
   this.combineCheckbox.checked = stretchAndCombineRGBStars.combine;
   this.combineCheckbox.toolTip = "<p>Combines the stars with the starless image.</p>";

   this.combineCheckbox.onCheck = function(checked) {
      stretchAndCombineRGBStars.combine = checked;
   }


   this.correctStarControls = new HorizontalSizer();
   this.correctStarControls.scaledMinWidth = LEFTWIDTH;

   this.correctStarControls.addSpacing(8);
   this.correctStarControls.add(this.correctSaturatedStarsCheckbox);
   this.correctStarControls.addSpacing(12);
   this.correctStarControls.add(this.newImageCheckbox);
   this.correctStarControls.addSpacing(8);
   this.correctStarControls.add(this.combineCheckbox);
   this.correctStarControls.addSpacing(8);

   this.sizerLeft.add(this.correctStarControls);
   this.sizerLeft.addSpacing(32);

   this.stretchSlider = new NumericControl(this);

   this.stretchSlider.label.text = "Stretch:      ";
   this.stretchSlider.toolTip = "<p>Increase the stretch factor for the stars.</p>";
   this.stretchSlider.setRange(0.0, 10.0);
   this.stretchSlider.slider.setRange(0.0, 10000.0);
   this.stretchSlider.setPrecision(4);
   this.stretchSlider.setReal(true);
   this.stretchSlider.setValue(stretchAndCombineRGBStars.stretchFactor);
   this.stretchSlider.onValueUpdated = function(t) {
      stretchAndCombineRGBStars.stretchFactor = t;
   };

   this.resetStretchButton = new ToolButton( this );
   this.resetStretchButton.icon = this.scaledResource( ":/icons/clear-inverted.png" );
   this.resetStretchButton.setScaledFixedSize( 24, 24 );
   this.resetStretchButton.toolTip = "<p>Reset the stretch factor.</p>";
   this.resetStretchButton.onClick = () => {
      stretchAndCombineRGBStars.stretchFactor = Stretch_DEFAULT;
      this.dialog.stretchSlider.setValue(Stretch_DEFAULT);
   }

   this.stretchControl = new Frame();
   this.stretchControl.sizer = new HorizontalSizer;
   this.stretchControl.scaledMinWidth = LEFTWIDTH;
   this.stretchControl.scaledMaxWidth = LEFTWIDTH;

   this.stretchControl.sizer.addSpacing(8);
   this.stretchControl.sizer.add(this.stretchSlider);
   this.stretchControl.sizer.add(this.resetStretchButton);
   this.stretchControl.sizer.addSpacing(8);

   this.sizerLeft.add(this.stretchControl);
   this.sizerLeft.addSpacing(8);

   this.localStretchSlider = new NumericControl(this);

   this.localStretchSlider.label.text = "Local:         ";
   this.localStretchSlider.toolTip = "<p>Increase the local stretch factor for the stars.</p>";
   this.localStretchSlider.setRange(0.0, 8.0);
   this.localStretchSlider.slider.setRange(0.0, 10000.0);
   this.localStretchSlider.setPrecision(4);
   this.localStretchSlider.setReal(true);
   this.localStretchSlider.setValue(stretchAndCombineRGBStars.localStretchFactor);
   this.localStretchSlider.onValueUpdated = function(t) {
      stretchAndCombineRGBStars.localStretchFactor = t;
   };

   this.resetLocalStretchButton = new ToolButton( this );
   this.resetLocalStretchButton.icon = this.scaledResource( ":/icons/clear-inverted.png" );
   this.resetLocalStretchButton.setScaledFixedSize( 24, 24 );
   this.resetLocalStretchButton.toolTip = "<p>Reset the stretch factor.</p>";
   this.resetLocalStretchButton.onClick = () => {
      stretchAndCombineRGBStars.localStretchFactor = LocalStretch_DEFAULT;
      this.dialog.localStretchSlider.setValue(LocalStretch_DEFAULT);
   }

   this.localStretchControl = new Frame();
   this.localStretchControl.sizer = new HorizontalSizer;
   this.localStretchControl.scaledMinWidth = LEFTWIDTH;
   this.localStretchControl.scaledMaxWidth = LEFTWIDTH;

   this.localStretchControl.sizer.addSpacing(8);
   this.localStretchControl.sizer.add(this.localStretchSlider);
   this.localStretchControl.sizer.add(this.resetLocalStretchButton);
   this.localStretchControl.sizer.addSpacing(8);

   this.sizerLeft.add(this.localStretchControl);
   this.sizerLeft.addSpacing(8);

   this.saturationSlider = new NumericControl(this);

   this.saturationSlider.label.text = "Saturation: ";
   this.saturationSlider.toolTip = "<p>Increase the saturation of the stars.</p>";
   this.saturationSlider.setRange(0.0, 10.0);
   this.saturationSlider.slider.setRange(0.0, 10000.0);
   this.saturationSlider.setPrecision(4);
   this.saturationSlider.setReal(true);
   this.saturationSlider.setValue(stretchAndCombineRGBStars.saturation);
   this.saturationSlider.onValueUpdated = function(t) {
      stretchAndCombineRGBStars.saturation = t;
   };

   this.resetSaturationButton = new ToolButton( this );
   this.resetSaturationButton.icon = this.scaledResource( ":/icons/clear-inverted.png" );
   this.resetSaturationButton.setScaledFixedSize( 24, 24 );
   this.resetSaturationButton.toolTip = "<p>Reset the saturation.</p>";
   this.resetSaturationButton.onClick = () => {
      stretchAndCombineRGBStars.saturation = Saturation_DEFAULT;
      this.dialog.saturationSlider.setValue(Saturation_DEFAULT);
   }

   this.saturationControl = new Frame();
   this.saturationControl.sizer = new HorizontalSizer;
   this.saturationControl.scaledMinWidth = LEFTWIDTH;
   this.saturationControl.scaledMaxWidth = LEFTWIDTH;

   this.saturationControl.sizer.addSpacing(8);
   this.saturationControl.sizer.add(this.saturationSlider);
   this.saturationControl.sizer.add(this.resetSaturationButton);
   this.saturationControl.sizer.addSpacing(8);

   this.sizerLeft.add(this.saturationControl);
   this.sizerLeft.addSpacing(8);

   this.contrastSlider = new NumericControl(this);

   this.contrastSlider.label.text = "Contrast:    ";
   this.contrastSlider.toolTip = "<p>Increase the contrast of the stars to reduce halos.</p>";
   this.contrastSlider.setRange(0.0, 1.0);
   this.contrastSlider.slider.setRange(0.0, 10000.0);
   this.contrastSlider.setPrecision(4);
   this.contrastSlider.setReal(true);
   this.contrastSlider.setValue(stretchAndCombineRGBStars.contrast);
   this.contrastSlider.onValueUpdated = function(t) {
      stretchAndCombineRGBStars.contrast = t;
   };

   this.resetContrastButton = new ToolButton( this );
   this.resetContrastButton.icon = this.scaledResource( ":/icons/clear-inverted.png" );
   this.resetContrastButton.setScaledFixedSize( 24, 24 );
   this.resetContrastButton.toolTip = "<p>Reset the contrast.</p>";
   this.resetContrastButton.onClick = () => {
      stretchAndCombineRGBStars.contrast = Contrast_DEFAULT;
      this.dialog.contrastSlider.setValue(Contrast_DEFAULT);
   }

   this.contrastControl = new Frame();
   this.contrastControl.sizer = new HorizontalSizer;
   this.contrastControl.scaledMinWidth = LEFTWIDTH;
   this.contrastControl.scaledMaxWidth = LEFTWIDTH;

   this.contrastControl.sizer.addSpacing(8);
   this.contrastControl.sizer.add(this.contrastSlider);
   this.contrastControl.sizer.add(this.resetContrastButton);
   this.contrastControl.sizer.addSpacing(8);

   this.sizerLeft.add(this.contrastControl);
   this.sizerLeft.addSpacing(8);


   this.scnrGreenSlider = new NumericControl(this);

   this.scnrGreenSlider.label.text = "SCNR(G):    ";
   this.scnrGreenSlider.toolTip = "<p>Remove a green cast of stars, if necessary.</p>";
   this.scnrGreenSlider.setRange(0.0, 1.0);
   this.scnrGreenSlider.slider.setRange(0.0, 10000.0);
   this.scnrGreenSlider.setPrecision(4);
   this.scnrGreenSlider.setReal(true);
   this.scnrGreenSlider.setValue(stretchAndCombineRGBStars.scnrGreenValue);
   this.scnrGreenSlider.onValueUpdated = function(t) {
      stretchAndCombineRGBStars.scnrGreenValue = t;
   };

   this.resetSCNRGreenButton = new ToolButton( this );
   this.resetSCNRGreenButton.icon = this.scaledResource( ":/icons/clear-inverted.png" );
   this.resetSCNRGreenButton.setScaledFixedSize( 24, 24 );
   this.resetSCNRGreenButton.toolTip = "<p>Reset the contrast.</p>";
   this.resetSCNRGreenButton.onClick = () => {
      stretchAndCombineRGBStars.scnrGreenValue = SCNR_DEFAULT;
      this.dialog.scnrGreenSlider.setValue(SCNR_DEFAULT);
   }

   this.scnrGreenControl = new Frame();
   this.scnrGreenControl.sizer = new HorizontalSizer;
   this.scnrGreenControl.scaledMinWidth = LEFTWIDTH;
   this.scnrGreenControl.scaledMaxWidth = LEFTWIDTH;

   this.scnrGreenControl.sizer.addSpacing(8);
   this.scnrGreenControl.sizer.add(this.scnrGreenSlider);
   this.scnrGreenControl.sizer.add(this.resetSCNRGreenButton);
   this.scnrGreenControl.sizer.addSpacing(8);

   this.sizerLeft.add(this.scnrGreenControl);
   this.sizerLeft.addSpacing(8);

   this.brightnessSlider = new NumericControl(this);

   this.brightnessSlider.label.text = "Brightness: ";
   this.brightnessSlider.toolTip = "<p>Adjust the brightness of stars, if necessary.</p>";
   this.brightnessSlider.setRange(0.0, 2.0);
   this.brightnessSlider.slider.setRange(0.0, 20000.0);
   this.brightnessSlider.setPrecision(4);
   this.brightnessSlider.setReal(true);
   this.brightnessSlider.setValue(stretchAndCombineRGBStars.starBrightness);
   this.brightnessSlider.onValueUpdated = function(t) {
      stretchAndCombineRGBStars.starBrightness = t;
   };

   this.resetBrightnessButton = new ToolButton( this );
   this.resetBrightnessButton.icon = this.scaledResource( ":/icons/clear-inverted.png" );
   this.resetBrightnessButton.setScaledFixedSize( 24, 24 );
   this.resetBrightnessButton.toolTip = "<p>Reset the contrast.</p>";
   this.resetBrightnessButton.onClick = () => {
      stretchAndCombineRGBStars.starBrightness = BRIGHTNESS_DEFAULT;
      this.dialog.brightnessSlider.setValue(BRIGHTNESS_DEFAULT);
   }

   this.brightnessControl = new Frame();
   this.brightnessControl.sizer = new HorizontalSizer;
   this.brightnessControl.scaledMinWidth = LEFTWIDTH;
   this.brightnessControl.scaledMaxWidth = LEFTWIDTH;

   this.brightnessControl.sizer.addSpacing(8);
   this.brightnessControl.sizer.add(this.brightnessSlider);
   this.brightnessControl.sizer.add(this.resetBrightnessButton);
   this.brightnessControl.sizer.addSpacing(8);

   this.sizerLeft.add(this.brightnessControl);
   this.sizerLeft.addSpacing(32);

   this.previewSelectControl = new Frame();
   this.previewSelectControl.sizer = new HorizontalSizer;
   this.previewSelectControl.scaledMinWidth = LEFTWIDTH;
   this.previewSelectControl.scaledMaxWidth = LEFTWIDTH;

   this.usePreviewsCheckbox = new CheckBox(this);
   this.usePreviewsCheckbox.text = "Preview";
   this.usePreviewsCheckbox.checked = stretchAndCombineRGBStars.usePreviews;
   this.usePreviewsCheckbox.toolTip = "<p>Previews an selected preview. Define one or more previews to make processing the preview faster.</p>";

   this.usePreviewsCheckbox.onCheck = function(checked) {
      stretchAndCombineRGBStars.usePreviews = checked;
      this.dialog.previewSelector.enabled = checked;

      closePreview();
      this.dialog.updatePreview();
      this.dialog.previewControl.zoomToFit();
   }

   this.previewSelector = new ViewList(this);
   this.previewSelector.scaledMinWidth = LEFTWIDTH-100;
   this.previewSelector.getPreviews();
   this.previewSelector.enabled = false;

   this.previewSelectControl.sizer.addSpacing(8);
   this.previewSelectControl.sizer.add(this.usePreviewsCheckbox);
   this.previewSelectControl.sizer.add(this.previewSelector);
   this.previewSelectControl.sizer.addSpacing(8);

   this.sizerLeft.add(this.previewSelectControl);

   this.sizerLeft.addStretch();

   this.previewSelector.onViewSelected = function(view) {
      if (stretchAndCombineRGBStars.imageStarless != undefined) {
          for ( var i = 0; i < stretchAndCombineRGBStars.imageStarless.window.previews.length; i++ ) {
            if (view.id == stretchAndCombineRGBStars.imageStarless.window.previews[i].id) {
               Console.noteln("Using preview "+view.id);
               closePreview();
               stretchAndCombineRGBStars.preview = view;

               this.dialog.updatePreview();
               this.dialog.previewControl.zoomToFit();
               return;
            }
          }
      }

      //this.dialog.previewSelector.currentView = null;
      closePreview();
      this.dialog.updatePreview();
      this.dialog.previewControl.zoomToFit();
   }

   this.buttonFrame = new Frame;
   this.buttonFrame.scaledMinWidth = LEFTWIDTH;
   this.buttonFrame.scaledMaxWidth = LEFTWIDTH;
   this.buttonFrame.sizer = new HorizontalSizer;
   this.buttonFrame.sizer.margin = 6;
   this.buttonFrame.sizer.spacing = 6;
   this.buttonFrame.sizer.addSpacing(8);

   this.newInstanceButton = new ToolButton( this );
   this.newInstanceButton.icon = this.scaledResource( ":/process-interface/new-instance.png" );
   this.newInstanceButton.setScaledFixedSize( 24, 24 );
   this.newInstanceButton.onMousePress = () => {
     stretchAndCombineRGBStars.save();
     this.newInstance();
   }

   this.ok_Button = new ToolButton( this );
   this.ok_Button.icon = this.scaledResource( ":/process-interface/execute.png" );
   this.ok_Button.setScaledFixedSize( 24, 24 );
   this.ok_Button.toolTip = "<p>Change the target view image using the specified parameters.</p>";
   this.ok_Button.onClick = () => {
      this.dialog.cursor = new Cursor( StdCursor_Hourglass );
      this.dialog.updateRefresh_Button.enabled = false;
      this.dialog.ok_Button.enabled = false;
      this.dialog.cancel_Button.enabled = false;
      process();
      this.dialog.cursor = new Cursor( StdCursor_Arrow);


      this.ok();
   };

   this.cancel_Button = new ToolButton( this );
   this.cancel_Button.icon = this.scaledResource( ":/process-interface/cancel.png" );
   this.cancel_Button.setScaledFixedSize( 24, 24 );
   this.cancel_Button.toolTip = "<p>Close this dialog with no changes.</p>";
   this.cancel_Button.onClick = () => {
      this.cancel();
   };

   this.help_Button = new ToolButton( this );
   this.help_Button.icon = this.scaledResource(":/process-interface/browse-documentation.png");
   this.help_Button.setScaledFixedSize( 24, 24 );
   this.help_Button.toolTip = "<p>Shows the script documentation.</p>";
   this.help_Button.onClick = () => {
      Dialog.browseScriptDocumentation("StretchAndCombineRGBStars", "Stretch and combine stars");
   };


   this.updateRefresh_Button = new ToolButton( this );
   this.updateRefresh_Button.icon = this.scaledResource( ":/icons/refresh.png" );
   this.updateRefresh_Button.setScaledFixedSize( 24, 24 );
   this.updateRefresh_Button.toolTip = "<p>Update the preview.</p>";
   this.updateRefresh_Button.onClick = () => {

      this.dialog.updatePreview();
   };


   this.buttonFrame.sizer.add( this.newInstanceButton );
   this.buttonFrame.sizer.addSpacing(16);

   this.buttonFrame.sizer.add( this.ok_Button );
   this.buttonFrame.sizer.addSpacing(16);
   this.buttonFrame.sizer.add( this.cancel_Button );
   this.buttonFrame.sizer.addStretch();
   this.buttonFrame.sizer.add(this.updateRefresh_Button);
   this.buttonFrame.sizer.addSpacing(16);
   this.buttonFrame.sizer.add( this.help_Button );
   this.buttonFrame.sizer.addSpacing(8);

   this.sizerLeft.add(this.buttonFrame);


   this.previewControl = new PreviewControl(this);
   this.previewControl.scaledMinWidth = 800;
   this.previewControl.minHeight = 600;

   this.sizer = new HorizontalSizer;
   this.sizer.margin = 8;
   this.sizer.spacing = 6;
   this.sizer.add(this.sizerLeft);
   this.sizer.add(this.previewControl);

   this.updatePreview = function() {
      this.dialog.cursor = new Cursor( StdCursor_Hourglass );
      this.dialog.updateRefresh_Button.enabled = false;
      processPreview();
      this.dialog.updateRefresh_Button.enabled = true;
      this.dialog.cursor = new Cursor( StdCursor_Arrow);

      let previewImage = stretchAndCombineRGBStars.previewImage;
      if (previewImage) {
         let metadata = {
                     width: previewImage.image.width,
                     height: previewImage.image.height
         };

         this.dialog.previewControl.SetPreview(previewImage.image, previewImage, metadata);
         this.dialog.previewControl.forceRedraw();
      }
   }

   this.onHide = function() {
      Console.writeln("onHide...");
      closePreview();
   }

}


StretchAndCombineStarsDialog.prototype = new Dialog


function main() {

   if (Parameters.isViewTarget || Parameters.isGlobalTarget) {
      stretchAndCombineRGBStars.load();
   }

   let dialog = new StretchAndCombineStarsDialog();
   dialog.execute();

}

main();
