// CometColorCorrection
// version 0.03 12/14/2024
// by Jürgen Bätz
// using PreviewControl.js by PI Team

#include <pjsr/ColorSpace.jsh>
#include <pjsr/UndoFlag.jsh>
#include <pjsr/Sizer.jsh>
#include <pjsr/FrameStyle.jsh>
#include <pjsr/NumericControl.jsh>

#feature-id CometColorCorrection : Toolbox > CometColorCorrection

#feature-info  Script to fit the starless comet image to a color calibrated image<br/>\
    <br/> \
    This script fits the color of the starless comet image to the color calibrated star registered image\
    <br/> \
    <br/>Copyright &copy; 2024-2025 J&uuml;rgen B&auml;tz

#define OUTPUTVIEWID	"CometColorCorrection"
#define VERSION			0.03

// define a global variable containing script's parameters

var firstrun = false;

var gParameters = {
    targetView: undefined,
    cometTargetView: undefined,
    bgTargetView: undefined,
    cometSourceView: undefined,
    bgSourceView: undefined,

    // stores the current parameters values into the script instance
    save: function() {
        if (gParameters.targetView) Parameters.set("targetView", gParameters.targetView.id);
        if (gParameters.cometTargetView) Parameters.set("cometTargetView", gParameters.cometTargetView.fullId);
        if (gParameters.bgTargetView) Parameters.set("bgTargetView", gParameters.bgTargetView.fullId);
        if (gParameters.cometSourceView) Parameters.set("cometSourceView", gParameters.cometSourceView.fullId);
        if (gParameters.bgSourceView) Parameters.set("bgSourceView", gParameters.bgSourceView.fullId);
    },

    // loads the script instance parameters
    load: function() {
        if (Parameters.has("targetView")) {
            var id = Parameters.getString("targetView");
            gParameters.targetView = View.viewById(id);
        }
        if (Parameters.has("cometTargetView")) {
            var id = Parameters.getString("cometTargetView");
            gParameters.cometTargetView = View.viewById(id);
        }
        if (Parameters.has("bgTargetView")) {
            var id = Parameters.getString("bgTargetView");
            gParameters.bgTargetView = View.viewById(id);
        }
        if (Parameters.has("cometSourceView")) {
            var id = Parameters.getString("cometSourceView");
            gParameters.cometSourceView = View.viewById(id);
        }
        if (Parameters.has("bgSourceView")) {
            var id = Parameters.getString("bgSourceView");
            gParameters.bgSourceView = View.viewById(id);
        }
    }
}

function CloneView(view, newId) {
    let newWindow = new ImageWindow(1, 1, 3, view.window.bitsPerSample, view.window.isFloatSample, view.image.colorSpace != ColorSpace_Gray, newId);
    newWindow.mainView.beginProcess(UndoFlag_NoSwapFile);
    newWindow.mainView.image.assign(view.image);
    newWindow.mainView.endProcess();
    return newWindow.mainView;
}

function PixelMathOp(S_objPreview, S_bgrPreview, C_objPreview, C_bgrPreview, targetview) {

    // gParameters.cometSourceView, gParameters.bgSourceView, gParameters.cometTargetView, gParameters.bgTargetView, gParameters.targetView
    // PixelMath expression by Rainer Raupach
    //Sbgr = med(S_bgr);
    //Sobj = mean(S_obj);
    //Cbgr = med(C_bgr);
    //Cobj = mean(C_obj);
    //f = (Sobj - Sbgr)/(Cobj - Cbgr);
    // Apply to comet image
    //(C-Cbgr)*f + Cbgr
    //S_bgr
    //S_obj
    //C_bgr
    //S_obj

    // creating views from previews
    CloneView(S_objPreview,"S_obj");
    CloneView(C_objPreview,"C_obj");
    CloneView(S_bgrPreview,"S_bgr");
    CloneView(C_bgrPreview,"C_bgr");

    let expression = "Sbgr = med(S_bgr);\nSobj = mean(S_obj);\nCbgr = med(C_bgr);\nCobj = mean(C_obj);\nf = (Sobj - Sbgr)/(Cobj - Cbgr);\n("+targetview.id+"-Cbgr)*f + Cbgr";

    var P = new PixelMath;
    P.expression = expression;
    P.expression1 = "";
    P.expression2 = "";
    P.expression3 = "";
    P.useSingleExpression = true;
    P.symbols = "Sbgr,Sobj,Cbgr,Cobj,f";
    P.clearImageCacheAndExit = false;
    P.cacheGeneratedImages = false;
    P.generateOutput = true;
    P.singleThreaded = false;
    P.optimization = true;
    P.use64BitWorkingImage = false;
    P.rescale = true;
    P.rescaleLower = 0;
    P.rescaleUpper = 1;
    P.truncate = true;
    P.truncateLower = 0;
    P.truncateUpper = 1;
    P.createNewImage = true;
    P.showNewImage = true;
    P.newImageId = "CometColorCorrected";
    P.newImageAlpha = false;
    P.newImageColorSpace = PixelMath.prototype.SameAsTarget;
    P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
    P.executeOn(targetview, false);
}

/*
 * Construct the script dialog interface
 */
function CometColorCorrectionDialog() {
    this.__base__ = Dialog;
    this.__base__();

    var error = false;

    this.title = new Label( this );
    this.title.frameStyle = FrameStyle_Box;
    this.title.margin = 6;
    this.title.wordWrapping = true;
    this.title.useRichText = true;
    this.title.text = "<b>CometColorCorrection v"+VERSION+"</b><br><br>Copyright &copy; 2024-2025 J&uuml;rgen B&auml;tz<br>Based on an idea and algorithm by Rainer Raupach.";

    //  target view

    this.targetViewLabel = new Label(this);
    this.targetViewLabel.text = "Target view:";
    this.targetViewList = new ViewList(this);
    this.targetViewList.getMainViews();
    if ( gParameters.targetView )
        this.targetViewList.currentView = gParameters.targetView;
    this.targetViewList.onViewSelected = function (view) {
        gParameters.targetView = view;
    }


    this.targetSizer = new HorizontalSizer;
    this.targetSizer.add(this.targetViewLabel)
    this.targetSizer.addSpacing(8);
    this.targetSizer.add(this.targetViewList)
    this.targetSizer.textAlignment = TextAlign_Left | TextAlign_VertCenter;

    //  comet sourceview preview

    this.cometSourceViewLabel = new Label(this);
    this.cometSourceViewLabel.text = "Comet preview:";
    this.cometSourceViewList = new ViewList(this);
    this.cometSourceViewList.getPreviews();
    if ( gParameters.cometSourceView )
        this.cometSourceViewList.currentView = gParameters.cometSourceView;
    this.cometSourceViewList.onViewSelected = function (view) {
        gParameters.cometSourceView = view;
    }


    this.cometSourceSizer = new HorizontalSizer;
    this.cometSourceSizer.add(this.cometSourceViewLabel)
    this.cometSourceSizer.addSpacing(8);
    this.cometSourceSizer.add(this.cometSourceViewList)
    this.cometSourceSizer.textAlignment = TextAlign_Left | TextAlign_VertCenter;

    //  bg sourceview preview

    this.bgSourceViewLabel = new Label(this);
    this.bgSourceViewLabel.text = "Background preview:";
    this.bgSourceViewList = new ViewList(this);
    this.bgSourceViewList.getPreviews();
    if ( gParameters.bgSourceView )
        this.bgSourceViewList.currentView = gParameters.bgSourceView;
    this.bgSourceViewList.onViewSelected = function (view) {
        gParameters.bgSourceView = view;
    }


    this.bgSourceSizer = new HorizontalSizer;
    this.bgSourceSizer.add(this.bgSourceViewLabel)
    this.bgSourceSizer.addSpacing(8);
    this.bgSourceSizer.add(this.bgSourceViewList)
    this.bgSourceSizer.textAlignment = TextAlign_Left | TextAlign_VertCenter;

    //  comet targetview preview

    this.cometTargetViewLabel = new Label(this);
    this.cometTargetViewLabel.text = "Comet preview:";
    this.cometTargetViewList = new ViewList(this);
    this.cometTargetViewList.getPreviews();
    if ( gParameters.cometTargetView )
        this.cometTargetViewList.currentView = gParameters.cometTargetView;
    this.cometTargetViewList.onViewSelected = function (view) {
        gParameters.cometTargetView = view;
    }


    this.cometTargetSizer = new HorizontalSizer;
    this.cometTargetSizer.add(this.cometTargetViewLabel)
    this.cometTargetSizer.addSpacing(8);
    this.cometTargetSizer.add(this.cometTargetViewList)
    this.cometTargetSizer.textAlignment = TextAlign_Left | TextAlign_VertCenter;

    //  bg targetview preview

    this.bgTargetViewLabel = new Label(this);
    this.bgTargetViewLabel.text = "Background preview:";
    this.bgTargetViewList = new ViewList(this);
    this.bgTargetViewList.getPreviews();
    if ( gParameters.bgTargetView )
        this.bgTargetViewList.currentView = gParameters.bgTargetView;
    this.bgTargetViewList.onViewSelected = function (view) {
        gParameters.bgTargetView = view;
    }


    this.bgTargetSizer = new HorizontalSizer;
    this.bgTargetSizer.add(this.bgTargetViewLabel)
    this.bgTargetSizer.addSpacing(8);
    this.bgTargetSizer.add(this.bgTargetViewList)
    this.bgTargetSizer.textAlignment = TextAlign_Left | TextAlign_VertCenter;

    //  settings area

    this.previews1GroupBox = new GroupBox(this);
    this.previews1GroupBox.title = "star-registered previews";
    this.previews1GroupBox.sizer = new VerticalSizer;
    this.previews1GroupBox.sizer.margin = 8;
    this.previews1GroupBox.sizer.spacing = 8;
    this.previews1GroupBox.sizer.addSpacing(8);
    this.previews1GroupBox.sizer.add(this.cometSourceSizer);
    this.previews1GroupBox.sizer.addSpacing(8);
    this.previews1GroupBox.sizer.add(this.bgSourceSizer);

    this.previews2GroupBox = new GroupBox(this);
    this.previews2GroupBox.title = "starless comet-registered previews";
    this.previews2GroupBox.sizer = new VerticalSizer;
    this.previews2GroupBox.sizer.margin = 8;
    this.previews2GroupBox.sizer.spacing = 8;
    this.previews2GroupBox.sizer.addSpacing(8);
    this.previews2GroupBox.sizer.add(this.cometTargetSizer);
    this.previews2GroupBox.sizer.addSpacing(8);
    this.previews2GroupBox.sizer.add(this.bgTargetSizer);
    this.previews2GroupBox.sizer.addSpacing(16);

    this.targetGroupBox = new GroupBox(this);
    this.targetGroupBox.sizer = new VerticalSizer;
    this.targetGroupBox.sizer.margin = 8;
    this.targetGroupBox.sizer.add(this.targetSizer);

    //	message area

    this.messageLabel = new Label(this);
    this.messageLabel.text = "Ready.";
    this.messageLabel.frameStyle = FrameStyle_Sunken;
    this.messageLabel.margin = 4;

    //	lower button row

    // Add create instance button to the lower left corner
    this.newInstanceButton = new ToolButton( this );
    this.newInstanceButton.icon = this.scaledResource( ":/process-interface/new-instance.png" );
    this.newInstanceButton.setScaledFixedSize( 24, 24 );
    this.newInstanceButton.toolTip = "New Instance";
    this.newInstanceButton.onMousePress = () => {
        // stores the parameters
        gParameters.save();
        // create the script instance
        this.newInstance();
    };

    this.helpButton = new PushButton(this);
    this.helpButton.text = "Help";
    this.helpButton.icon = this.scaledResource(":/icons/document-text.png");
    this.helpButton.width = 20;
    this.helpButton.onClick = () => {
        Dialog.browseScriptDocumentation("CometColorCorrection");
    };

    this.cancelButton = new PushButton(this);
    this.cancelButton.text = "Cancel";
    this.cancelButton.icon = this.scaledResource(":/icons/cancel.png");
    this.cancelButton.width = 20;
    this.cancelButton.onClick = () => {
    this.dialog.cancel();
    };

    this.execButton = new PushButton(this);
    this.execButton.text = "Execute";
    this.execButton.icon = this.scaledResource(":/icons/ok.png");
    this.execButton.width = 20;
    this.execButton.onClick = () => {
        error = false;
        if (gParameters.targetView === undefined) {
            this.messageLabel.text = "Please select your target image view.";
            error = true;
        } else if ( gParameters.targetView.image.isGrayscale ) {
            this.messageLabel.text = "No RGB image view specified. Target image must be RGB.";
            error = true;
        }
        if (!error) {
            this.messageLabel.text = "Computing...";
            error = false;
            PixelMathOp(gParameters.cometSourceView, gParameters.bgSourceView, gParameters.cometTargetView, gParameters.bgTargetView, gParameters.targetView)
            this.messageLabel.text = "Ready.";
            this.dialog.ok();
        }
    };

    this.buttonSizer = new HorizontalSizer;
    this.buttonSizer.add(this.newInstanceButton);
    this.buttonSizer.addSpacing(20);
    this.buttonSizer.add(this.helpButton);
    this.buttonSizer.addSpacing(20);
    this.buttonSizer.add(this.cancelButton);
    this.buttonSizer.addSpacing(20);
    this.buttonSizer.add(this.execButton);

    // layout the dialog

    this.sizer = new VerticalSizer;
    this.sizer.margin = 8;
    this.sizer.add(this.title);
    this.sizer.addSpacing(8);
    this.sizer.add(this.previews1GroupBox);
    this.sizer.addSpacing(8);
    this.sizer.add(this.previews2GroupBox);
    this.sizer.addSpacing(8);
    this.sizer.add(this.targetGroupBox);
    this.sizer.addSpacing(20);
    this.sizer.add(this.messageLabel);
    this.sizer.addSpacing(10);
    this.sizer.add(this.buttonSizer);

    this.adjustToContents();
    this.setFixedSize();
 
}

CometColorCorrectionDialog.prototype = new Dialog;

function main() {

    // ImageWindow
    gParameters.imageActiveWindow = new ImageWindow(ImageWindow.activeWindow);

    // Validations
    if (gParameters.imageActiveWindow === undefined) {

		console.show();
        Console.warningln("<p>No active image.</p>");

    } else if (gParameters.imageActiveWindow.currentView.isPreview) {

		console.show();
        Console.warningln("<p>This script can not work on previews.</p>");

    } else {

        // is script started from an instance in global or view target context?
        if (Parameters.isGlobalTarget || Parameters.isViewTarget) {
            // then load the parameters from the instance and continue
            gParameters.load();
        }

        // create and show the dialog
        let dialog = new CometColorCorrectionDialog;
        dialog.execute();
        if (!gParameters.doneButtonPressed) {
            dialog.cancel();
        }
    }

}

main();
