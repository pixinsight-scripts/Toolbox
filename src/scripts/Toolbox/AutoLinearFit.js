#include <pjsr/StdIcon.jsh>
#include <pjsr/StdButton.jsh>
#include <pjsr/StdDialogCode.jsh>
#include <pjsr/Sizer.jsh>
#include <pjsr/FrameStyle.jsh>
#include <pjsr/TextAlign.jsh>
#include <pjsr/StdButton.jsh>
#include <pjsr/StdIcon.jsh>
#include <pjsr/ColorSpace.jsh>
#include <pjsr/Interpolation.jsh>
#include <pjsr/UndoFlag.jsh>
#include <pjsr/NumericControl.jsh>
#include <pjsr/CheckState.jsh>
#include <pjsr/SampleType.jsh>

#feature-id AutoLinearFit :  Toolbox > AutoLinearFit

#feature-info  A script to automatically do a linear fit over all color channels.<br/>\
   <br/>\
   A script to automatically do a linear fit over all color channels. \
   <br/> \
   Copyright &copy; 2024 Juergen Terpe


#define VERSION "1.3"
#define TITLE  "AutoLinearFit"
#define TEXT   "Makes a rough color calibration (i.e. for narrowband images) using an automatic LinearFit. The color channel to be used as reference can be selected."

#define METHODTOOLTIP "<p>Use the channel with lowest mean or lowest noise or one of the channels as your reference image.</p>"

var autoLinearFitParameters = {
   image: undefined,
   referenceMethod: "Lowest mean",

    // stores the current parameters values into the script instance
    save: function() {
        if (autoLinearFitParameters.image)  Parameters.set("image", autoLinearFitParameters.image.id);
        Parameters.set("referenceMethod", autoLinearFitParameters.referenceMethod);
    },
    // loads the script instance parameters
    load: function() {
        if (Parameters.has("image")) {
            var id = Parameters.getString("image");
            autoLinearFitParameters.image = View.viewById(id);
        }

        if (Parameters.has("referenceMethod"))
            autoLinearFitParameters.referenceMethod = Parameters.getString("referenceMethod")
    }
}

function extractChannels(view) {
   var P = new ChannelExtraction;
   P.colorSpace = ChannelExtraction.prototype.RGB;
   P.channels = [ // enabled, id
      [true, view.id+"_temp_R"],
      [true, view.id+"_temp_G"],
      [true, view.id+"_temp_B"]
   ];
   P.sampleFormat = ChannelExtraction.prototype.SameAsSource;
   //P.inheritAstrometricSolution =

   P.executeOn(view);
}

function hide(view) {
   view.window.hide();
}

function linearFit(view, referenceView) {
   var P = new LinearFit;
   P.referenceViewId = referenceView.id;
   P.rejectLow = 0.000000;
   P.rejectHigh = 0.920000;
   P.executeOn(view);
}

function combine(viewR, viewG, viewB, resultView) {
   var P = new ChannelCombination;
   P.colorSpace = ChannelCombination.prototype.RGB;
   P.channels = [ // enabled, id
      [true, viewR.id],
      [true, viewG.id],
      [true, viewB.id]
   ];
   P.executeOn(resultView);
}

function closeView(view) {
   if (view !== undefined && view != null && view.window != null) {
      try {
         Console.writeln("closing view: "+view.id);
         view.window.forceClose();
      }
      catch(error) {
         Console.writeln("could not close view..." + error);
      }
   }
}

function process(view) {
   if (view != undefined) {
      extractChannels(view);
      var viewR = View.viewById(view.id+"_temp_R");
      var viewG = View.viewById(view.id+"_temp_G");
      var viewB = View.viewById(view.id+"_temp_B");

      hide(viewR);
      hide(viewG);
      hide(viewB);

      if (autoLinearFitParameters.referenceMethod == "Lowest mean") {

         var meanR = viewR.image.mean();
         var meanG = viewG.image.mean();
         var meanB = viewB.image.mean();

         if (meanB < Math.min(meanG, meanR)) {
            Console.noteln("Selecting the blue channel as a reference for linear fit...");
            Console.noteln("Red channel mean:   "+meanR);
            Console.noteln("Green channel mean: "+meanG);
            Console.noteln("Blue channel mean:  "+meanB);

            linearFit(viewR, viewB);
            linearFit(viewG, viewB);
            combine(viewR, viewG, viewB, view);

         } else if (meanG < Math.min(meanB, meanR)) {
            Console.noteln("Selecting the green channel as a reference for linear fit...");
            Console.noteln("Red channel mean:   "+meanR);
            Console.noteln("Green channel mean: "+meanG);
            Console.noteln("Blue channel mean:  "+meanB);

            linearFit(viewB, viewG);
            linearFit(viewR, viewG);
            combine(viewR, viewG, viewB, view);
         } else {
            Console.noteln("Selecting the green channel as a reference for linear fit...");
            Console.noteln("Red channel mean:   "+meanR);
            Console.noteln("Green channel mean: "+meanG);
            Console.noteln("Blue channel mean:  "+meanB);

            linearFit(viewG, viewR);
            linearFit(viewB, viewR);
            combine(viewR, viewG, viewB, view);
         }
      }
      else if (autoLinearFitParameters.referenceMethod == "Lowest noise k-sigma") {
         var kSigmaR = viewR.image.noiseKSigma();
         var kSigmaG = viewG.image.noiseKSigma();
         var kSigmaB = viewB.image.noiseKSigma();

         if (kSigmaB[0] < Math.min(kSigmaG[0], kSigmaR[0])) {
            Console.noteln("Selecting the blue channel as a reference for linear fit...");
            Console.noteln("Red channel k-sigma:   "+kSigmaR);
            Console.noteln("Green channel k-sigma: "+kSigmaG);
            Console.noteln("Blue channel k-sigma:  "+kSigmaB);

            linearFit(viewR, viewB);
            linearFit(viewG, viewB);
            combine(viewR, viewG, viewB, view);

         } else if (kSigmaG[0] < Math.min(kSigmaB[0], kSigmaR[0])) {
            Console.noteln("Selecting the green channel as a reference for linear fit...");
            Console.noteln("Red channel k-sigma:   "+kSigmaR);
            Console.noteln("Green channel k-sigma: "+kSigmaG);
            Console.noteln("Blue channel k-sigma:  "+kSigmaB);

            linearFit(viewB, viewG);
            linearFit(viewR, viewG);
            combine(viewR, viewG, viewB, view);
         } else {
            Console.noteln("Selecting the red channel as a reference for linear fit...");
            Console.noteln("Red channel k-sigma:   "+kSigmaR);
            Console.noteln("Green channel k-sigma: "+kSigmaG);
            Console.noteln("Blue channel k-sigma:  "+kSigmaB);

            linearFit(viewG, viewR);
            linearFit(viewB, viewR);
            combine(viewR, viewG, viewB, view);
         }
      }
      else if (autoLinearFitParameters.referenceMethod == "Lowest noise MRS") {
         var mrsR = viewR.image.noiseMRS();
         var mrsG = viewG.image.noiseMRS();
         var mrsB = viewB.image.noiseMRS();

         if (mrsB[0] < Math.min(mrsG[0], mrsR[0])) {
            Console.noteln("Selecting the blue channel as a reference for linear fit...");
            Console.noteln("Red channel noise MRS:   "+mrsR);
            Console.noteln("Green channel noise MRS: "+mrsG);
            Console.noteln("Blue channel noise MRS:  "+mrsB);

            linearFit(viewR, viewB);
            linearFit(viewG, viewB);
            combine(viewR, viewG, viewB, view);

         } else if (mrsG[0] < Math.min(mrsB[0], mrsR[0])) {
            Console.noteln("Selecting the green channel as a reference for linear fit...");
            Console.noteln("Red channel noise MRS:   "+mrsR);
            Console.noteln("Green channel noise MRS: "+mrsG);
            Console.noteln("Blue channel noise MRS:  "+mrsB);

            linearFit(viewB, viewG);
            linearFit(viewR, viewG);
            combine(viewR, viewG, viewB, view);
         } else {
            Console.noteln("Selecting the red channel as a reference for linear fit...");
            Console.noteln("Red channel noise MRS:   "+mrsR);
            Console.noteln("Green channel noise MRS: "+mrsG);
            Console.noteln("Blue channel noise MRS:  "+mrsB);

            linearFit(viewG, viewR);
            linearFit(viewB, viewR);
            combine(viewR, viewG, viewB, view);
         }
      }
      else if (autoLinearFitParameters.referenceMethod == "Red channel") {
         Console.noteln("Selecting the red channel as a reference for linear fit...");
         linearFit(viewG, viewR);
         linearFit(viewB, viewR);
         combine(viewR, viewG, viewB, view);
      }
      else if (autoLinearFitParameters.referenceMethod == "Green channel") {
         Console.noteln("Selecting the green channel as a reference for linear fit...");
         linearFit(viewR, viewG);
         linearFit(viewB, viewG);
         combine(viewR, viewG, viewB, view);
      }
      else if (autoLinearFitParameters.referenceMethod == "Blue channel") {
         Console.noteln("Selecting the blue channel as a reference for linear fit...");
         linearFit(viewR, viewB);
         linearFit(viewG, viewB);
         combine(viewR, viewG, viewB, view);
      }

      closeView(viewR);
      closeView(viewG);
      closeView(viewB);

      Console.writeln("Finshed.");
   }
}


function AutoLinearFitDialog() {
   this.__base__ = Dialog
   this.__base__();
   this.minWidth = 400;

   this.sizer = new VerticalSizer;
   this.sizer.margin = 6;
   this.sizer.spacing = 6;

   this.helpLabel = new Label( this );

   this.helpLabel.frameStyle = FrameStyle_Box;
   this.helpLabel.margin = 4;
   this.helpLabel.wordWrapping = true;
   this.helpLabel.useRichText = true;
   this.helpLabel.text = "<b>" + TITLE + " v"
                        + VERSION + "</b> &mdash; "
                        + TEXT;

   this.sizer.add(this.helpLabel);

   this.imageViewSelectorFrame = new Frame(this);
   this.imageViewSelectLabel = new Label(this);
   this.imageViewSelectLabel.text = "Target View:";
   this.imageViewSelectLabel.toolTip = "<p>Select the view to be processed.</p>";
   this.imageViewSelectLabel.textAlignment = TextAlign_Left|TextAlign_VertCenter;

   this.imageViewSelector = new ViewList(this);
   this.imageViewSelector.maxWidth = 250;
   this.imageViewSelector.getMainViews();

   with(this.imageViewSelectorFrame) {
      sizer = new HorizontalSizer();

      with(sizer) {
         margin = 6;
         add(this.imageViewSelectLabel);
         addSpacing(8);
         add(this.imageViewSelector);
         adjustToContents();
      }
   }

   this.sizer.add(this.imageViewSelectorFrame);

   this.methodHSizer = new HorizontalSizer;
   this.methodHSizer.margin = 6;
   this.methodHSizer.spacing = 6;

   this.methodLabel = new Label(this);
   this.methodLabel.text = "Reference channel selection:";
   this.methodLabel.toolTip = "<p>Select the reference channel by lowest mean or lowest noise.</p>";
   this.methodLabel.textAlignment = TextAlign_Left|TextAlign_VertCenter;

   this.methodSelector = new ComboBox(this);

   this.methodSelector.toolTip = METHODTOOLTIP;
   this.methodSelector.maxWidth = 350;
   this.methodSelector.addItem("Lowest mean");
   this.methodSelector.addItem("Lowest noise k-sigma");
   this.methodSelector.addItem("Lowest noise MRS");
   this.methodSelector.addItem("Red channel");
   this.methodSelector.addItem("Green channel");
   this.methodSelector.addItem("Blue channel");

   this.methodHSizer.add(this.methodLabel);
   this.methodHSizer.add(this.methodSelector);

   this.methodSelector.onItemSelected = function(index) {
      autoLinearFitParameters.referenceMethod = this.itemText(index);
   };

   this.sizer.add(this.methodHSizer);
   this.sizer.addStretch();

   this.buttonHSizer = new HorizontalSizer;
   this.buttonHSizer.margin = 6;
   this.buttonHSizer.spacing = 6;
   this.buttonHSizer.addSpacing(8);

   this.ok_Button = new ToolButton( this );
   this.ok_Button.icon = this.scaledResource( ":/process-interface/execute.png" );
   this.ok_Button.setScaledFixedSize( 24, 24 );
   this.ok_Button.toolTip = "<p>Change the target view image using the specified parameters.</p>";
   this.ok_Button.onClick = () => {
      this.ok();
      var view = this.imageViewSelector.currentView;
      if (view !== undefined) {
         autoLinearFitParameters.image = view;
         autoLinearFitParameters.save();
         process(view);
      }
   };

   this.cancel_Button = new ToolButton( this );
   this.cancel_Button.icon = this.scaledResource( ":/process-interface/cancel.png" );
   this.cancel_Button.setScaledFixedSize( 24, 24 );
   this.cancel_Button.toolTip = "<p>Close this dialog with no changes.</p>";
   this.cancel_Button.onClick = () => {
      this.cancel();
   };

   this.help_Button = new ToolButton( this );
   this.help_Button.icon = this.scaledResource(":/process-interface/browse-documentation.png");
   this.help_Button.setScaledFixedSize( 24, 24 );
   this.help_Button.toolTip = "<p>Shows the script documentation.</p>";
   this.help_Button.onClick = () => {
      Dialog.browseScriptDocumentation("AutoLinearFit");
   };

   this.buttonHSizer.add( this.ok_Button );
   this.buttonHSizer.addSpacing(16);
   this.buttonHSizer.add( this.cancel_Button );
   this.buttonHSizer.addStretch();
   this.buttonHSizer.add( this.help_Button );
   this.buttonHSizer.addSpacing(8);

   this.sizer.add(this.buttonHSizer);

   if (autoLinearFitParameters.image !== undefined) {
      this.imageViewSelector.currentView = autoLinearFitParameters.image;
   }

}

AutoLinearFitDialog.prototype = new Dialog


function main() {

   var window = ImageWindow.activeWindow;
   if (!window.isNull) {
      autoLinearFitParameters.image = window.mainView;
   }

   // is script started from an instance in global or view target context?
   if (Parameters.isGlobalTarget || Parameters.isViewTarget) {
      // then load the parameters from the instance and continue
      autoLinearFitParameters.load();
   }

   let dialog = new AutoLinearFitDialog();
   dialog.execute();

}

main();
