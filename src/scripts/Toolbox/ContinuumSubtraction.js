#include <pjsr/StdIcon.jsh>
#include <pjsr/StdButton.jsh>
#include <pjsr/StdDialogCode.jsh>
#include <pjsr/Sizer.jsh>
#include <pjsr/FrameStyle.jsh>
#include <pjsr/TextAlign.jsh>
#include <pjsr/StdButton.jsh>
#include <pjsr/StdIcon.jsh>
#include <pjsr/ColorSpace.jsh>
#include <pjsr/Interpolation.jsh>
#include <pjsr/UndoFlag.jsh>
#include <pjsr/NumericControl.jsh>
#include <pjsr/CheckState.jsh>
#include <pjsr/SampleType.jsh>
#include "PixInsightToolsPreviewControl.jsh"

#include <pjsr/DataType.jsh>

#feature-id ContinuumSubtraction :  Toolbox > ContinuumSubtraction

#feature-info  A script to subtract the continuum of an RGB image from an Ha image.<br/>\
   <br/>\
   A script to subtract the continuum of an RGB image from an Ha image. \
   <br/> \
   Copyright &copy; 2024 Juergen Terpe


#define VERSION "1.10"
#define TITLE  "Subtract Continuum"
#define TEXT   "Subtract the continuum of an RGB image (or the red channel grayscale image) from an Ha image. Both images must be linear!"

 /*
 * Default STF Parameters
 */
// Shadows clipping point in (normalized) MAD units from the median.
#define SHADOWS_CLIP -2.80
// Target mean background in the [0,1] range.
#define TARGET_BKG    0.25

#define MAX_AMOUNT 0.95

#define LEFTWIDTH 400

var continuumParameters = {
   amount: 0.1,
   rgbView: undefined,
   redView: undefined,
   hAlphaSourceView: undefined,
   hAlphaView: undefined,
   previewImage: undefined,
   blackZonesImage: undefined,
   showBlackZones: false,
   previewMode: 0,
   grayScale: false,
   grayScaleHAlpha: false,

   // stores the current parameters values into the script instance
   save: function() {
     Parameters.set("showBlackZones", continuumParameters.showBlackZones);
     Parameters.set("hAlphaSourceView", continuumParameters.hAlphaSourceView.id);
     Parameters.set("previewMode", continuumParameters.previewMode);
     Parameters.set("grayScale", continuumParameters.grayScale);
     Parameters.set("grayScaleHAlpha", continuumParameters.grayScaleHAlpha);
     Parameters.set("amount", continuumParameters.amount);
   },

   // loads the script instance parameters
   load: function() {

      if (Parameters.has("hAlphaSourceView")) {
         Console.writeln("H-Alpha view: "+ Parameters.get("hAlphaSourceView"));

         continuumParameters.hAlphaSourceView = View.viewById(Parameters.get("hAlphaSourceView"));
         if (continuumParameters.hAlphaSourceView.image.isGrayscale) {

            var hAlphaId = cloneHidden(continuumParameters.hAlphaSourceView, "_Ha");
            continuumParameters.hAlphaView = View.viewById(hAlphaId);
            continuumParameters.grayScaleHAlpha = true;
         } else {

            var hAlphaId = createHa(continuumParameters.hAlphaSourceView);
            continuumParameters.grayScaleHAlpha = false;
            var imageView = View.viewById(hAlphaId);
            continuumParameters.hAlphaView = imageView;
         }

      }
      if (Parameters.has("showBlackZones"))
         continuumParameters.showBlackZones = Parameters.getBoolean("showBlackZones");
      if (Parameters.has("grayScale"))
         continuumParameters.grayScale = Parameters.getBoolean("grayScale");
      if (Parameters.has("grayScaleHAlpha"))
         continuumParameters.grayScaleHAlpha = Parameters.getBoolean("grayScaleHAlpha");
      if (Parameters.has("amount"))
         continuumParameters.amount = Parameters.getReal("amount");
      if (Parameters.has("previewMode"))
         continuumParameters.previewMode = Parameters.getInteger("previewMode");
   }

}

function getPreviewKey() {
   if (!continuumParameters.redView || !continuumParameters.hAlphaView)
      return ""
   else
      return "Amount: "+continuumParameters.amount
            + ", RedView: "+continuumParameters.redView.id
            + ", H-Alpha: "+continuumParameters.hAlphaView.id;
}

function scaleImage(image) {
   var maxSize = Math.max(image.width, image.height);

   if (continuumParameters.previewMode == 0) {
      var scale = Math.min(2400.0/maxSize, 1.0/2.0);
      image.resample(scale);
   }
   else if (continuumParameters.previewMode == 1) {
      var scale = Math.min(3600.0/maxSize, 1.0/1.5);
      image.resample(scale);
   }

   return image;
}


function closeView(view) {
   if (view && view.window) {
      try {
         Console.writeln("closing view: "+view.id);
         view.window.forceClose();
      }
      catch(error) {
         Console.writeln("could not close view..." + error);
      }
   }
}

function closeAllTempViews() {

   if (!continuumParameters.grayScale)
     closeView(continuumParameters.redView);

   if (continuumParameters.hAlphaView) {
      closeView(continuumParameters.hAlphaView);
      continuumParameters.hAlphaView = undefined;
   }

   continuumParameters.grayScale = false;
   continuumParameters.redView = undefined;
}

function hideView(view) {
   if (view) {
      var imageView = View.viewById(view.id);
      imageView.window.hide();
   }
}

function doAutoStretchImage(image, id) {
   Console.writeln("Autostretch image"+ id);

   var imageWindow = new ImageWindow(image.width, image.height, image.numberOfChannels, image.bitsPerSample,
                                 image.sampleType == SampleType_Real, image.isColor, id );
   var view = imageWindow.mainView;
   view.beginProcess( UndoFlag_NoSwapFile );
   view.image.assign( image );
   view.endProcess();

   doAutoStretch(view);
   image.assign(view.image);
   imageWindow.forceClose();

   return image;
}

function doAutoStretch(view) {
   let shadowsClipping = -2.80;
   let targetBackground = 0.2;

   var P = new HistogramTransformation;

   var median = view.image.median();//.computeOrFetchProperty( "Median" );
   var mad = view.image.MAD(median);//.computeOrFetchProperty( "MAD" );

   mad = mad * 1.4826; // coherent with a normal distribution

   var c0 = median + shadowsClipping * mad;
   var m = median;
   c0 = Math.range( c0, 0.0, 1.0 );
   m = Math.mtf( targetBackground, m - c0 );

   P.H = [ // c0, m, c1, r0, r1
      [0.00000000, 0.5, 1.00000000, 0.00000000, 1.00000000],
      [0.00000000, 0.5, 1.00000000, 0.00000000, 1.00000000],
      [0.00000000, 0.5, 1.00000000, 0.00000000, 1.00000000],
      [c0,   m, 1.00000000, 0.00000000, 1.00000000],
      [0.00000000, 0.5, 1.00000000, 0.00000000, 1.00000000]
   ];

   P.executeOn(view);
}

function doSTFStretch( view )  {
   var stf = new ScreenTransferFunction;
   var median = view.computeOrFetchProperty( "Median" );
   var c = 0;
   var mad = view.computeOrFetchProperty( "MAD" );
   mad.mul( 1.4826 ); // coherent with a normal distribution

   var A = [ // c0, c1, m, r0, r1
            [0, 1, 0.5, 0, 1],
            [0, 1, 0.5, 0, 1],
            [0, 1, 0.5, 0, 1],
            [0, 1, 0.5, 0, 1] ];

   if ( median.at( 0 ) < 0.5 ) {
      /*
       * Noninverted channel
       */
      var c0 = (1 + mad.at( c ) != 1) ? Math.range( median.at( c ) + SHADOWS_CLIP * mad.at( c ), 0.0, 1.0 ) : 0.0;
      var m  = Math.mtf( TARGET_BKG, median.at( c ) - c0 );
      A[c] = [c0, 1, m, 0, 1];
   } else {
      /*
       * Inverted channel, should not happen
       */
      var c1 = (1 + mad.at( c ) != 1) ? Math.range( median.at( c ) - SHADOWS_CLIP * mad.at( c ), 0.0, 1.0 ) : 1.0;
      var m  = Math.mtf( c1 - median.at( c ), TARGET_BKG );
      A[c] = [0, c1, m, 0, 1];
   }

   stf.STF = A;
   stf.executeOn( view );
}

function clone(view, appendix) {
   var P = new PixelMath;
   P.expression = "$T";
   P.useSingleExpression = true;
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = false;
   P.truncate = true;
   P.createNewImage = true;
   P.showNewImage = true;
   P.newImageId = view.id + appendix;
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.SameAsTarget;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
   P.executeOn(view);
   return View.viewById(P.newImageId);
}

function cloneHidden(view, appendix) {
   var P = new PixelMath;
   P.expression = "$T";
   P.useSingleExpression = true;
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = false;
   P.truncate = true;
   P.createNewImage = true;
   P.showNewImage = false;
   P.newImageId = view.id + appendix;
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.SameAsTarget;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
   P.executeOn(view);
   return view.id + appendix;
}

function createRed(view) {
   Console.writeln("creating Red channel view");
   var P1 = new PixelMath;
   P1.expression = "$T[0]";
   P1.useSingleExpression = true;
   P1.clearImageCacheAndExit = false;
   P1.cacheGeneratedImages = false;
   P1.generateOutput = true;
   P1.singleThreaded = false;
   P1.optimization = true;
   P1.use64BitWorkingImage = false;
   P1.rescale = false;
   P1.truncate = true;
   P1.createNewImage = true;
   P1.showNewImage = false;
   P1.newImageId = view.id + "_Red";
   P1.newImageColorSpace = PixelMath.prototype.Gray;
   P1.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
   P1.executeOn(view);

   return view.id + "_Red";
}

function createHa(view) {
   Console.writeln("creating Ha view");
   var P1 = new PixelMath;
   P1.expression = "$T[0]";
   P1.useSingleExpression = true;
   P1.clearImageCacheAndExit = false;
   P1.cacheGeneratedImages = false;
   P1.generateOutput = true;
   P1.singleThreaded = false;
   P1.optimization = true;
   P1.use64BitWorkingImage = false;
   P1.rescale = false;
   P1.truncate = true;
   P1.createNewImage = true;
   P1.showNewImage = false;
   P1.newImageId = view.id + "_Ha";
   P1.newImageColorSpace = PixelMath.prototype.Gray;
   P1.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
   P1.executeOn(view);

   return view.id + "_Ha";
}

function subtractMean(view) {
   var P = new PixelMath;
   P.expression = "$T-0.125*med($T)";
   P.expression1 = "";
   P.expression2 = "";
   P.expression3 = "";
   P.useSingleExpression = true;
   P.symbols = "";
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = false;
   P.rescaleLower = 0;
   P.rescaleUpper = 1;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = false;
   P.showNewImage = true;
   P.newImageId = "";
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.SameAsTarget;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;

   P.executeOn(view);
}

function applyContinuumSubtraction(view, redView, alphaView, amount) {

   var P = new PixelMath;
   P.expression = alphaView.id + " - Q*("+redView.id+" - med("+redView.id+"))";
   P.useSingleExpression = true;
   P.symbols = "Q = " + amount;
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = false;
   P.rescaleLower = 0;
   P.rescaleUpper = 1;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = false;
   P.showNewImage = true;
   P.newImageId = "";
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.SameAsTarget;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;

   P.executeOn(view);
}

function getPreviewImageView(view) {
   var image = new Image(view.image);
   var img = scaleImage(image);
   // create a closed image for processing in the background
   var wtmp = new ImageWindow( img.width, img.height, img.numberOfChannels,
                                  img.bitsPerSample, img.sampleType == SampleType_Real, img.isColor, view.id + "_tmp_preview" );
   var v = wtmp.mainView;
   v.beginProcess( UndoFlag_NoSwapFile );
   v.image.assign( img );
   v.endProcess();

   image.free();

   return v;
}

function createBlackZonesImage(view) {
   var P = new PixelMath;
   P.expression = "iif($T<=0.00000001, 1, 0)";
   P.expression1 = "0";
   P.expression2 = "0";
   P.expression3 = "iif($T<=0.00000001, 1, 0)";
   P.useSingleExpression = false;
   P.symbols = "";
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = false;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = true;
   P.showNewImage = false;
   P.newImageId = view.id+"_zeros";
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = true;
   P.newImageColorSpace = PixelMath.prototype.RGB;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
   P.executeOn(view);

   var blackView = View.viewById(P.newImageId);
   var image = new Image(blackView.image);

   if (continuumParameters.blackZonesImage)
      continuumParameters.blackZonesImage.free();

   continuumParameters.blackZonesImage = image;
   blackView.window.forceClose();
}


function processPreview() {
   if (continuumParameters.redView
       && continuumParameters.hAlphaView) {

      Console.writeln("processing preview Q=" + continuumParameters.amount);
      Console.writeln("Red channel "+ continuumParameters.redView.id);
      Console.writeln("Ha image "+ continuumParameters.hAlphaView.id);


      var v = getPreviewImageView(continuumParameters.hAlphaView);
      var redViewPreview = getPreviewImageView(continuumParameters.redView);
      var hAlphaPreview = getPreviewImageView(continuumParameters.hAlphaView);

      applyContinuumSubtraction(v,
                             redViewPreview,
                             hAlphaPreview,
                             continuumParameters.amount);

      doAutoStretch(v);

      continuumParameters.previewImage.assign(v.image);
      createBlackZonesImage(v);

      v.window.forceClose();
      redViewPreview.window.forceClose();
      hAlphaPreview.window.forceClose();
   }
}

function process() {
   if (continuumParameters.hAlphaView
       && continuumParameters.redView && continuumParameters.rgbView) {

      Console.writeln("processing with Q=" + continuumParameters.amount);

      applyContinuumSubtraction(continuumParameters.hAlphaView,
                                continuumParameters.redView,
                                continuumParameters.hAlphaView,
                                continuumParameters.amount);

      // clone view and make it visible
      var resultView = clone(continuumParameters.hAlphaView, "_final");
      doSTFStretch(resultView);

      let originWindow = continuumParameters.rgbView.window;

      resultView.window.keywords = originWindow.keywords;
      if (originWindow.hasAstrometricSolution) {
         resultView.window.copyAstrometricSolution(originWindow);
      }

   }
}




function ContinuumSubtractionDialog() {
   this.__base__ = Dialog
   this.__base__();
   this.scaledMinWidth = 1280;
   this.scaledMinHeight = 800;

   let labelMinWidth = Math.round(this.font.width("Amount:"));

   this.helpLabel = new Label( this );
   this.helpLabel.scaledMinWidth = LEFTWIDTH;
   this.helpLabel.scaledMaxWidth = LEFTWIDTH;
   this.helpLabel.frameStyle = FrameStyle_Box;
   this.helpLabel.margin = 4;
   this.helpLabel.wordWrapping = true;
   this.helpLabel.useRichText = true;
   this.helpLabel.text = "<b>" + TITLE + " v" + VERSION + "</b> &mdash;" + TEXT +"<p>Hint: Both images must be aligned using StarAlignment process! Remove the stars from both the Ha image and the RGB image before using this script!</p>";

   this.imageGroup = new GroupBox(this);
   this.imageGroup.title = "Images";
   this.imageGroup.scaledMinWidth = LEFTWIDTH;
   this.imageGroup.scaledMaxWidth = LEFTWIDTH;
   this.imageGroup.sizer = new VerticalSizer();

   // Select the RGB image to be procressed
   this.rgbViewSelectorFrame = new Frame(this);
   this.rgbViewSelectorFrame.scaledMinWidth = LEFTWIDTH;
   this.rgbViewSelectorFrame.scaledMaxWidth = LEFTWIDTH;
   this.rgbViewSelectLabel = new Label(this);
   this.rgbViewSelectLabel.text = "RGB view or\nred channel view:";
   this.rgbViewSelectLabel.toolTip = "<p>Select the RGB image or Red Channel view to be subtracted from Ha.</p>";

   this.rgbViewSelector = new ViewList(this);
   this.rgbViewSelector.scaledMaxWidth = 250;
   this.rgbViewSelector.getMainViews();

   with(this.rgbViewSelectorFrame) {
      sizer = new HorizontalSizer();

      with(sizer) {
         margin = 6;
         add(this.rgbViewSelectLabel);
         addSpacing(8);
         add(this.rgbViewSelector);
         adjustToContents();
      }
   }

   this.rgbViewSelector.onViewSelected = function(view) {

      this.dialog.previewTimer.stop();

      if (continuumParameters.redView) {
         if (!continuumParameters.grayScale)
            closeView(continuumParameters.redView);
         continuumParameters.redView = undefined;
      }

      continuumParameters.rgbView = view;
      if (continuumParameters.rgbView && view.id) {

         if (view.image.isGrayscale) {
            continuumParameters.redView = view;
            continuumParameters.grayScale = true;

            if (continuumParameters.previewImage) {
               continuumParameters.previewImage.free();
               continuumParameters.previewImage = undefined;
            }

            if (continuumParameters.blackZonesImage) {
               continuumParameters.blackZonesImage.free();
               continuumParameters.blackZonesImage = undefined;
            }

            var previewImage = new Image(continuumParameters.redView.image);
            previewImage = doAutoStretchImage(scaleImage(previewImage), view.id+"_red");

            var metadata = {
               width: previewImage.width,
               height: previewImage.height
            }

            this.dialog.previewControl.SetPreview(previewImage, continuumParameters.rgbView, metadata);
            this.dialog.previewControl.forceRedraw();
            continuumParameters.previewImage = previewImage;

            this.dialog.previewTimer.previewKey = "..";
            this.dialog.previewTimer.start();

         } else {
            continuumParameters.grayScale = false;

            var redId = createRed(continuumParameters.rgbView);
            var imageView = View.viewById(redId);
            continuumParameters.redView = imageView;

            if (continuumParameters.previewImage) {
               continuumParameters.previewImage.free();
               continuumParameters.previewImage = undefined;
            }

            if (continuumParameters.blackZonesImage) {
               continuumParameters.blackZonesImage.free();
               continuumParameters.blackZonesImage = undefined;
            }

            var previewImage = new Image(continuumParameters.redView.image);
            previewImage = doAutoStretchImage(scaleImage(previewImage), view.id+"_red");
            var metadata = {
               width: previewImage.width,
               height: previewImage.height
            }

            this.dialog.previewControl.SetPreview(previewImage, continuumParameters.rgbView, metadata);
            this.dialog.previewControl.zoomToFit();
            this.dialog.previewControl.forceRedraw();
            continuumParameters.previewImage = previewImage;

            this.dialog.previewTimer.previewKey = "..";
            this.dialog.previewTimer.start();
         }
      } else {

         if (continuumParameters.previewImage) {
            continuumParameters.previewImage.free();
            continuumParameters.previewImage = undefined;
         }

         if (continuumParameters.blackZonesImage) {
            continuumParameters.blackZonesImage.free();
            continuumParameters.blackZonesImage = undefined;
         }
      }

   };

   // Select the Ha image to be procressed
   this.hAlphaSelectorFrame = new Frame(this);
   this.hAlphaSelectorFrame.scaledMinWidth = LEFTWIDTH;
   this.hAlphaSelectorFrame.scaledMaxWidth = LEFTWIDTH;
   this.hAlphaViewSelectLabel = new Label(this);
   this.hAlphaViewSelectLabel.text = "Narrowband view or\nHa view:";
   this.hAlphaViewSelectLabel.toolTip = "<p>Select the Narrowband image where the Ha should be extracted and processed.</p>";

   this.hAlphaViewSelector = new ViewList(this);
   this.hAlphaViewSelector.scaledMaxWidth = 250;
   this.hAlphaViewSelector.getMainViews();

   with(this.hAlphaSelectorFrame) {
      sizer = new HorizontalSizer();

      with(sizer) {
         margin = 6;
         add(this.hAlphaViewSelectLabel);
         addSpacing(8);
         add(this.hAlphaViewSelector);
         adjustToContents();
      }
   }

   this.hAlphaViewSelector.onViewSelected = function(view) {

      if (view && view.id) {
         this.dialog.previewTimer.stop();

         if (!continuumParameters.grayScaleHAlpha)
            closeView(continuumParameters.hAlphaView);

         if (view.image.isGrayscale) {

            var hAlphaId = cloneHidden(view, "_Ha");
            continuumParameters.hAlphaView = View.viewById(hAlphaId);
            continuumParameters.grayScaleHAlpha = true;
            continuumParameters.hAlphaSourceView = view;

         } else {

            var hAlphaId = createHa(view);
            continuumParameters.grayScaleHAlpha = false;
            var imageView = View.viewById(hAlphaId);
            continuumParameters.hAlphaView = imageView;
            continuumParameters.hAlphaSourceView = view;
         }

         if (continuumParameters.hAlphaView) {
            if (continuumParameters.previewImage) {
               continuumParameters.previewImage.free();
               continuumParameters.previewImage = undefined;
            }

            var previewImage = new Image(continuumParameters.hAlphaView.image);
            previewImage = doAutoStretchImage(scaleImage(previewImage), continuumParameters.hAlphaView.id+"_HaPreview");
            var metadata = {
               width: previewImage.width,
               height: previewImage.height
            }

            let targetView = continuumParameters.rgbView;
            if (targetView == undefined)
               targetView = continuumParameters.hAlphaView;

            this.dialog.previewControl.SetPreview(previewImage, targetView, metadata);
            this.dialog.previewControl.zoomToFit();
            this.dialog.previewControl.forceRedraw();
            continuumParameters.previewImage = previewImage;

            this.dialog.previewTimer.previewKey = ".";
            this.dialog.previewTimer.start();
         }
      } else {
         if (continuumParameters.hAlphaView) {
            if (!continuumParameters.grayScaleHAlpha)
               closeView(continuumParameters.hAlphaView);

            continuumParameters.hAlphaView = undefined;
            if (continuumParameters.previewImage) {
               continuumParameters.previewImage.free();
               continuumParameters.previewImage = undefined;
            }

            if (continuumParameters.blackZonesImage) {
               continuumParameters.blackZonesImage.free();
               continuumParameters.blackZonesImage = undefined;
            }
         }
      }
   };

   this.imageGroup.sizer.add(this.rgbViewSelectorFrame);
   this.imageGroup.sizer.add(this.hAlphaSelectorFrame);

   this.controlGroup = new GroupBox(this);
   this.controlGroup.title = "Amount";
   this.controlGroup.scaledMinWidth = LEFTWIDTH;
   this.controlGroup.scaledMaxWidth = LEFTWIDTH;
   this.controlGroup.sizer = new VerticalSizer();

   this.qToolFrame = new Frame(this);
   this.qToolFrame.scaledMinWidth = LEFTWIDTH;
   this.qToolFrame.scaledMaxWidth = LEFTWIDTH;
   this.qSlider = new NumericControl(this);
   this.qSlider.setRange(0.00001, MAX_AMOUNT);
   this.qSlider.slider.setRange(0.0, 10000000.0);
   this.qSlider.setPrecision(5);
   this.qSlider.setReal(true);
   this.qSlider.enableFixedPrecision(true);
   this.qSlider.setValue(continuumParameters.amount);
   this.qSlider.label.text = "Amount:";
   this.qSlider.label.minWidth = labelMinWidth;
   this.qSlider.tooltip = "<p>Increase or decrease the amount to control the amount of continuum to be substracted from Ha. Increasing will remove more continuum light from Ha, but when the result starts to show black areas this is too much. Find the best balance for a good result.</p>";

   this.qSliderSmall = new Slider(this);
   this.qSliderSmall.setRange(0, 1000.0);
   this.qSliderSmall.value = 500.0;

   this.qToolVertSizer = new VerticalSizer();
   this.qToolVertSizer.margin = 4;
   this.qToolVertSizer.add(this.qSlider);
   this.qToolVertSizer.addSpacing(8);
   this.qToolVertSizer.add(this.qSliderSmall);

   with(this.qToolFrame) {
      sizer = new HorizontalSizer();

      with(sizer) {
         margin = 4;
         add(this.qToolVertSizer);
      }
   }

   this.controlGroup.sizer.add(this.qToolFrame);

   this.qSlider.onValueUpdated = function(q) {
      this.dialog.previewTimer.stop();
      continuumParameters.amount = q;
      this.dialog.previewTimer.start();
   };

   this.qSliderSmall.onValueUpdated = function(q) {
      this.dialog.previewTimer.stop();
      continuumParameters.amount += (q-500.0)/1000000.0;
      if (continuumParameters.amount<0.000001)
         continuumParameters.amount = 0.000001;
      else if (continuumParameters.amount > MAX_AMOUNT)
         continuumParameters.amount = MAX_AMOUNT;

      this.dialog.qSlider.setValue(continuumParameters.amount);
      this.dialog.previewTimer.start();
   };

   this.qSliderSmall.onMouseRelease = function() {
      this.dialog.previewTimer.stop();
      this.dialog.qSliderSmall.value = 500.0;
      this.dialog.previewTimer.start();
   }

   this.checkboxFrame = new Frame;
   this.checkboxFrame.scaledMinWidth = LEFTWIDTH;
   this.checkboxFrame.scaledMaxWidth = LEFTWIDTH;
   this.checkboxFrame.sizer = new HorizontalSizer;
   this.checkboxFrame.sizer.textAlignment = TextAlign_Left | TextAlign_VertCenter;
   this.checkboxFrame.sizer.margin = 6;
   this.checkboxFrame.sizer.spacing = 16;

   this.showBlackPixel = new CheckBox(this);
   this.showBlackPixel.text = "Show black pixel";
   this.showBlackPixel.checked = continuumParameters.showBlackZones;
   this.showBlackPixel.toolTip = "<p>Show black pixels in preview as red overlay.</p>";
   this.checkboxFrame.sizer.add(this.showBlackPixel);

   this.controlGroup.sizer.add(this.checkboxFrame);

   this.showBlackPixel.onCheck = function(checked) {
      this.dialog.previewTimer.stop();
      continuumParameters.showBlackZones = checked;
      this.dialog.previewControl.forceRedraw();
      this.dialog.previewTimer.previewKey = "....";
      this.dialog.previewTimer.start();
   }

   this.buttonFrame = new Frame;
   this.buttonFrame.scaledMinWidth = LEFTWIDTH;
   this.buttonFrame.scaledMaxWidth = LEFTWIDTH;
   this.buttonFrame.sizer = new HorizontalSizer;
   this.buttonFrame.sizer.margin = 6;
   this.buttonFrame.sizer.spacing = 6;
   this.buttonFrame.sizer.addSpacing(8);

   this.newInstanceButton = new ToolButton( this );
   this.newInstanceButton.icon = this.scaledResource( ":/process-interface/new-instance.png" );
   this.newInstanceButton.setScaledFixedSize( 24, 24 );
   this.newInstanceButton.onMousePress = () => {

      if (continuumParameters.hAlphaView) {
         continuumParameters.save();
         this.newInstance();
      }
   }


   this.ok_Button = new ToolButton( this );
   this.ok_Button.icon = this.scaledResource( ":/process-interface/execute.png" );
   this.ok_Button.setScaledFixedSize( 24, 24 );
   this.ok_Button.toolTip = "<p>Subtract the continuum emission using these parameters.</p>";
   this.ok_Button.onClick = () => {
      this.previewTimer.stop();
      if (this.dialog.busy) return;
      process();
      continuumParameters.save();
      this.ok();
   };

   this.cancel_Button = new ToolButton( this );
   this.cancel_Button.icon = this.scaledResource( ":/process-interface/cancel.png" );
   this.cancel_Button.setScaledFixedSize( 24, 24 );
   this.cancel_Button.toolTip = "<p>Close this dialog with no changes.</p>";
   this.cancel_Button.onClick = () => {
      this.previewTimer.stop();
      if (this.dialog.busy) return;
      this.cancel();
      closeAllTempViews();
   };

   this.help_Button = new ToolButton( this );
   this.help_Button.icon = this.scaledResource(":/process-interface/browse-documentation.png");
   this.help_Button.setScaledFixedSize( 24, 24 );
   this.help_Button.toolTip = "<p>Shows the script documentation.</p>";
   this.help_Button.onClick = () => {
      Dialog.browseScriptDocumentation("ContinuumSubtraction");
   };

   this.buttonFrame.sizer.add( this.newInstanceButton );
   this.buttonFrame.sizer.addSpacing(16);
   this.buttonFrame.sizer.add( this.ok_Button );
   this.buttonFrame.sizer.addSpacing(16);
   this.buttonFrame.sizer.add( this.cancel_Button );
   this.buttonFrame.sizer.addStretch();
   this.buttonFrame.sizer.add( this.help_Button );
   this.buttonFrame.sizer.addSpacing(8);


   this.previewControl = new PreviewControl(this);
   this.previewControl.scaledMinWidth = 800;
   this.previewControl.minHeight = 600;
   this.previewControl.onCustomPaintScope = this;
   this.previewControl.onCustomPaint = function onCustomPreview(graphics, x0, y0, x1, y1) {
      if (continuumParameters.showBlackZones && continuumParameters.blackZonesImage) {
         graphics.drawBitmap(0, 0, continuumParameters.blackZonesImage.render());
      }
   }

   // Layout the dialog...
   this.sizer = new HorizontalSizer;
   this.sizer.margin = 6;
   this.sizer.spacing = 6;

   this.leftSizer = new VerticalSizer;
   this.leftSizer.margin = 6;
   this.leftSizer.spacing = 6;

   this.leftSizer.add(this.helpLabel);
   this.leftSizer.addSpacing(8);
   this.leftSizer.add(this.imageGroup);
   this.leftSizer.addSpacing(8);
   this.leftSizer.add(this.controlGroup);

   this.leftSizer.addStretch();
   this.leftSizer.add(this.buttonFrame);

   this.sizer.add(this.leftSizer);
   this.sizer.add(this.previewControl);

   this.previewTimer = new Timer();
   this.previewTimer.interval = 0.500;
   this.previewTimer.periodic = true;
   this.previewTimer.dialog = this;
   this.previewTimer.busy = false;
   this.previewTimer.previewKey = "";
   this.previewTimer.updateCount = 0;

   this.previewTimer.onTimeout = function()
   {
      let currentPreviewKey = getPreviewKey();
      let needsUpdate = (this.previewKey != currentPreviewKey && currentPreviewKey != "");

      if (needsUpdate)
      {
         if (this.busy) return;

         this.dialog.ok_Button.enabled = false;
         this.dialog.cancel_Button.enabled = false;
         this.stop();
         this.busy = true;


         try {

            processPreview();

            if (continuumParameters.previewImage) {
               let previewImage = continuumParameters.previewImage;
               var metadata = {
                  width: previewImage.width,
                  height: previewImage.height
               }

               if (continuumParameters.rgbView)
                  this.dialog.previewControl.SetPreview(previewImage, continuumParameters.rgbView, metadata);
               else
                  this.dialog.previewControl.SetPreview(previewImage, continuumParameters.hAlphaView, metadata);

               this.dialog.previewControl.forceRedraw();
            }
         }
         finally {
            this.busy = false;
            this.start();
            this.dialog.ok_Button.enabled = true;
            this.dialog.cancel_Button.enabled = true;
         }
      }

      this.previewKey = currentPreviewKey
   }

   this.onHide = function() {
      this.previewTimer.stop();
      closeAllTempViews();
   }

   this.previewTimer.start();
}

ContinuumSubtractionDialog.prototype = new Dialog


function main() {

   // is script started from an instance in global or view target context?
   if (Parameters.isViewTarget) {
      continuumParameters.load();
      continuumParameters.rgbView = Parameters.targetView;
      if (continuumParameters.rgbView.image.isGrayScale) {
         continuumParameters.redView = continuumParameters.rgbView;
         continuumParameters.grayScale = true;

      } else {
         var redId = createRed(continuumParameters.rgbView);
         var imageView = View.viewById(redId);
         continuumParameters.redView = imageView;
         continuumParameters.grayScale = false;
      }

      Console.writeln("Amount: "+continuumParameters.amount
            + ", RedView: "+continuumParameters.redView.id
            + ", H-Alpha: "+continuumParameters.hAlphaView.id);

      process();
      return;
   } else if (Parameters.isGlobalTarget) {
      // then load the parameters from the instance and continue
      continuumParameters.load();
   }

   let dialog = new ContinuumSubtractionDialog();
   dialog.execute();

}

main();

