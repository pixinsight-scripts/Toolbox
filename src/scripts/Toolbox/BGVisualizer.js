// BGVisualizer
// version 0.51 03/17/2024
// by Jürgen Bätz
// using PreviewControl.js by PI Team

#include <pjsr/ColorSpace.jsh>
#include <pjsr/UndoFlag.jsh>
#include <pjsr/Sizer.jsh>
#include <pjsr/FrameStyle.jsh>
#include <pjsr/NumericControl.jsh>

#feature-id BGVisualizer : Toolbox > BGVisualizer

#feature-info  Helper script to visualize in black/white the flatness of the background.<br/>\
    <br/> \
    <br/>Copyright &copy; 2023-2024 J&uuml;rgen B&auml;tz

#define OUTPUTVIEWID	"BGVisualizer"
#define VERSION			0.51
#define SCALEDWIDTH     1200
#define SCALEDHEIGHT     600
#define SCALEDRIGHTWIDTH 420

// define a global variable containing script's parameters

var firstrun = false;

var gParameters = {
    targetView: undefined,
    threshRaw: 0,
    greyscale: false,
    doneButtonPressed: false,
    previewed: false,

    // stores the current parameters values into the script instance
    save: function () {
        if (gParameters.targetView) Parameters.set("targetView", gParameters.targetView.id);
    },

    // loads the script instance parameters
    load: function () {
        if (Parameters.has("targetView")) {
            var id = Parameters.getString("targetView");
            gParameters.targetView = View.viewById(id);
        }
    }
}

function CopyViewContentWithPM(sourceview, targetview) {
    var P = new PixelMath;
    P.expression = sourceview.id;
    P.expression1 = "";
    P.expression2 = "";
    P.expression3 = "";
    P.useSingleExpression = true;
    P.symbols = "";
    P.clearImageCacheAndExit = false;
    P.cacheGeneratedImages = false;
    P.generateOutput = true;
    P.singleThreaded = false;
    P.optimization = true;
    P.use64BitWorkingImage = false;
    P.rescale = false;
    P.rescaleLower = 0;
    P.rescaleUpper = 1;
    P.truncate = true;
    P.truncateLower = 0;
    P.truncateUpper = 1;
    P.createNewImage = false;
    P.showNewImage = false;
    P.newImageId = "";
    P.newImageWidth = 0;
    P.newImageHeight = 0;
    P.newImageAlpha = false;
    P.newImageColorSpace = PixelMath.prototype.SameAsTarget;
    P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
    P.executeOn(targetview, false);
}

function Visualize(value, view, targetview) {

    if (gParameters.greyscale) {
        expr = "iif(I(" + view.id + ")>" + value + ", 1, 0)";
    } else {
        expr = "iif(" + view.id + ">" + value + ", 1, 0)";
    }
    var P = new PixelMath;
    P.expression = expr;
    P.expression1 = "";
    P.expression2 = "";
    P.expression3 = "";
    P.useSingleExpression = true;
    P.symbols = "";
    P.clearImageCacheAndExit = false;
    P.cacheGeneratedImages = false;
    P.generateOutput = true;
    P.singleThreaded = false;
    P.optimization = true;
    P.use64BitWorkingImage = false;
    P.rescale = false;
    P.rescaleLower = 0;
    P.rescaleUpper = 1;
    P.truncate = true;
    P.truncateLower = 0;
    P.truncateUpper = 1;
    P.createNewImage = false;
    P.showNewImage = false;
    P.newImageId = "";
    P.newImageWidth = 0;
    P.newImageHeight = 0;
    P.newImageAlpha = false;
    P.newImageColorSpace = PixelMath.prototype.SameAsTarget;
    P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
    P.executeOn(targetview, false);
}

function ResetToMean(view, preview) {
    var mean = view.image.mean();
    gParameters.threshRaw = mean;
    Visualize(mean, view, preview);
}

/*
 * Construct the script dialog interface
 */
function BGVisualizerDialog() {
    this.__base__ = Dialog;
    this.__base__();

    this.userResizable = true;
    this.scaledMinWidth = SCALEDWIDTH;
    this.scaledMinHeight = SCALEDHEIGHT;

    var error = false;

    this.title = new Label( this );
    this.title.frameStyle = FrameStyle_Box;
    this.title.margin = 6;
    this.title.wordWrapping = true;
    this.title.useRichText = true;
    this.title.text = "<b>BGVisualizer v" + VERSION + "</b><br>Visualize in black/white the flatness of the background.<br>Copyright &copy; 2023-2024 J&uuml;rgen B&auml;tz";

    // initialize preview with dummy data

    gParameters.workingPreview = new ImageWindow(1, 1, 3, 32, true, true, OUTPUTVIEWID);

    var i = new Image(4000, 4000, 3, 1, 32, 1);
    gParameters.workingPreview.mainView.beginProcess(UndoFlag_NoSwapFile);
    gParameters.workingPreview.mainView.image.assign(i);
    gParameters.workingPreview.mainView.endProcess();
    var metadata = {
        width: 4000,
        height: 4000
    }

    this.previewControl = new PreviewControl(this);
    this.previewControl.SetImage(gParameters.workingPreview.mainView.image.render(), metadata);

    this.targetViewLabel = new Label(this);
    this.targetViewLabel.text = "Target view:";

    this.targetViewList = new ViewList(this);
    this.targetViewList.getMainViews();
    var previewView = View.viewById(OUTPUTVIEWID);
    this.targetViewList.remove(previewView);
    this.targetViewList.setFixedWidth(290);
    if (gParameters.targetView)
        this.targetViewList.currentView = gParameters.targetView;
    this.targetViewList.onViewSelected = function (view) {
        gParameters.targetView = view;
        if (!firstrun) {
            var i = new Image(gParameters.targetView.image.width, gParameters.targetView.image.height, 3, 1, 32, 1);
            gParameters.workingPreview.mainView.beginProcess(UndoFlag_NoSwapFile);
            gParameters.workingPreview.mainView.image.assign(i);
            gParameters.workingPreview.mainView.endProcess();
            firstrun = true;
        }
        CopyViewContentWithPM(gParameters.targetView, gParameters.workingPreview.mainView);
        var metadata = {
            width: gParameters.targetView.image.width,
            height: gParameters.targetView.image.height
        }
        ResetToMean(gParameters.targetView, gParameters.workingPreview.mainView);
        this.parent.parent.previewControl.SetImage(gParameters.workingPreview.mainView.image.render(), metadata);
        this.parent.parent.previewControl.forceRedraw();
        this.parent.parent.threshRawControl.setValue(gParameters.threshRaw);
        gParameters.previewed = true;
    }

    this.targetSizer = new HorizontalSizer;
    this.targetSizer.add(this.targetViewLabel)
    this.targetSizer.add(this.targetViewList)
    this.targetSizer.textAlignment = TextAlign_Left | TextAlign_VertCenter;

    //  grayscale switch

    this.greyscaleBox = new CheckBox(this);
    this.greyscaleBox.text = 'Grayscale';
    this.greyscaleBox.checked = gParameters.greyscale;
    this.greyscaleBox.toolTip = 'Grayscale conversion';
    this.greyscaleBox.onClick = () => {
        gParameters.greyscale = this.greyscaleBox.checked;
        if (gParameters.targetView) {
            Visualize(gParameters.threshRaw, gParameters.targetView, gParameters.workingPreview.mainView);
            this.previewControl.SetImage(gParameters.workingPreview.mainView.image.render(), metadata);
            this.previewControl.forceRedraw();
            this.threshRawControl.setValue(gParameters.threshRaw);
            gParameters.previewed = true;
        }
    };

    //  threshold slider raw

    this.threshRawControl = new NumericControl(this);
    this.threshRawControl.label.text = "Threshold:";
    this.threshRawControl.label.width = 60;
    this.threshRawControl.setRange(0, 1);
    this.threshRawControl.setPrecision(7);
    this.threshRawControl.setValue(gParameters.threshRaw);
    this.threshRawControl.slider.setRange(0, 100);
    this.threshRawControl.toolTip = "<p>Sets the amount of threshold.</p>";
    this.threshRawControl.onValueUpdated = function (value) {
        if (gParameters.targetView) {
            gParameters.threshRaw = value;
            Visualize(value, gParameters.targetView, gParameters.workingPreview.mainView);
            this.parent.parent.previewControl.SetImage(gParameters.workingPreview.mainView.image.render(), metadata);
            this.parent.parent.previewControl.forceRedraw();
            gParameters.previewed = true;
        }
    };

    //  threshold slider fine

    this.threshFineSlider = new Slider(this);
    this.threshFineSlider.setRange(0, 1000.0);
    this.threshFineSlider.value = 500.0;
    this.threshFineSlider.onValueUpdated = function (value) {
        if (gParameters.targetView) {
            value = (value-500.0)/10000000.0;
            gParameters.threshRaw += value;
            if (gParameters.threshRaw < 0.0000000)
                gParameters.threshRaw = 0.0000000;
            else if (gParameters.threshRaw > 1.0)
                gParameters.threshRaw = 1.0;
            this.parent.parent.threshRawControl.setValue(gParameters.threshRaw);
            Visualize(gParameters.threshRaw, gParameters.targetView, gParameters.workingPreview.mainView);
            this.parent.parent.previewControl.SetImage(gParameters.workingPreview.mainView.image.render(), metadata);
            this.parent.parent.previewControl.forceRedraw();
            gParameters.previewed = true;
        }
    };
    this.threshFineSlider.onMouseRelease = function (value) {
        this.value = 500.0;
    };

    //  reset to mean

    this.meanButton = new PushButton(this);
    this.meanButton.text = "Reset to Mean";
    this.meanButton.icon = this.scaledResource(":/icons/refresh.png");
    this.meanButton.onClick = () => {
        if (gParameters.targetView) {
            ResetToMean(gParameters.targetView, gParameters.workingPreview.mainView);
            this.previewControl.SetImage(gParameters.workingPreview.mainView.image.render(), metadata);
            this.previewControl.forceRedraw();
            this.threshRawControl.setValue(gParameters.threshRaw);
            gParameters.previewed = true;
        }
    };

    this.settingsGroupBox = new GroupBox(this);
    this.settingsGroupBox.title = "Settings";
    this.settingsGroupBox.sizer = new VerticalSizer;
    this.settingsGroupBox.sizer.margin = 8;
    this.settingsGroupBox.sizer.spacing = 8;
    this.settingsGroupBox.sizer.add(this.targetSizer);
    this.settingsGroupBox.sizer.addSpacing(8);
    this.settingsGroupBox.sizer.add(this.greyscaleBox);
    this.settingsGroupBox.sizer.addSpacing(8);
    this.settingsGroupBox.sizer.add(this.threshRawControl);
    this.settingsGroupBox.sizer.addSpacing(8);
    this.settingsGroupBox.sizer.add(this.threshFineSlider);
    this.settingsGroupBox.sizer.addSpacing(8);
    this.settingsGroupBox.sizer.add(this.meanButton);
    this.settingsGroupBox.scaledMinWidth = SCALEDRIGHTWIDTH;
    this.settingsGroupBox.scaledMaxWidth = SCALEDRIGHTWIDTH;

    //	message area

    this.messageLabel = new Label(this);
    this.messageLabel.text = "Ready.";
    this.messageLabel.frameStyle = FrameStyle_Sunken;
    this.messageLabel.margin = 4;

    //	lower button row

    this.helpButton = new PushButton(this);
    this.helpButton.text = "Help";
    this.helpButton.icon = this.scaledResource(":/icons/document-text.png");
    this.helpButton.width = 20;
    this.helpButton.onClick = () => {
       Dialog.browseScriptDocumentation("BGVisualizer");
    };

    this.cancelButton = new PushButton(this);
    this.cancelButton.text = "Cancel";
    this.cancelButton.icon = this.scaledResource(":/icons/cancel.png");
    this.cancelButton.width = 20;
    this.cancelButton.onClick = () => {
        Cleanup();
        this.dialog.cancel();
    };

    this.execButton = new PushButton(this);
    this.execButton.text = "Preview";
    this.execButton.icon = this.scaledResource(":/icons/execute.png");
    this.execButton.width = 20;
    this.execButton.onClick = () => {
        error = false;
        if (gParameters.targetView === undefined) {
            this.messageLabel.text = "Please select your view.";
            error = true;
        }
        if (!error) {
            this.messageLabel.text = "Computing...";
            error = false;
            Visualize(gParameters.threshRaw, gParameters.targetView, gParameters.workingPreview.mainView);
            var metadata = {
                width: gParameters.targetView.image.width,
                height: gParameters.targetView.image.height
            }
            this.previewControl.SetImage(gParameters.workingPreview.mainView.image.render(), metadata);
            this.previewControl.forceRedraw();
            this.messageLabel.text = "Ready.";
            gParameters.previewed = true;
        }
    };

    this.doneButton = new PushButton(this);
    this.doneButton.text = "Done";
    this.doneButton.icon = this.scaledResource(":/icons/ok.png");
    this.doneButton.width = 20;
    this.doneButton.onClick = () => {
        Cleanup();
        this.dialog.ok();
    };

    this.buttonSizer = new HorizontalSizer;
    this.buttonSizer.add(this.helpButton);
    this.buttonSizer.addSpacing(20);
    this.buttonSizer.add(this.cancelButton);
    this.buttonSizer.addSpacing(20);
    this.buttonSizer.add(this.execButton);
    this.buttonSizer.addSpacing(20);
    this.buttonSizer.add(this.doneButton);
    this.buttonSizer.scaledMinWidth = SCALEDRIGHTWIDTH;
    this.buttonSizer.scaledMaxWidth = SCALEDRIGHTWIDTH;

    // layout the dialog

    this.sizerRight = new VerticalSizer;
    this.sizerRight.margin = 8;
    this.sizerRight.add(this.title);
    this.sizerRight.addStretch();
    this.sizerRight.addSpacing(8);
    this.sizerRight.add(this.settingsGroupBox);
    this.sizerRight.addSpacing(20);
    this.sizerRight.add(this.messageLabel);
    this.sizerRight.addSpacing(20);
    this.sizerRight.add(this.buttonSizer);


    // Add create instance button to the lower left corner
    this.newInstanceButton = new ToolButton(this);
    this.newInstanceButton.icon = this.scaledResource(":/process-interface/new-instance.png");
    this.newInstanceButton.setScaledFixedSize(24, 24);
    this.newInstanceButton.toolTip = "New Instance";
    this.newInstanceButton.onMousePress = () => {
        // stores the parameters
        gParameters.save();
        // create the script instance
        this.newInstance();
    };

    this.newInstanceSizer = new VerticalSizer;
    this.newInstanceSizer.margin = 4;
    this.newInstanceSizer.addStretch();
    this.newInstanceSizer.add(this.newInstanceButton);

    this.sizer = new HorizontalSizer;
    this.sizer.margin = 8;
    this.sizer.add(this.newInstanceSizer);
    this.sizer.add(this.previewControl);
    this.sizer.add(this.sizerRight);

}

BGVisualizerDialog.prototype = new Dialog;

/*
 * Preview Control
 *
 * This file is part of the AnnotateImage script
 *
 * Copyright (C) 2013-2020, Andres del Pozo
 * Contributions (C) 2019-2020, Juan Conejero (PTeam)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <pjsr/ButtonCodes.jsh>
#include <pjsr/StdCursor.jsh>

    function PreviewControl(parent) {
        this.__base__ = Frame;
        this.__base__(parent);

        this.SetImage = function (image, metadata) {
            this.image = image;
            this.metadata = metadata;
            this.scaledImage = null;
            this.SetZoomOutLimit();
            this.UpdateZoom(this.zoom);
        };

        this.UpdateZoom = function (newZoom, refPoint) {
            newZoom = Math.max(this.zoomOutLimit, Math.min(4, newZoom));
            if (newZoom == this.zoom && this.scaledImage)
                return;

            if (refPoint == null)
                refPoint = new Point(this.scrollbox.viewport.width / 2, this.scrollbox.viewport.height / 2);

            let imgx = null;
            if (this.scrollbox.maxHorizontalScrollPosition > 0)
                imgx = (refPoint.x + this.scrollbox.horizontalScrollPosition) / this.scale;

            let imgy = null;
            if (this.scrollbox.maxVerticalScrollPosition > 0)
                imgy = (refPoint.y + this.scrollbox.verticalScrollPosition) / this.scale;

            this.zoom = newZoom;
            this.scaledImage = null;
            gc(true);

            if (this.zoom > 0) {
                this.scale = this.zoom;
                this.zoomVal_Label.text = format("%d:1", this.zoom);
            } else {
                this.scale = 1 / (-this.zoom + 2);
                this.zoomVal_Label.text = format("1:%d", -this.zoom + 2);
            }

            if (this.image)
                this.scaledImage = this.image.scaled(this.scale);
            else
                this.scaledImage = {
                    width: this.metadata.width * this.scale,
                    height: this.metadata.height * this.scale
                };

            this.scrollbox.maxHorizontalScrollPosition = Math.max(0, this.scaledImage.width - this.scrollbox.viewport.width);
            this.scrollbox.maxVerticalScrollPosition = Math.max(0, this.scaledImage.height - this.scrollbox.viewport.height);

            if (this.scrollbox.maxHorizontalScrollPosition > 0 && imgx != null)
                this.scrollbox.horizontalScrollPosition = imgx * this.scale - refPoint.x;
            if (this.scrollbox.maxVerticalScrollPosition > 0 && imgy != null)
                this.scrollbox.verticalScrollPosition = imgy * this.scale - refPoint.y;

            this.scrollbox.viewport.update();
        };

        this.zoomIn_Button = new ToolButton(this);
        this.zoomIn_Button.icon = this.scaledResource(":/icons/zoom-in.png");
        this.zoomIn_Button.setScaledFixedSize(24, 24);
        this.zoomIn_Button.toolTip = "Zoom In";
        this.zoomIn_Button.onMousePress = function () {
            this.parent.UpdateZoom(this.parent.zoom + 1);
        };

        this.zoomOut_Button = new ToolButton(this);
        this.zoomOut_Button.icon = this.scaledResource(":/icons/zoom-out.png");
        this.zoomOut_Button.setScaledFixedSize(24, 24);
        this.zoomOut_Button.toolTip = "Zoom Out";
        this.zoomOut_Button.onMousePress = function () {
            this.parent.UpdateZoom(this.parent.zoom - 1);
        };

        this.zoom11_Button = new ToolButton(this);
        this.zoom11_Button.icon = this.scaledResource(":/icons/zoom-1-1.png");
        this.zoom11_Button.setScaledFixedSize(24, 24);
        this.zoom11_Button.toolTip = "Zoom 1:1";
        this.zoom11_Button.onMousePress = function () {
            this.parent.UpdateZoom(1);
        };

        this.buttons_Sizer = new HorizontalSizer;
        this.buttons_Sizer.margin = 4;
        this.buttons_Sizer.spacing = 4;
        this.buttons_Sizer.add(this.zoomIn_Button);
        this.buttons_Sizer.add(this.zoomOut_Button);
        this.buttons_Sizer.add(this.zoom11_Button);
        this.buttons_Sizer.addStretch();

        this.setScaledMinSize(600, 400);
        this.zoom = 1;
        this.scale = 1;
        this.zoomOutLimit = -5;
        this.scrollbox = new ScrollBox(this);
        this.scrollbox.autoScroll = true;
        this.scrollbox.tracking = true;
        this.scrollbox.cursor = new Cursor(StdCursor_OpenHand);

        this.scroll_Sizer = new HorizontalSizer;
        this.scroll_Sizer.add(this.scrollbox);

        this.SetZoomOutLimit = function () {
            let scaleX = Math.ceil(this.metadata.width / this.scrollbox.viewport.width);
            let scaleY = Math.ceil(this.metadata.height / this.scrollbox.viewport.height);
            let scale = Math.max(scaleX, scaleY);
            this.zoomOutLimit = -scale + 2;
        };

        this.scrollbox.onHorizontalScrollPosUpdated = function (newPos) {
            this.viewport.update();
        };

        this.scrollbox.onVerticalScrollPosUpdated = function (newPos) {
            this.viewport.update();
        };

        this.forceRedraw = function () {
            this.scrollbox.viewport.update();
        };

        this.scrollbox.viewport.onMouseWheel = function (x, y, delta, buttonState, modifiers) {
            let preview = this.parent.parent;
            preview.UpdateZoom(preview.zoom + ((delta > 0) ? -1 : 1), new Point(x, y));
        };

        this.scrollbox.viewport.onMousePress = function (x, y, button, buttonState, modifiers) {
            let preview = this.parent.parent;
            // if (button == MouseButton_Right) {
            //     this.cursor = new Cursor(StdCursor_Cross);
            //     return;
            // }
            if (preview.scrolling || button != MouseButton_Left)
                return;
            preview.scrolling = {
                orgCursor: new Point(x, y),
                orgScroll: new Point(preview.scrollbox.horizontalScrollPosition, preview.scrollbox.verticalScrollPosition)
            };
            this.cursor = new Cursor(StdCursor_ClosedHand);
        };

        this.scrollbox.viewport.onMouseMove = function (x, y, buttonState, modifiers) {
            let preview = this.parent.parent;
            if (preview.scrolling) {
                preview.scrollbox.horizontalScrollPosition = preview.scrolling.orgScroll.x - (x - preview.scrolling.orgCursor.x);
                preview.scrollbox.verticalScrollPosition = preview.scrolling.orgScroll.y - (y - preview.scrolling.orgCursor.y);
            }

            let ox = (this.parent.maxHorizontalScrollPosition > 0) ?
                -this.parent.horizontalScrollPosition : (this.width - preview.scaledImage.width) / 2;
            let oy = (this.parent.maxVerticalScrollPosition > 0) ?
                -this.parent.verticalScrollPosition : (this.height - preview.scaledImage.height) / 2;
            let coordPx = new Point((x - ox) / preview.scale, (y - oy) / preview.scale);
            /*    if ( coordPx.x < 0 ||
            coordPx.x > preview.metadata.width ||
            coordPx.y < 0 ||
            coordPx.y > preview.metadata.height ||
            !preview.metadata.projection ){
            preview.Xval_Label.text = "---";
            preview.Yval_Label.text = "---";
            preview.RAval_Label.text = "---";
            preview.Decval_Label.text = "---";
            }
            else{
            try{
            preview.Xval_Label.text = format( "%8.2f", coordPx.x );
            preview.Yval_Label.text = format( "%8.2f", coordPx.y );
            let coordRD = preview.metadata.Convert_I_RD( coordPx );
            if ( coordRD.x < 0 )
            coordRD.x += 360;
            preview.RAval_Label.text = DMSangle.FromAngle( coordRD.x*24/360 ).ToString( true );
            preview.Decval_Label.text = DMSangle.FromAngle( coordRD.y ).ToString( false );
            }
            catch ( ex ){
            preview.Xval_Label.text = "---";
            preview.Yval_Label.text = "---";
            preview.RAval_Label.text = "---";
            preview.Decval_Label.text = "---";
            }
            } */
        };

        this.scrollbox.viewport.onMouseRelease = function (x, y, button, buttonState, modifiers) {
            let preview = this.parent.parent;
            if (preview.scrolling && button == MouseButton_Left) {
                preview.scrollbox.horizontalScrollPosition = preview.scrolling.orgScroll.x - (x - preview.scrolling.orgCursor.x);
                preview.scrollbox.verticalScrollPosition = preview.scrolling.orgScroll.y - (y - preview.scrolling.orgCursor.y);
                preview.scrolling = null;
                this.cursor = new Cursor(StdCursor_OpenHand);
            }
            // if (button == MouseButton_Right) {
            //     this.cursor = new Cursor(StdCursor_OpenHand);
            // }
        };

        this.scrollbox.viewport.onResize = function (wNew, hNew, wOld, hOld) {
            let preview = this.parent.parent;
            if (preview.metadata && preview.scaledImage) {
                this.parent.maxHorizontalScrollPosition = Math.max(0, preview.scaledImage.width - wNew);
                this.parent.maxVerticalScrollPosition = Math.max(0, preview.scaledImage.height - hNew);
                preview.SetZoomOutLimit();
                preview.UpdateZoom(preview.zoom);
            }
            this.update();
        };

        this.scrollbox.viewport.onPaint = function (x0, y0, x1, y1) {
            let preview = this.parent.parent;
            let graphics = new VectorGraphics(this);

            graphics.fillRect(x0, y0, x1, y1, new Brush(0xff202020));

            let offsetX = (this.parent.maxHorizontalScrollPosition > 0) ?
                -this.parent.horizontalScrollPosition : (this.width - preview.scaledImage.width) / 2;
            let offsetY = (this.parent.maxVerticalScrollPosition > 0) ?
                -this.parent.verticalScrollPosition : (this.height - preview.scaledImage.height) / 2;
            graphics.translateTransformation(offsetX, offsetY);

            if (preview.image)
                graphics.drawBitmap(0, 0, preview.scaledImage);
            else
                graphics.fillRect(0, 0, preview.scaledImage.width, preview.scaledImage.height, new Brush(0xff000000));

            graphics.pen = new Pen(0xffffffff, 0);
            graphics.drawRect(-1, -1, preview.scaledImage.width + 1, preview.scaledImage.height + 1);

            if (preview.onCustomPaint) {
                graphics.antialiasing = true;
                graphics.scaleTransformation(preview.scale, preview.scale);
                preview.onCustomPaint.call(preview.onCustomPaintScope, graphics, x0, y0, x1, y1);
            }

            graphics.end();
        };

        this.zoomLabel_Label = new Label(this);
        this.zoomLabel_Label.text = "Zoom:";
        this.zoomVal_Label = new Label(this);
        this.zoomVal_Label.text = "1:1";
        /*

        this.Xlabel_Label = new Label( this );
        this.Xlabel_Label.text = "X:";
        this.Xval_Label = new Label( this );
        this.Xval_Label.text = "---";

        this.Ylabel_Label = new Label( this );
        this.Ylabel_Label.text = "Y:";
        this.Yval_Label = new Label( this );
        this.Yval_Label.text = "---";

        this.RAlabel_Label = new Label( this );
        this.RAlabel_Label.text = "\u03B1:";
        this.RAval_Label = new Label( this );
        this.RAval_Label.text = "---";

        this.Declabel_Label = new Label( this );
        this.Declabel_Label.text = "\u03B4:";
        this.Decval_Label = new Label( this );
        this.Decval_Label.text = "---";
         */
        this.coords_Frame = new Frame(this);
        this.coords_Frame.styleSheet = this.scaledStyleSheet(
            "QLabel { font-family: Hack; background: white; }");
        this.coords_Frame.backgroundColor = 0xffffffff;
        this.coords_Frame.sizer = new HorizontalSizer;
        this.coords_Frame.sizer.margin = 4;
        this.coords_Frame.sizer.spacing = 4;
        this.coords_Frame.sizer.add(this.zoomLabel_Label);
        this.coords_Frame.sizer.add(this.zoomVal_Label);
        /* this.coords_Frame.sizer.addSpacing( 20 );
        this.coords_Frame.sizer.add( this.Xlabel_Label );
        this.coords_Frame.sizer.add( this.Xval_Label );
        this.coords_Frame.sizer.addSpacing( 20 );
        this.coords_Frame.sizer.add( this.Ylabel_Label );
        this.coords_Frame.sizer.add( this.Yval_Label );
        this.coords_Frame.sizer.addSpacing( 20 );
        this.coords_Frame.sizer.add( this.RAlabel_Label );
        this.coords_Frame.sizer.add( this.RAval_Label );
        this.coords_Frame.sizer.addSpacing( 20 );
        this.coords_Frame.sizer.add( this.Declabel_Label );
        this.coords_Frame.sizer.add( this.Decval_Label );  */
        this.coords_Frame.sizer.addStretch();

        this.sizer = new VerticalSizer;
        this.sizer.add(this.buttons_Sizer);
        this.sizer.add(this.scroll_Sizer);
        this.sizer.add(this.coords_Frame);
    }

PreviewControl.prototype = new Frame;

function Cleanup() {
    if (!gParameters.workingPreview.isNull) {
        gParameters.workingPreview.forceClose();
    }
}

function main() {

    // ImageWindow
    gParameters.imageActiveWindow = new ImageWindow(ImageWindow.activeWindow);

    // Validations
    if (gParameters.imageActiveWindow === undefined) {

        console.show();
        Console.warningln("<p>No active image.</p>");

    } else if (gParameters.imageActiveWindow.currentView.isPreview) {

        console.show();
        Console.warningln("<p>This script can not work on prewiews.</p>");

    } else {

        // is script started from an instance in global or view target context?
        if (Parameters.isGlobalTarget || Parameters.isViewTarget) {
            // then load the parameters from the instance and continue
            gParameters.load();
        }

        // create and show the dialog
        let dialog = new BGVisualizerDialog;
        dialog.execute();
        if (!gParameters.doneButtonPressed) {
            Cleanup();
            if (gParameters.previewed) {
                // gParameters.view.window.forceClose();
            }
            dialog.cancel();
        }
    }

}

main();
