// NBStarColorsFromRGB
// version 1.20 03/06/2024
// by Jürgen Bätz
// using PreviewControl.js by PI Team

#include <pjsr/ColorSpace.jsh>
#include <pjsr/UndoFlag.jsh>
#include <pjsr/Sizer.jsh>
#include <pjsr/FrameStyle.jsh>
#include <pjsr/NumericControl.jsh>

#feature-id NBStarColorsFromRGB : Toolbox > NBStarColorsFromRGB

#feature-info  Script to automate the a-b channel exchange to get better star colors.<br/>\
   <br/> \
   This script extracts the RGB stars and replaces them in the target narrowband image using their a and b components in CIELAB color space. A star mask is required.\
   <br/> \
   <br/>Copyright &copy; 2022-2024 J&uuml;rgen B&auml;tz

#define OUTPUTVIEWID	"NBStarColorsFromRGBView"
#define VERSION			1.20
#define SCALEDWIDTH     1200
#define SCALEDHEIGHT     600
#define SCALEDRIGHTWIDTH 460

// define a global variable containing script's parameters

var previewed = false;

var gParameters = {
    reductionAmount: 0,
    colorAmount: 0,
    sourceView: undefined,
    targetView: undefined,
    UUID: undefined,
    doneButtonPressed: false,
    rgbExtracted: false,
    nbExtracted: false,

    // stores the current parameters values into the script instance
    save: function() {
        if (gParameters.rgbView)  Parameters.set("rgbView", gParameters.rgbView.id);
        if (gParameters.nbView)  Parameters.set("nbView", gParameters.nbView.id);
        if (gParameters.maskView)  Parameters.set("maskView", gParameters.maskView.id);
        Parameters.set("reductionAmount", gParameters.reductionAmount);
        Parameters.set("colorAmount", gParameters.colorAmount);
    },

    // loads the script instance parameters
    load: function() {
        if (Parameters.has("rgbView")) {
            var id = Parameters.getString("rgbView");
            gParameters.rgbView = View.viewById(id);
        }
        if (Parameters.has("nbView")) {
            var id = Parameters.getString("nbView");
            gParameters.nbView = View.viewById(id);
        }
        if (Parameters.has("maskView")) {
            var id = Parameters.getString("maskView");
            gParameters.maskView = View.viewById(id);
        }
        if (Parameters.has("reductionAmount"))
        gParameters.reductionAmount = Parameters.getReal("reductionAmount")
        if (Parameters.has("colorAmount"))
        gParameters.colorAmount = Parameters.getReal("colorAmount")
    }
}

function ExtractABChannels(view) {

    SetRGBWS(view);

    var P = new ChannelExtraction;
    P.colorSpace = ChannelExtraction.prototype.CIELab;
    P.channels = [// enabled, id
        [false, ""],
        [true, view.id + "_a" + gParameters.UUID],
        [true, view.id + "_b" + gParameters.UUID]
    ];
    P.sampleFormat = ChannelExtraction.prototype.SameAsSource;
	P.inheritAstrometricSolution = false;
    P.executeOn(view, false);
    gParameters.rgbExtracted = true;

}

function ExtractLABChannels(view) {

    SetRGBWS(view);

    var P = new ChannelExtraction;
    P.colorSpace = ChannelExtraction.prototype.CIELab;
    P.channels = [// enabled, id
        [true, view.id + "_L" + gParameters.UUID],
        [true, view.id + "_a" + gParameters.UUID],
        [true, view.id + "_b" + gParameters.UUID]
    ];
    P.sampleFormat = ChannelExtraction.prototype.SameAsSource;
	P.inheritAstrometricSolution = false;
    P.executeOn(view, false);
    gParameters.nbExtracted = true;

}

function ApplyPixelMath(sourceview, targetview) {

    var P = new PixelMath;
    P.expression = sourceview.id;
    P.expression1 = "";
    P.expression2 = "";
    P.expression3 = "";
    P.useSingleExpression = true;
    P.symbols = "";
    P.clearImageCacheAndExit = false;
    P.cacheGeneratedImages = false;
    P.generateOutput = true;
    P.singleThreaded = false;
    P.optimization = true;
    P.use64BitWorkingImage = false;
    P.rescale = false;
    P.rescaleLower = 0;
    P.rescaleUpper = 1;
    P.truncate = true;
    P.truncateLower = 0;
    P.truncateUpper = 1;
    P.createNewImage = false;
    P.showNewImage = false;
    P.newImageId = "";
    P.newImageWidth = 0;
    P.newImageHeight = 0;
    P.newImageAlpha = false;
    P.newImageColorSpace = PixelMath.prototype.SameAsTarget;
    P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
    P.executeOn(targetview, false);

}

function MorphTrans(view, amount) {

    var P = new MorphologicalTransformation;
    P.operator = MorphologicalTransformation.prototype.Erosion;
    P.interlacingDistance = 1;
    P.lowThreshold = 0.000000;
    P.highThreshold = 0.000000;
    P.numberOfIterations = 1;
    P.amount = 0.50 * amount;
    P.selectionPoint = 0.50;
    P.structureName = "";
    P.structureSize = 5;
    P.structureWayTable = [// mask
        [[
                0x00, 0x01, 0x01, 0x01, 0x00,
                0x01, 0x01, 0x01, 0x01, 0x01,
                0x01, 0x01, 0x01, 0x01, 0x01,
                0x01, 0x01, 0x01, 0x01, 0x01,
                0x00, 0x01, 0x01, 0x01, 0x00
            ]]
    ];
    P.executeOn(view, false);
}

/*
 * This function takes a view and the amount value (between 0 and 1) and
 * applies a linear curves transformation on the C channel of the Lch color
 * space to increase the image saturation.
 */
function LRGBCombine(nbLumView, targetView, amount) {

    var P = new LRGBCombination;
    P.channels = [// enabled, id, k
        [false, "", 1.00000],
        [false, "", 1.00000],
        [false, "", 1.00000],
        [true, nbLumView.id + gParameters.UUID, 1.00000]
    ];
    P.mL = 0.500;
    P.mc = 0.500 - (0.3 * amount);
    P.clipHighlights = true;
    P.noiseReduction = false;
    P.layersRemoved = 1;
    P.layersProtected = 0;
    P.executeOn(targetView, false);
}

/*
 * This function applies the RGBWorkingSpace process with equal factors over all channels.
 */
function SetRGBWS(targetView) {

    var P = new RGBWorkingSpace;
    P.channels = [// Y, x, y
		[0.333333, 0.648431, 0.330856],
		[0.333333, 0.321152, 0.597871],
		[0.333333, 0.155886, 0.066044]
    ];
    P.gamma = 2.20;
    P.sRGBGamma = true;
    P.applyGlobalRGBWS = false;
    P.executeOn(targetView, false);

}

function CloneView(view, newId) {
    let newWindow = new ImageWindow(1, 1, 1, view.window.bitsPerSample, view.window.isFloatSample, view.image.colorSpace != ColorSpace_Gray, newId);
    newWindow.mainView.beginProcess(UndoFlag_NoSwapFile);
    newWindow.mainView.image.assign(view.image);
    newWindow.mainView.endProcess();
    return newWindow.mainView;
}

function ReplaceStarColors(rgbView, nbView, maskView, targetView, reduction, colorboost) {

    // extract RGB into Lab channels if not yet done
    if (!gParameters.rgbExtracted) {
        ExtractABChannels(rgbView);
    }

    var rgbView_a = View.viewById(rgbView.id + "_a" + gParameters.UUID);
    var rgbView_b = View.viewById(rgbView.id + "_b" + gParameters.UUID);

    rgbView_a.window.visible = false;
    rgbView_b.window.visible = false;

    // extract Narrowband into Lab channels if not yet done
    if (!gParameters.nbExtracted) {
        ExtractLABChannels(nbView);
    }

    var nbView_L = View.viewById(nbView.id + "_L" + gParameters.UUID);
    var nbView_a = View.viewById(nbView.id + "_a" + gParameters.UUID);
    var nbView_b = View.viewById(nbView.id + "_b" + gParameters.UUID);

    nbView_L.window.visible = false;
    nbView_a.window.visible = false;
    nbView_b.window.visible = false;

    // work is done on a copy
    CloneView(nbView_L, "workingView_L");
    CloneView(nbView_a, "workingView_a");
    CloneView(nbView_b, "workingView_b");

    var workingView_L = View.viewById("workingView_L");
    var workingView_a = View.viewById("workingView_a");
    var workingView_b = View.viewById("workingView_b");

    // mask prep
    if (maskView.image.colorSpace != ColorSpace_Gray) {
        var toGray = new ConvertToGrayscale;
        maskView.beginProcess(UndoFlag_NoSwapFile);
        toGray.executeOn(maskView);
        maskView.endProcess();
    }

    // morphological trans of star cores (masked L)
    if (reduction > 0) {
        workingView_L.window.mask = maskView.window;
        MorphTrans(workingView_L, reduction);
        workingView_L.window.maskEnabled = false;
    }

    // transfer a and b channels from RGB to Narrowband (masked)
    workingView_a.window.mask = maskView.window;
    workingView_b.window.mask = maskView.window;
    workingView_a.window.maskVisible = false;
    workingView_b.window.maskVisible = false;

    ApplyPixelMath(rgbView_a, workingView_a);
    ApplyPixelMath(rgbView_b, workingView_b);

    workingView_a.window.maskEnabled = false;
    workingView_b.window.maskEnabled = false;

    SetRGBWS(targetView);

    // recombine L+a+b
    var P = new ChannelCombination;
    P.colorSpace = ChannelCombination.prototype.CIELab;
    P.channels = [// enabled, id
        [true, workingView_L.id],
        [true, workingView_a.id],
        [true, workingView_b.id]
    ];
	P.inheritAstrometricSolution = false;
    P.executeOn(targetView, false);

    // color boost using LRGBCombination
    if (colorboost > 0) {
        targetView.window.mask = maskView.window;
        targetView.window.maskVisible = false;
        LRGBCombine(workingView_L, targetView, colorboost);
        targetView.window.removeMask()
    }

    gParameters.rgbView_a = rgbView_a;
    gParameters.rgbView_b = rgbView_b;
    gParameters.nbView_L = nbView_L;
    gParameters.nbView_a = nbView_a;
    gParameters.nbView_b = nbView_b;

    workingView_L.window.forceClose();
    workingView_a.window.forceClose();
    workingView_b.window.forceClose();

}

/*
 * Construct the script dialog interface
 */
function NBStarColorsFromRGBDialog() {
    this.__base__ = Dialog;
    this.__base__();

    this.userResizable = true;

    this.scaledMinWidth = SCALEDWIDTH;
    this.scaledMinHeight = SCALEDHEIGHT;

    var error = false;

    this.title = new Label( this );
    this.title.frameStyle = FrameStyle_Box;
    this.title.margin = 6;
    this.title.wordWrapping = true;
    this.title.useRichText = true;
    this.title.text = "<b>NBStarColorsFromRGB v"+VERSION+"</b><br><br>This script extracts" +
        " the RGB stars and replaces them in the target narrowband image" +
        " using their <b>a</b> and <b>b</b> components. A star mask is required. <br>\
        <br>Copyright &copy; 2022-2024 J&uuml;rgen B&auml;tz";

    // initialize preview with dummy data

    gParameters.workingPreview = new ImageWindow(1, 1, 3, 32, true, true, OUTPUTVIEWID);

    var i = new Image(4000, 4000, 3, 1, 32, 1);
    // var i = new Image(gParameters.imageActiveWindow.mainView.image.width, gParameters.imageActiveWindow.mainView.image.height, 3, 1, 32, 1);
    gParameters.workingPreview.mainView.beginProcess(UndoFlag_NoSwapFile);
    gParameters.workingPreview.mainView.image.assign(i);
    gParameters.workingPreview.mainView.endProcess();
    var metadata = {
        width: 4000,
        height: 4000
    }

    this.previewControl = new PreviewControl(this);
    this.previewControl.SetImage(gParameters.workingPreview.mainView.image.render(), metadata);

    this.rgbViewLabel = new Label(this);
    this.rgbViewLabel.text = "Source view (RGB):";

    this.rgbViewList = new ViewList(this);
    this.rgbViewList.getMainViews();
	this.rgbViewList.setFixedWidth( 290 );
    if ( gParameters.rgbView )
        this.rgbViewList.currentView = gParameters.rgbView;
    this.rgbViewList.onViewSelected = function (view) {
        gParameters.rgbView = view;
    }

    this.sourceSizer = new HorizontalSizer;
    this.sourceSizer.add(this.rgbViewLabel)
    this.sourceSizer.add(this.rgbViewList)
    this.sourceSizer.textAlignment = TextAlign_Left | TextAlign_VertCenter;
    this.sourceSizer.scaledMinWidth = SCALEDRIGHTWIDTH;
    this.sourceSizer.scaledMaxWidth = SCALEDRIGHTWIDTH;

    this.nbViewLabel = new Label(this);
    this.nbViewLabel.text = "Target view (NB):";

    this.nbViewList = new ViewList(this);
    this.nbViewList.getMainViews();
	this.nbViewList.setFixedWidth( 290 );
    if ( gParameters.nbView )
        this.nbViewList.currentView = gParameters.nbView;
    this.nbViewList.onViewSelected = function (view) {
        gParameters.nbView = view;
    }

    this.targetSizer = new HorizontalSizer;
    this.targetSizer.add(this.nbViewLabel)
    this.targetSizer.add(this.nbViewList)
    this.targetSizer.textAlignment = TextAlign_Left | TextAlign_VertCenter;
    this.targetSizer.scaledMinWidth = SCALEDRIGHTWIDTH;
    this.targetSizer.scaledMaxWidth = SCALEDRIGHTWIDTH;

    this.maskViewLabel = new Label(this);
    this.maskViewLabel.text = "Star mask:";

    this.maskViewList = new ViewList(this);
    this.maskViewList.getMainViews();
	this.maskViewList.setFixedWidth( 290 );
    if ( gParameters.maskView )
        this.maskViewList.currentView = gParameters.maskView;
    this.maskViewList.onViewSelected = function (view) {
        gParameters.maskView = view;
    }

    this.maskSizer = new HorizontalSizer;
    this.maskSizer.add(this.maskViewLabel)
    this.maskSizer.add(this.maskViewList)
    this.maskSizer.textAlignment = TextAlign_Left | TextAlign_VertCenter;
    this.maskSizer.scaledMinWidth = SCALEDRIGHTWIDTH;
    this.maskSizer.scaledMaxWidth = SCALEDRIGHTWIDTH;

    this.reductionAmountControl = new NumericControl(this);
    this.reductionAmountControl.label.text = "Core reduction:";
    this.reductionAmountControl.label.width = 60;
    this.reductionAmountControl.setRange(0, 1);
    this.reductionAmountControl.setPrecision(1);
    this.reductionAmountControl.setValue(gParameters.reductionAmount);
    this.reductionAmountControl.slider.setRange(0, 100);
    this.reductionAmountControl.toolTip = "<p>Sets the amount of star core intensity reduction.</p>";
    this.reductionAmountControl.onValueUpdated = function (value) {
        gParameters.reductionAmount = value;
    };

    this.colorAmountControl = new NumericControl(this);
    this.colorAmountControl.label.text = "Color boost:";
    this.colorAmountControl.label.width = 60;
    this.colorAmountControl.setRange(0, 1);
    this.colorAmountControl.setPrecision(1);
    this.colorAmountControl.setValue(gParameters.colorAmount);
    this.colorAmountControl.slider.setRange(0, 100);
    this.colorAmountControl.toolTip = "<p>Sets the amount of color boost.</p>";
    this.colorAmountControl.onValueUpdated = function (value) {
        gParameters.colorAmount = value;
    };

    this.settingsGroupBox = new GroupBox(this);
    this.settingsGroupBox.title = "Settings";
    this.settingsGroupBox.sizer = new VerticalSizer;
    this.settingsGroupBox.sizer.margin = 8;
    this.settingsGroupBox.sizer.spacing = 8;
    this.settingsGroupBox.sizer.add(this.reductionAmountControl);
    this.settingsGroupBox.sizer.addSpacing(8);
    this.settingsGroupBox.sizer.add(this.colorAmountControl);
    this.settingsGroupBox.scaledMinWidth = SCALEDRIGHTWIDTH;
    this.settingsGroupBox.scaledMaxWidth = SCALEDRIGHTWIDTH;

    //	message area

    this.messageLabel = new Label(this);
    this.messageLabel.text = "Ready.";
	this.messageLabel.frameStyle = FrameStyle_Sunken;
	this.messageLabel.margin = 4;

    //	lower button row

    this.helpButton = new PushButton(this);
    this.helpButton.text = "Help";
    this.helpButton.icon = this.scaledResource(":/icons/document-text.png");
    this.helpButton.width = 20;
    this.helpButton.onClick = () => {
       Dialog.browseScriptDocumentation("NBStarColorsFromRGB");
    };

    this.cancelButton = new PushButton(this);
    this.cancelButton.text = "Cancel";
    this.cancelButton.icon = this.scaledResource(":/icons/cancel.png");
    this.cancelButton.width = 20;
    this.cancelButton.onClick = () => {
        Cleanup();
        this.dialog.cancel();
    };

    this.execButton = new PushButton(this);
    this.execButton.text = "Preview";
    this.execButton.icon = this.scaledResource(":/icons/execute.png");
    this.execButton.width = 20;
    this.execButton.onClick = () => {
        error = false;
        if (gParameters.rgbView === undefined || gParameters.nbView === undefined || gParameters.maskView === undefined) {
            this.messageLabel.text = "Please select your views and mask.";
            error = true;
        } else if (gParameters.rgbView.image.isGrayscale || gParameters.nbView.image.isGrayscale) {
            this.messageLabel.text = "No RGB image(s) specified. Source and target must be RGB images.";
            error = true;
            if ((gParameters.rgbView.image.width != gParameters.nbView.image.width) ||
                (gParameters.rgbView.image.height != gParameters.nbView.image.height) ||
                (gParameters.nbView.image.width != gParameters.maskView.image.width) ||
                (gParameters.nbView.image.height != gParameters.maskView.image.height)) {

                this.messageLabel.text = "Specified views have different image geometry.";
                error = true;
            }
        }
        if (!error) {
            this.messageLabel.text = "Computing...";
            if ( !previewed ) {
                var i = new Image(gParameters.rgbView.image.width, gParameters.rgbView.image.height, 3, 1, 32, 1);
                gParameters.workingPreview.mainView.beginProcess(UndoFlag_NoSwapFile);
                gParameters.workingPreview.mainView.image.assign(i);
                gParameters.workingPreview.mainView.endProcess();
            }
            previewed = true;
            error = false;
            ReplaceStarColors(gParameters.rgbView, gParameters.nbView, gParameters.maskView, gParameters.workingPreview.mainView, gParameters.reductionAmount, gParameters.colorAmount);
            var metadata = {
                width: gParameters.rgbView.image.width,
                height: gParameters.rgbView.image.height
            }
            this.previewControl.SetImage(gParameters.workingPreview.mainView.image.render(), metadata);
            this.previewControl.forceRedraw();
            this.messageLabel.text = "Ready.";
        }
    };

    this.doneButton = new PushButton(this);
    this.doneButton.text = "Generate";
    this.doneButton.icon = this.scaledResource(":/icons/ok.png");
    this.doneButton.width = 20;
    this.doneButton.onClick = () => {
        error = false;
        if (gParameters.rgbView === undefined || gParameters.nbView === undefined || gParameters.maskView === undefined) {
            this.messageLabel.text = "Please select your views and mask.";
            error = true;
        } else if (gParameters.rgbView.image.isGrayscale || gParameters.nbView.image.isGrayscale) {
            this.messageLabel.text = "No RGB image(s) specified. Source and target must be RGB images.";
            error = true;
            if ((gParameters.rgbView.image.width != gParameters.nbView.image.width) ||
                (gParameters.rgbView.image.height != gParameters.nbView.image.height) ||
                (gParameters.nbView.image.width != gParameters.maskView.image.width) ||
                (gParameters.nbView.image.height != gParameters.maskView.image.height)) {

                this.messageLabel.text = "Specified views have different image geometry.";
                error = true;
            }
        }
        if (!error) {
            this.messageLabel.text = "Computing...";
            if ( !previewed ) {
                var i = new Image(gParameters.rgbView.image.width, gParameters.rgbView.image.height, 3, 1, 32, 1);
                gParameters.workingPreview.mainView.beginProcess(UndoFlag_NoSwapFile);
                gParameters.workingPreview.mainView.image.assign(i);
                gParameters.workingPreview.mainView.endProcess();
            }
            previewed = true;
            error = false;
            gParameters.doneButtonPressed = true;
            ReplaceStarColors(gParameters.rgbView, gParameters.nbView, gParameters.maskView, gParameters.workingPreview.mainView, gParameters.reductionAmount, gParameters.colorAmount);
            gParameters.workingPreview.show();
            gParameters.workingPreview.zoomToOptimalFit();
            this.messageLabel.text = "Ready.";
            if (gParameters.doneButtonPressed) {
                gParameters.rgbView_a.window.forceClose();
                gParameters.rgbView_b.window.forceClose();
                gParameters.nbView_L.window.forceClose();
                gParameters.nbView_a.window.forceClose();
                gParameters.nbView_b.window.forceClose();
            }
            this.dialog.ok();
        }
    };

    this.buttonSizer = new HorizontalSizer;
    this.buttonSizer.add(this.helpButton);
    this.buttonSizer.addSpacing(20);
    this.buttonSizer.add(this.cancelButton);
    this.buttonSizer.addSpacing(20);
    this.buttonSizer.add(this.execButton);
    this.buttonSizer.addSpacing(20);
    this.buttonSizer.add(this.doneButton);
    this.buttonSizer.scaledMinWidth = SCALEDRIGHTWIDTH;
    this.buttonSizer.scaledMaxWidth = SCALEDRIGHTWIDTH;

    // layout the dialog

    this.sizerRight = new VerticalSizer;
    this.sizerRight.margin = 8;
    this.sizerRight.add(this.title);
    this.sizerRight.addStretch();
    this.sizerRight.addSpacing(8);
    this.sizerRight.add(this.sourceSizer);
    this.sizerRight.addSpacing(8);
    this.sizerRight.add(this.targetSizer);
    this.sizerRight.addSpacing(8);
    this.sizerRight.add(this.maskSizer);
    this.sizerRight.addSpacing(8);
    this.sizerRight.add(this.settingsGroupBox);
    this.sizerRight.addSpacing(20);
    this.sizerRight.add(this.messageLabel);
    this.sizerRight.addSpacing(10);
    this.sizerRight.add(this.buttonSizer);

    // Add create instance button to the lower left corner
    this.newInstanceButton = new ToolButton( this );
    this.newInstanceButton.icon = this.scaledResource( ":/process-interface/new-instance.png" );
    this.newInstanceButton.setScaledFixedSize( 24, 24 );
    this.newInstanceButton.toolTip = "New Instance";
    this.newInstanceButton.onMousePress = () => {
        // stores the parameters
        gParameters.save();
        // create the script instance
        this.newInstance();
    };

    this.newInstanceSizer = new VerticalSizer;
    this.newInstanceSizer.margin = 4;
    this.newInstanceSizer.addStretch();
    this.newInstanceSizer.add(this.newInstanceButton);

    this.sizer = new HorizontalSizer;
    this.sizer.margin = 8;
    this.sizer.add(this.newInstanceSizer);
    this.sizer.add(this.previewControl);
    this.sizer.add(this.sizerRight);

}

NBStarColorsFromRGBDialog.prototype = new Dialog;

/*
 * Preview Control
 *
 * This file is part of the AnnotateImage script
 *
 * Copyright (C) 2013-2020, Andres del Pozo
 * Contributions (C) 2019-2020, Juan Conejero (PTeam)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <pjsr/ButtonCodes.jsh>
#include <pjsr/StdCursor.jsh>

function PreviewControl(parent) {
    this.__base__ = Frame;
    this.__base__(parent);

    this.SetImage = function (image, metadata) {
        this.image = image;
        this.metadata = metadata;
        this.scaledImage = null;
        this.SetZoomOutLimit();
        this.UpdateZoom(this.zoom);
    };

    this.UpdateZoom = function (newZoom, refPoint) {
        newZoom = Math.max(this.zoomOutLimit, Math.min(4, newZoom));
        if (newZoom == this.zoom && this.scaledImage)
            return;

        if (refPoint == null)
            refPoint = new Point(this.scrollbox.viewport.width / 2, this.scrollbox.viewport.height / 2);

        let imgx = null;
        if (this.scrollbox.maxHorizontalScrollPosition > 0)
            imgx = (refPoint.x + this.scrollbox.horizontalScrollPosition) / this.scale;

        let imgy = null;
        if (this.scrollbox.maxVerticalScrollPosition > 0)
            imgy = (refPoint.y + this.scrollbox.verticalScrollPosition) / this.scale;

        this.zoom = newZoom;
        this.scaledImage = null;
        gc(true);

        if (this.zoom > 0) {
            this.scale = this.zoom;
            this.zoomVal_Label.text = format("%d:1", this.zoom);
        } else {
            this.scale = 1 / (-this.zoom + 2);
            this.zoomVal_Label.text = format("1:%d", -this.zoom + 2);
        }

        if (this.image)
            this.scaledImage = this.image.scaled(this.scale);
        else
            this.scaledImage = {
                width: this.metadata.width * this.scale,
                height: this.metadata.height * this.scale
            };

        this.scrollbox.maxHorizontalScrollPosition = Math.max(0, this.scaledImage.width - this.scrollbox.viewport.width);
        this.scrollbox.maxVerticalScrollPosition = Math.max(0, this.scaledImage.height - this.scrollbox.viewport.height);

        if (this.scrollbox.maxHorizontalScrollPosition > 0 && imgx != null)
            this.scrollbox.horizontalScrollPosition = imgx * this.scale - refPoint.x;
        if (this.scrollbox.maxVerticalScrollPosition > 0 && imgy != null)
            this.scrollbox.verticalScrollPosition = imgy * this.scale - refPoint.y;

        this.scrollbox.viewport.update();
    };

    this.zoomIn_Button = new ToolButton(this);
    this.zoomIn_Button.icon = this.scaledResource(":/icons/zoom-in.png");
    this.zoomIn_Button.setScaledFixedSize(24, 24);
    this.zoomIn_Button.toolTip = "Zoom In";
    this.zoomIn_Button.onMousePress = function () {
        this.parent.UpdateZoom(this.parent.zoom + 1);
    };

    this.zoomOut_Button = new ToolButton(this);
    this.zoomOut_Button.icon = this.scaledResource(":/icons/zoom-out.png");
    this.zoomOut_Button.setScaledFixedSize(24, 24);
    this.zoomOut_Button.toolTip = "Zoom Out";
    this.zoomOut_Button.onMousePress = function () {
        this.parent.UpdateZoom(this.parent.zoom - 1);
    };

    this.zoom11_Button = new ToolButton(this);
    this.zoom11_Button.icon = this.scaledResource(":/icons/zoom-1-1.png");
    this.zoom11_Button.setScaledFixedSize(24, 24);
    this.zoom11_Button.toolTip = "Zoom 1:1";
    this.zoom11_Button.onMousePress = function () {
        this.parent.UpdateZoom(1);
    };

    this.buttons_Sizer = new HorizontalSizer;
    this.buttons_Sizer.margin = 4;
    this.buttons_Sizer.spacing = 4;
    this.buttons_Sizer.add(this.zoomIn_Button);
    this.buttons_Sizer.add(this.zoomOut_Button);
    this.buttons_Sizer.add(this.zoom11_Button);
    this.buttons_Sizer.addStretch();

    this.setScaledMinSize(600, 400);
    this.zoom = 1;
    this.scale = 1;
    this.zoomOutLimit = -5;
    this.scrollbox = new ScrollBox(this);
    this.scrollbox.autoScroll = true;
    this.scrollbox.tracking = true;
    this.scrollbox.cursor = new Cursor(StdCursor_OpenHand);

    this.scroll_Sizer = new HorizontalSizer;
    this.scroll_Sizer.add(this.scrollbox);

    this.SetZoomOutLimit = function () {
        let scaleX = Math.ceil(this.metadata.width / this.scrollbox.viewport.width);
        let scaleY = Math.ceil(this.metadata.height / this.scrollbox.viewport.height);
        let scale = Math.max(scaleX, scaleY);
        this.zoomOutLimit = -scale + 2;
    };

    this.scrollbox.onHorizontalScrollPosUpdated = function (newPos) {
        this.viewport.update();
    };

    this.scrollbox.onVerticalScrollPosUpdated = function (newPos) {
        this.viewport.update();
    };

    this.forceRedraw = function () {
        this.scrollbox.viewport.update();
    };

    this.scrollbox.viewport.onMouseWheel = function (x, y, delta, buttonState, modifiers) {
        let preview = this.parent.parent;
        preview.UpdateZoom(preview.zoom + ((delta > 0) ? -1 : 1), new Point(x, y));
    };

    this.scrollbox.viewport.onMousePress = function (x, y, button, buttonState, modifiers) {
        let preview = this.parent.parent;
        if (preview.scrolling || button != MouseButton_Left)
            return;
        preview.scrolling = {
            orgCursor: new Point(x, y),
            orgScroll: new Point(preview.scrollbox.horizontalScrollPosition, preview.scrollbox.verticalScrollPosition)
        };
        this.cursor = new Cursor(StdCursor_ClosedHand);
    };

    this.scrollbox.viewport.onMouseMove = function (x, y, buttonState, modifiers) {
        let preview = this.parent.parent;
        if (preview.scrolling) {
            preview.scrollbox.horizontalScrollPosition = preview.scrolling.orgScroll.x - (x - preview.scrolling.orgCursor.x);
            preview.scrollbox.verticalScrollPosition = preview.scrolling.orgScroll.y - (y - preview.scrolling.orgCursor.y);
        }

        let ox = (this.parent.maxHorizontalScrollPosition > 0) ?
        -this.parent.horizontalScrollPosition : (this.width - preview.scaledImage.width) / 2;
        let oy = (this.parent.maxVerticalScrollPosition > 0) ?
        -this.parent.verticalScrollPosition : (this.height - preview.scaledImage.height) / 2;
        let coordPx = new Point((x - ox) / preview.scale, (y - oy) / preview.scale);
        /*    if ( coordPx.x < 0 ||
        coordPx.x > preview.metadata.width ||
        coordPx.y < 0 ||
        coordPx.y > preview.metadata.height ||
        !preview.metadata.projection ){
        preview.Xval_Label.text = "---";
        preview.Yval_Label.text = "---";
        preview.RAval_Label.text = "---";
        preview.Decval_Label.text = "---";
        }
        else{
        try{
        preview.Xval_Label.text = format( "%8.2f", coordPx.x );
        preview.Yval_Label.text = format( "%8.2f", coordPx.y );
        let coordRD = preview.metadata.Convert_I_RD( coordPx );
        if ( coordRD.x < 0 )
        coordRD.x += 360;
        preview.RAval_Label.text = DMSangle.FromAngle( coordRD.x*24/360 ).ToString( true );
        preview.Decval_Label.text = DMSangle.FromAngle( coordRD.y ).ToString( false );
        }
        catch ( ex ){
        preview.Xval_Label.text = "---";
        preview.Yval_Label.text = "---";
        preview.RAval_Label.text = "---";
        preview.Decval_Label.text = "---";
        }
        } */
    };

    this.scrollbox.viewport.onMouseRelease = function (x, y, button, buttonState, modifiers) {
        let preview = this.parent.parent;
        if (preview.scrolling && button == MouseButton_Left) {
            preview.scrollbox.horizontalScrollPosition = preview.scrolling.orgScroll.x - (x - preview.scrolling.orgCursor.x);
            preview.scrollbox.verticalScrollPosition = preview.scrolling.orgScroll.y - (y - preview.scrolling.orgCursor.y);
            preview.scrolling = null;
            this.cursor = new Cursor(StdCursor_OpenHand);
        }
    };

    this.scrollbox.viewport.onResize = function (wNew, hNew, wOld, hOld) {
        let preview = this.parent.parent;
        if (preview.metadata && preview.scaledImage) {
            this.parent.maxHorizontalScrollPosition = Math.max(0, preview.scaledImage.width - wNew);
            this.parent.maxVerticalScrollPosition = Math.max(0, preview.scaledImage.height - hNew);
            preview.SetZoomOutLimit();
            preview.UpdateZoom(preview.zoom);
        }
        this.update();
    };

    this.scrollbox.viewport.onPaint = function (x0, y0, x1, y1) {
        let preview = this.parent.parent;
        let graphics = new VectorGraphics(this);

        graphics.fillRect(x0, y0, x1, y1, new Brush(0xff202020));

        let offsetX = (this.parent.maxHorizontalScrollPosition > 0) ?
        -this.parent.horizontalScrollPosition : (this.width - preview.scaledImage.width) / 2;
        let offsetY = (this.parent.maxVerticalScrollPosition > 0) ?
        -this.parent.verticalScrollPosition : (this.height - preview.scaledImage.height) / 2;
        graphics.translateTransformation(offsetX, offsetY);

        if (preview.image)
            graphics.drawBitmap(0, 0, preview.scaledImage);
        else
            graphics.fillRect(0, 0, preview.scaledImage.width, preview.scaledImage.height, new Brush(0xff000000));

        graphics.pen = new Pen(0xffffffff, 0);
        graphics.drawRect(-1, -1, preview.scaledImage.width + 1, preview.scaledImage.height + 1);

        if (preview.onCustomPaint) {
            graphics.antialiasing = true;
            graphics.scaleTransformation(preview.scale, preview.scale);
            preview.onCustomPaint.call(preview.onCustomPaintScope, graphics, x0, y0, x1, y1);
        }

        graphics.end();
    };

    this.zoomLabel_Label = new Label(this);
    this.zoomLabel_Label.text = "Zoom:";
    this.zoomVal_Label = new Label(this);
    this.zoomVal_Label.text = "1:1";
    /*

    this.Xlabel_Label = new Label( this );
    this.Xlabel_Label.text = "X:";
    this.Xval_Label = new Label( this );
    this.Xval_Label.text = "---";

    this.Ylabel_Label = new Label( this );
    this.Ylabel_Label.text = "Y:";
    this.Yval_Label = new Label( this );
    this.Yval_Label.text = "---";

    this.RAlabel_Label = new Label( this );
    this.RAlabel_Label.text = "\u03B1:";
    this.RAval_Label = new Label( this );
    this.RAval_Label.text = "---";

    this.Declabel_Label = new Label( this );
    this.Declabel_Label.text = "\u03B4:";
    this.Decval_Label = new Label( this );
    this.Decval_Label.text = "---";
     */
    this.coords_Frame = new Frame(this);
    this.coords_Frame.styleSheet = this.scaledStyleSheet(
            "QLabel { font-family: Hack; background: white; }");
    this.coords_Frame.backgroundColor = 0xffffffff;
    this.coords_Frame.sizer = new HorizontalSizer;
    this.coords_Frame.sizer.margin = 4;
    this.coords_Frame.sizer.spacing = 4;
    this.coords_Frame.sizer.add(this.zoomLabel_Label);
    this.coords_Frame.sizer.add(this.zoomVal_Label);
    /* this.coords_Frame.sizer.addSpacing( 20 );
    this.coords_Frame.sizer.add( this.Xlabel_Label );
    this.coords_Frame.sizer.add( this.Xval_Label );
    this.coords_Frame.sizer.addSpacing( 20 );
    this.coords_Frame.sizer.add( this.Ylabel_Label );
    this.coords_Frame.sizer.add( this.Yval_Label );
    this.coords_Frame.sizer.addSpacing( 20 );
    this.coords_Frame.sizer.add( this.RAlabel_Label );
    this.coords_Frame.sizer.add( this.RAval_Label );
    this.coords_Frame.sizer.addSpacing( 20 );
    this.coords_Frame.sizer.add( this.Declabel_Label );
    this.coords_Frame.sizer.add( this.Decval_Label );  */
    this.coords_Frame.sizer.addStretch();

    this.sizer = new VerticalSizer;
    this.sizer.add(this.buttons_Sizer);
    this.sizer.add(this.scroll_Sizer);
    this.sizer.add(this.coords_Frame);
}

PreviewControl.prototype = new Frame;

function Cleanup() {
    if (!gParameters.workingPreview.isNull) {
        gParameters.workingPreview.forceClose();
    }
}

function main() {

    // ImageWindow
    gParameters.imageActiveWindow = new ImageWindow(ImageWindow.activeWindow);

    // Validations
    if (gParameters.imageActiveWindow === undefined) {

		console.show();
      Console.warningln("<p>No active image.</p>");

    } else if (gParameters.imageActiveWindow.currentView.isPreview) {

      console.show();
      Console.warningln("<p>This script can not work on prewiews.</p>");

    } else {

        // generate a fairly uniq ID to reference temporarly created instances
        //gParameters.UUID = Math.round(Math.random() * 1000000);
        gParameters.UUID = "";

        // is script started from an instance in global or view target context?
        if (Parameters.isGlobalTarget) {
            console.show();
            Console.warningln("<p>This script can not yet run in global context (todo).</p>");
        } else {

            if (Parameters.isViewTarget) {
                // then load the parameters from the instance and continue
                gParameters.load();
            }

            // create and show the dialog
            let dialog = new NBStarColorsFromRGBDialog;
            dialog.execute();
            if (!gParameters.doneButtonPressed) {
                Cleanup();
                if (previewed) {
                    gParameters.rgbView_a.window.forceClose();
                    gParameters.rgbView_b.window.forceClose();
                    gParameters.nbView_L.window.forceClose();
                    gParameters.nbView_a.window.forceClose();
                    gParameters.nbView_b.window.forceClose();
                }
                dialog.cancel();
            }
        }
    }

}

main();
