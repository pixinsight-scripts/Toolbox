<?xml version="1.0" encoding="UTF-8"?>
<xri version="1.0">
   <description>
      <p>A collection of PixInsight scripts for image processing. The following scripts are currently contained: </p>
      <p>	- CreateHubblePaletteFromOSC: Creates a Hubble palette image from a linear image captured using a One shot camera using dual narrowband filters.</p>
      <p>	- ImproveBrilliance: Improves the brilliance of color images.</p>
      <p>	- CombineImages: Combines images or masks with different methods. </p>
      <p>	- ContinuumSubtraction: Continuum subtraction from Narrowband images to extract Ha.</p>
      <p>	- CombineHaToRGB: Combine Ha with RGB image.</p>
      <p>	- CreateHDRImage: Integrate HDR into your images for brighter regions of your target to show more details.</p>
      <p>	- AutoLinearFit: Linear Fit on all color channels automatically. </p>
      <p>	- EnhanceNebula: Brightens the nebula.</p>
      <p>	- SelectiveColorCorrection: Selectively change the colors of your image. </p>
      <p>	- LabColorBoost: Improves the colors of your image using the L*a*b color space.</p>
      <p>	- CombineRGBAndNarrowband: Combines RGB image with duo narrowband image.</p>
      <p>	- GraXpert script: Use GraXpert from within PixInsight to automatically remove gradients using AI.</p>
      <p>	- NarrowbandHueCombination: Simplifies the HSV colourspace-based generation of RGB images from narrowband sources.</p>
      <p>	- NBStarColorsFromRGB: Extracts the RGB stars and replaces them in the target narrowband image using their a and b components in CIELAB color space.</p>
      <p>	- BGVisualizer: Helper script to visualize in black/white the flatness of the background.</p>
   </description>
   <platform os="all" arch="noarch" version="1.8.9-1:1.9.10">
      <package fileName="20240225-PixInsightToolbox-Package.zip" sha1="a7fb708ba54536ed24c88b249805b5e787375e0d" type="script" releaseDate="20240225">
         <title>
            PixInsight Utility scripts
         </title>
         <remove>
         	src/scripts/Toolbox/Graxpert.xsgn,
         	src/scripts/Toolbox/CombineImages.xsgn,
         	src/scripts/Toolbox/ContinuumSubtraction.xsgn,
         	src/scripts/Toolbox/CreateHDRImage.xsgn,
         	src/scripts/BGVisualizer.js,
         	src/scripts/BGVisualizer.xsgn,
         	src/scripts/NarrowbandHueCombination.js,
         	src/scripts/NarrowbandHueCombination.xsgn,
         	src/scripts/NBStarColorsFromRGB.js,
         	src/scripts/NBStarColorsFromRGB.xsgn         	
         </remove>
         <description>
            <p>ImproveBrilliance: Bugfix: added a missing header to SectionBar! </p>
            <p>CombineHaToRGB: Improved background noise removal. Added documentation.</p>
            <p>SelectiveColorCorrection: Added slider to change mask smoothness. Added Import/Export mask support.</p>
            <p>ContinuumSubtraction: LinearFit was removed, because it added more confusion than it helps. In those rare cases where it is necessary please do a linear fit manually! </p>
            <p>Code signing removed for GraXpert, ContinuumSubtraction, CombineImages and CreateHDRImages to support older PixInsight versions, where this signature fails for some unknown reason. </p>
            <p>Copyright (c) 2024 Juergen Terpe and Juergen Baetz, All Rights Reserved.</p>
         </description>
      </package>
   </platform>
</xri>
<Signature developerId="JuergenTerpe" timestamp="2024-02-25T07:33:03.658Z" encoding="Base64">ACG0wDJvGwzlOK/EJHMKXclbeBB9mWmmlJN15oCo43+0AYXXg1rskxOvz+Z25jMN+ehV9bgmNWRVNiSuptvQCQ==</Signature>
